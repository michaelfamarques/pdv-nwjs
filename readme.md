# PDV NW.JS
### Instalação

Para iniciar o desenvolvimento você precisa instalar os seguintes módulos:

```sh
$ npm install gulp-cli -g
$ npm install bower -g
```

Intalar o NW.js em sua última versão

[Download NW.js SDK](https://nwjs.io/downloads/)

Instalando as dependências:

```sh
$ npm install
$ bower install
```

Se você usa o [Visual Studio Code](https://code.visualstudio.com/) você pode utilizar os 3 comandos de dev que já fazem parte do repositório (estão no arquivo .vscode/launch.json):

- **Gulp Serve**: Inicia o servidor de desenvolvimento. Com ele você não precisa ficar reiniciando o NW.js toda vez que mudar algum arquivo do front-end (todos os arquivos que estão dentro da pasta **src**). Este servidor fica rodando em background na porta 5000.
- **Open NW - Dev**: Inicia a aplicação no NW.js. Lembrando que toda vez que houver alguma alteração em um arquivo da pasta **backend** é necessário reiniciar a aplicação
- **Kill Gulp Server**: Utilize para matar o servidor de desenvolvimento

Para caso você queira executar os comandos, segue abaixo a lista dos mais importantes:

Para iniciar o servidor de desenvolvimento:
```sh
$ gulp serve
```

Para iniciar a aplicação:
```sh
$ nw .
```

Para dar build na aplicação e gerar o pacote final do cliente (gerados na pasta dist/):
```sh
$ gulp build
```

Exemplo de compilação de um módulo
```sh
$ npm install sqlite3 --build-from-source --runtime=node-webkit --target_arch=x64 --target=0.26.4
```