(function () {
    'use strict';

    angular
        .module('app.core')
        .directive('validaCodigo', validaCodigoValidator);

    /** @ngInject */
    function validaCodigoValidator($http) {
        return {
            require: 'ngModel',
            restrict: "A",            
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$validators.codigo = function(modelValue, viewValue) {
                    if (ctrl.$isEmpty(modelValue)) {
                        return true;
                    }
                    if(modelValue != attrs.validaCodigo){
                        return false;
                    }
                    return true;
                };
            }
        };
    }
})();