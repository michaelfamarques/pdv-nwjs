(function() {

    angular
        .module('app.core')
        .directive('formataNumero', formataNumeroDirective);

    /** @ngInject */
    function formataNumeroDirective($filter) {
        return {
            require: 'ngModel',
            compile: function(elm, attrs) {

                var semZero = attrs.hasOwnProperty('semZero') ? true : false;
                var decimal = attrs.decimal ? parseInt(attrs.decimal) : 2;
                var inteiro = attrs.inteiro ? parseInt(attrs.inteiro) : 13;
                if (isNaN(decimal)) decimal = 2;
                if (isNaN(inteiro)) inteiro = 13;

                var isDinheiro = attrs.hasOwnProperty('dinheiro') ? true : false;
                var isPorcentagem = attrs.hasOwnProperty('porcentagem') ? "%" : "";

                var max = attrs.maximo ? parseFloat(attrs.maximo) : parseFloat(_.pad('', inteiro, '9') + "." + _.pad('', decimal, 9));
                var min = attrs.minimo ? parseFloat(attrs.minimo) : null;

                return function(scope, elem, attrs, ctrl) {

                    function formatValor(val) {
                        val = parseFloat(val);
                        if (!isNaN(val)) {
                            if (isDinheiro) {
                                return $filter('currency')(val, 'R$ ', decimal);
                            } else {
                                return val.toFixed(decimal).replace(".", ",") + isPorcentagem;
                            }
                        }
                    }

                    function transformaEmNumero(number) {
                        if (!number) {
                            return semZero ? null : 0;
                        }
                        number += '';
                        number = number.replace(/[^\d.,]/g, '');
                        number = number.replace(",", ".");
                        number = parseFloat(number);

                        if (!isNaN(number) && number > max) {
                            number = max;
                        }

                        if(!isNaN(number) && min && number < min){
                            number = min;
                        }

                        if(number === 0 && semZero){
                            return null;
                        }

                        return number;
                    }

                    ctrl.$formatters.unshift(formatValor);
                    ctrl.$parsers.unshift(transformaEmNumero);

                    var oldValue = "";

                    elem.bind("keypress", function(event) {
                        var keyCode = event.which || event.keyCode;
                        var keyCodeChar = String.fromCharCode(keyCode);
                        var value = this.value;
                        var valArr = null;
                        oldValue = value;

                        if (value.indexOf(".") > -1) {
                            valArr = value.split(".");
                        } else if (value.indexOf(",") > -1) {
                            valArr = value.split(",");
                        } else {
                            valArr = [value];
                        }

                        
                        //bloqueia qualquer caracter que não seja número                        8 == backspace firefox nao aceita
                        if (keyCode != '8' && !keyCodeChar.match(new RegExp("[0-9\.,]", "i")) ) {
                            event.preventDefault();
                            return false;
                        }

                        //Não deixa digitar mais de um . ou ,
                        if ((keyCodeChar == "." || keyCodeChar == ",") &&
                            (((value.indexOf(".") > -1 || value.indexOf(",") > -1)) || decimal === 0)) {
                            event.preventDefault();
                            return false;
                        }

                    });

                    elem.on('focus', function(evt) {
                        ctrl.$formatters.shift();
                        var val = elem.val().replace(/R\$ /gi, "").replace(/R\$/gi, "").replace(/%/gi, '');
                        val = val.replace(/\./g, '');
                        val = val.replace(/\,/g, '.');
                        val = parseFloat(val);
                        if (val == 0 || isNaN(val)) {
                            val = "";
                        }
                        elem.val(val);
                    });

                    elem.on('blur', function(evt) {
                        //scope.$apply(function() {
                            var number = transformaEmNumero(ctrl.$modelValue + '');
                            ctrl.$setViewValue(number);
                            elem.val(formatValor(number));
                            ctrl.$formatters.unshift(formatValor);
                        //});
                    });
                }

            }
        };
    };

})();