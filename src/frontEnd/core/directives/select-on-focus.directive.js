(function () {
    'use strict';

    angular
        .module('app.core')
        .directive('selectOnFocus', selectOnFocusDirective);

    /** @ngInject */
    function selectOnFocusDirective($timeout) {
        return {
            restrict: "A",            
            link: function (scope, element, attrs) {
                element.on('focus', function(e){
                    var isto = this;
                    setTimeout(function(){
                        isto.select();
                    }, 100);
                });
            }
        };
    }
})();