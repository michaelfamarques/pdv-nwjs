(function () {
    'use strict';

    angular
        .module('app.core')
        .directive('ngEnter', ngEnterDirective);

    /** @ngInject */
    function ngEnterDirective($http) {
        return {
            restrict: "A",            
            link: function (scope, elm, attrs) {
                elm.bind("keydown keypress", function(event) {
                    if(event.which === 13) {
                        scope.$apply(function(){
                            scope.$eval(attrs.ngEnter, {$event:event});
                        });                            
                        event.preventDefault();
                    }
                });
            }
        };
    }
})();