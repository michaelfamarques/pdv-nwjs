(function () {
    'use strict';

    angular
        .module('app.core')
        .directive('loading', loadingDirective);

    /** @ngInject */
    function loadingDirective($http) {
        return {
            restrict: "A",            
            link: function (scope, elm, attrs) {

                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (v) {
                    if(v){
                        angular.element(elm).addClass('carregando')
                    }else{
                        angular.element(elm).removeClass('carregando');                                      
                    }
                });
            }
        };
    }
})();