(function () {
    'use strict';

    angular
        .module('app.core')
        .directive('autofocusDirective', autofocusDirective);

    /** @ngInject */
    function autofocusDirective($timeout) {
        return {
            restrict: "A",            
            link: function (scope, elm, attrs) {
                $timeout(function() {
                    $element[0].focus();
                });
            }
        };
    }
})();