(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('msUtils', msUtils);

    /** @ngInject */
    function msUtils($window, $rootScope, $mdToast, $q, $mdDialog, $timeout, hotkeys) {
        // Private variables
        var mobileDetect = new MobileDetect($window.navigator.userAgent),
            browserInfo = null;

        var service = {
            exists: exists,
            detectBrowser: detectBrowser,
            guidGenerator: guidGenerator,
            isMobile: isMobile,
            toast: toast,
            login: login,
            loading: loading,
            errorAjax: errorAjax,
            errorPromise: errorPromise,
            confirmar: confirmar,
            alerta: alerta,
            abrirModal: abrirModal,
            error: error,
            toggleInArray: toggleInArray,
            validaCpfCnpj: validaCpfCnpj,
            validaCPF: validaCPF,
            validarCNPJ: validarCNPJ
        };

        var arrTitulos = [];

        return service;

        //////////

        /**
         * Check if item exists in a list
         *
         * @param item
         * @param list
         * @returns {boolean}
         */
        function exists(item, list) {
            return list.indexOf(item) > -1;
        }

        /**
         * Returns browser information
         * from user agent data
         *
         * Found at http://www.quirksmode.org/js/detect.html
         * but modified and updated to fit for our needs
         */
        function detectBrowser() {
            // If we already tested, do not test again
            if (browserInfo) {
                return browserInfo;
            }

            var browserData = [{
                    string: $window.navigator.userAgent,
                    subString: 'Edge',
                    versionSearch: 'Edge',
                    identity: 'Edge'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'Chrome',
                    identity: 'Chrome'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'OmniWeb',
                    versionSearch: 'OmniWeb/',
                    identity: 'OmniWeb'
                },
                {
                    string: $window.navigator.vendor,
                    subString: 'Apple',
                    versionSearch: 'Version',
                    identity: 'Safari'
                },
                {
                    prop: $window.opera,
                    identity: 'Opera'
                },
                {
                    string: $window.navigator.vendor,
                    subString: 'iCab',
                    identity: 'iCab'
                },
                {
                    string: $window.navigator.vendor,
                    subString: 'KDE',
                    identity: 'Konqueror'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'Firefox',
                    identity: 'Firefox'
                },
                {
                    string: $window.navigator.vendor,
                    subString: 'Camino',
                    identity: 'Camino'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'Netscape',
                    identity: 'Netscape'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'MSIE',
                    identity: 'Explorer',
                    versionSearch: 'MSIE'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'Trident/7',
                    identity: 'Explorer',
                    versionSearch: 'rv'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'Gecko',
                    identity: 'Mozilla',
                    versionSearch: 'rv'
                },
                {
                    string: $window.navigator.userAgent,
                    subString: 'Mozilla',
                    identity: 'Netscape',
                    versionSearch: 'Mozilla'
                }
            ];

            var osData = [{
                    string: $window.navigator.platform,
                    subString: 'Win',
                    identity: 'Windows'
                },
                {
                    string: $window.navigator.platform,
                    subString: 'Mac',
                    identity: 'Mac'
                },
                {
                    string: $window.navigator.platform,
                    subString: 'Linux',
                    identity: 'Linux'
                },
                {
                    string: $window.navigator.platform,
                    subString: 'iPhone',
                    identity: 'iPhone'
                },
                {
                    string: $window.navigator.platform,
                    subString: 'iPod',
                    identity: 'iPod'
                },
                {
                    string: $window.navigator.platform,
                    subString: 'iPad',
                    identity: 'iPad'
                },
                {
                    string: $window.navigator.platform,
                    subString: 'Android',
                    identity: 'Android'
                }
            ];

            var versionSearchString = '';

            function searchString(data) {
                for (var i = 0; i < data.length; i++) {
                    var dataString = data[i].string;
                    var dataProp = data[i].prop;

                    versionSearchString = data[i].versionSearch || data[i].identity;

                    if (dataString) {
                        if (dataString.indexOf(data[i].subString) !== -1) {
                            return data[i].identity;

                        }
                    } else if (dataProp) {
                        return data[i].identity;
                    }
                }
            }

            function searchVersion(dataString) {
                var index = dataString.indexOf(versionSearchString);

                if (index === -1) {
                    return;
                }

                return parseInt(dataString.substring(index + versionSearchString.length + 1));
            }

            var browser = searchString(browserData) || 'unknown-browser';
            var version = searchVersion($window.navigator.userAgent) || searchVersion($window.navigator.appVersion) || 'unknown-version';
            var os = searchString(osData) || 'unknown-os';

            // Prepare and store the object
            browser = browser.toLowerCase();
            version = browser + '-' + version;
            os = os.toLowerCase();

            browserInfo = {
                browser: browser,
                version: version,
                os: os
            };

            return browserInfo;
        }

        /**
         * Generates a globally unique id
         *
         * @returns {*}
         */
        function guidGenerator() {
            var S4 = function () {
                return (((1 + Math.random()) * 0x10000) || 0).toString(16).substring(1);
            };
            return (S4() + S4() + S4() + S4() + S4() + S4());
        }

        /**
         * Return if current device is a
         * mobile device or not
         */
        function isMobile() {
            return mobileDetect.mobile();
        }

        /**
         * Toggle in array (push or splice)
         *
         * @param item
         * @param array
         */
        function toggleInArray(item, array) {
            if (array.indexOf(item) === -1) {
                array.push(item);
            } else {
                array.splice(array.indexOf(item), 1);
            }
        }

        /**
         * Show a toast message
         *
         * @param message
         */
        function toast(message) {
            var toastObj = $mdToast.simple()
                .textContent(message)
                .action('OK')
                .highlightAction(true)
                .highlightClass('md-accent') // Accent is used by default, this just demonstrates the usage.
                .position('bottom right');
            return $mdToast.show(toastObj);
        }

        function errorAjax(err) {
            toast(err.data.error);
        }

        function abrirModal(config){
            $rootScope.popupAberto++;            
            return $mdDialog.show(config)
            .finally(() => {
                $rootScope.popupAberto--;
            });
        }

        /**
         * Show a confirm dialog
         *
         * @param titulo
         * @param message
         * @param callback
         */
        function confirmar(mensagem, titulo, podeCancelar, callback) {

            if (angular.isFunction(titulo)) {
                callback = titulo;
                titulo = "Atenção!";
            }

            if (!titulo) {
                titulo = "Atenção!";
            }

            return abrirModal({
                controller: function DialogController() {
                    var vmInterno = this;
                    vmInterno.titulo = titulo;
                    vmInterno.mensagem = mensagem;
                    vmInterno.sim = function (resposta) {
                        if (angular.isFunction(callback)) callback(true);
                        $mdDialog.hide();
                    }
                    vmInterno.nao = function () {
                        if (angular.isFunction(callback)) callback(false);
                        $mdDialog.cancel();
                    };
                },
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/core/services/views/confirm.dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: podeCancelar,
                escapeToClose: podeCancelar,
                multiple: true
            });

        }

        /**
         * Show a alert dialog
         *
         * @param titulo
         * @param message
         * @param callback
         */
        function alerta(mensagem, titulo, callback) {

            if (angular.isFunction(titulo)) {
                callback = titulo;
                titulo = "Atenção!";
            }

            if (!titulo) {
                titulo = "Atenção!";
            }

            return abrirModal({
                controller: ['$scope', function DialogController($scope) {
                    var vmInterno = this;
                    vmInterno.titulo = titulo;
                    vmInterno.mensagem = mensagem;
                    vmInterno.sim = function (resposta) {
                        if (angular.isFunction(callback)) callback(true);
                        $mdDialog.hide();
                    }
                    hotkeys.bindTo($scope)
                        .add({
                            combo: 'enter',
                            description: 'enter',
                            allowIn: ['INPUT'],
                            callback: function (evt) {
                                vmInterno.sim(); 
                            }
                        });
                }],
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/core/services/views/alert.dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                escapeToClose: true,
                multiple: true
            });

        }

        /**
         * Show a alert dialog
         *
         * @param titulo
         * @param message
         * @param callback
         */
        function login(mensagem, titulo, tipoSenha, podeCancelar, callback) {

            var executado = false;

            return abrirModal({
                controller: function DialogController() {
                    var vmInterno = this;
                    vmInterno.titulo = titulo; //"Acesso do usuário";
                    vmInterno.operador = "1";
                    vmInterno.senha = "1";
                    vmInterno.mensagem = mensagem;
                    vmInterno.podeCancelar = podeCancelar;
                    vmInterno.logar = function (resposta) {
                        var result = require('backend/models/Usuario').autenticarUsuario(vmInterno.operador, vmInterno.senha, tipoSenha);
                        if (angular.isFunction(callback)) callback(result);
                        $mdDialog.cancel(result);
                        executado = true;
                    }
                    vmInterno.nao = function () {        
                        callback(tipoSenha == 0 ? 'ABANDONOU' : 'Não tem acesso a função!');
                        $mdDialog.cancel(tipoSenha == 0 ? 'ABANDONOU' : 'Não tem acesso a função!');
                        executado = true;
                    };
                    $timeout(() => {
                        vmInterno.logar();
                    }, 500);                    
                },
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/core/services/views/login.dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: podeCancelar,
                escapeToClose: podeCancelar,
                multiple: true
            }).finally(() => {
                if (!executado) callback(tipoSenha == 0 ? 'ABANDONOU' : 'Não tem acesso a função!');
            });

        }
        
        function loading(mensagem) {

            return abrirModal({
                controller: function DialogController() {
                    var vmInterno = this;
                    vmInterno.mensagem = mensagem;               
                },
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/core/services/views/loading.dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                escapeToClose: false,
                multiple: true
            });

        }

        /**
         * Show a error dialog
         *
         * @param titulo
         * @param message
         * @param callback
         */
        function error(mensagem, titulo, callback) {

            if (angular.isFunction(titulo)) {
                callback = titulo;
                titulo = "Erro!";
            }

            if (!titulo) {
                titulo = "Erro!";
            }

            return abrirModal({
                controller: function DialogController() {
                    var vmInterno = this;
                    vmInterno.titulo = titulo;
                    vmInterno.mensagem = mensagem;
                    vmInterno.sim = function (resposta) {
                        if (angular.isFunction(callback)) callback(true);
                        $mdDialog.hide();
                    }
                },
                controllerAs: 'vm',
                hasBackdrop: true,
                templateUrl: 'frontEnd/core/services/views/error.dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                escapeToClose: true,
                fullscreen: true,
                multiple: true
            });

        }

        function errorPromise(err){
            if(err){
                if(err.status == "400") err.message = "Erro na requisição!";
                if(err.status == "404") err.message = "Serviço não encontrado!";
                if(err.status == "500" || err.status == "501" || err.status == "502" || err.status == "503" || err.status == "504") err.message = "Serviço não disponível!";                
                return error(err.message || err.description || _.get(err, 'data.error.message') || _.get(err, 'response.error.message'));
            }else{
                return Promise.resolve();
            }
        }

        function validaCpfCnpj(documento) {
            documento = documento.replace(/[^\d]+/g, '');
            if(documento.length == 11){
                return validaCPF(documento);
            }else if(documento.length == 14){
                return validarCNPJ(documento);
            }else{
                return false;
            }
        }
    
        function validaCPF(cpf) {
            let numeros, digitos, soma, i, resultado, digitos_iguais;
            digitos_iguais = 1;
            if (cpf.length < 11)
                return false;
            for (i = 0; i < cpf.length - 1; i++)
                if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                    digitos_iguais = 0;
                    break;
                }
            if (!digitos_iguais) {
                numeros = cpf.substring(0, 9);
                digitos = cpf.substring(9);
                soma = 0;
                for (i = 10; i > 1; i--)
                    soma += numeros.charAt(10 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0))
                    return false;
                numeros = cpf.substring(0, 10);
                soma = 0;
                for (i = 11; i > 1; i--)
                    soma += numeros.charAt(11 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1))
                    return false;
                return true;
            }
            else
                return false;
        }
    
        function validarCNPJ(cnpj) {
    
            let tamanho, numeros, digitos, soma, pos, resultado;
            // Elimina CNPJs invalidos conhecidos
            if (cnpj == "00000000000000" ||
                cnpj == "11111111111111" ||
                cnpj == "22222222222222" ||
                cnpj == "33333333333333" ||
                cnpj == "44444444444444" ||
                cnpj == "55555555555555" ||
                cnpj == "66666666666666" ||
                cnpj == "77777777777777" ||
                cnpj == "88888888888888" ||
                cnpj == "99999999999999")
                return false;
    
            // Valida DVs
            tamanho = cnpj.length - 2
            numeros = cnpj.substring(0, tamanho);
            digitos = cnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (let i = tamanho; i >= 1; i--) {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
    
            tamanho = tamanho + 1;
            numeros = cnpj.substring(0, tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (let i = tamanho; i >= 1; i--) {
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2)
                    pos = 9;
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;
    
            return true;
    
        }

    }
}());