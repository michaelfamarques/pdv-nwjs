(function () {
    'use strict';

    angular
        .module('novoPdv')
        .controller('ImpressoraController', ImpressoraController);

    /** @ngInject */
    function ImpressoraController(configInstance, msUtils, $rootScope, hotkeys, $scope, $mdDialog, $state, $timeout) {

        var vm = this;

        vm.configInstance = configInstance;

        vm.arrImpressoras = $rootScope.App.getImpressoras();

        vm.driversImpressoras = ["TP280", "BEMATECH", "DARUMA", "EPSON", "ELGIN", "GP-U80300III", "PDF"];

        vm.fechar = function(){
            $mdDialog.hide();
        }

    }

})();