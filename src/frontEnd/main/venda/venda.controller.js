(function () {
    'use strict';

    angular
        .module('app.venda')
        .controller('VendaController', VendaController);

    /** @ngInject */
    function VendaController($filter, msUtils, $rootScope, hotkeys, $scope, $mdDialog, $state, Cupom, $timeout) {

        const path = require("path");
        const ProdutoClass = $rootScope.Db.Produto;
        const CupomVendaClass = $rootScope.Db.CupomVenda;
        const ProdutoVendidoClass = $rootScope.Db.ProdutoVendido;
        const TIPOS = require('backend/tipos');

        var intervalFocus = null;

        vm = this;

        vm.acrescimoDesconto = null;
        vm.configInstance = $rootScope.configInstance;

        $scope.$on('$destroy', () => {
            clearInterval(intervalFocus);
        });

        $scope.$on('loadCupom', () => {
            CupomVendaClass.getVendaAberta(true).then((cupom) => {
                $scope.$apply(() => {
                    vm.cupomVenda = cupom;
                });                
            });
        });

        intervalFocus = setInterval(() => {
            var id = angular.element(document.activeElement).attr('id');
            if(id != entrada && !$rootScope.popupAberto){
                angular.element('#entrada').focus();
            }
        }, 1000);

        var vm = this;

        vm.subTotalVenda = 0;

        vm.cupomVenda = Cupom;       

        vm.hasLogo = () => {
            return !!vm.configInstance.dadosFilial.logomarca;
        }
        vm.getLogo = () => {
            return path.join(nw.App.dataPath, path.basename(vm.configInstance.dadosFilial.logomarca));
        }
        vm.mensagemAtual = "Informe um produto...";

        vm.abrirConfiguracoes = () => {

            if ($rootScope.popupAberto) return;
            return msUtils.abrirModal({
                controller: 'ConfiguracoesController',
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/main/configuracoes/views/configuracoes.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                escapeToClose: true
            }).then(() => {

            });

        }

        vm.cancelarItem = () => {
            if (!vm.cupomVenda) { return msUtils.toast("Venda não está aberta!"); }
            if (vm.cupomVenda.produtosArr.length == 0) {
                return msUtils.toast("A venda não possui produtos para cancelar!");
            }
            if ($rootScope.popupAberto) return;
            return msUtils.abrirModal({
                controller: 'CancelarModalController',
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/main/venda/views/modalCancelarItem.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                escapeToClose: true,
                locals: {
                    tipo: 'produto'
                }
            }).then((numero) => {
                if (numero !== undefined) {
                    var produtoRemovido = vm.cupomVenda.produtosArr.find((p) => p.numero == numero);
                    if (!produtoRemovido) return msUtils.toast("Número de produto inválido!");
                    vm.cupomVenda.removeProduto(produtoRemovido).then(() => {
                        $scope.$apply(() => {
                            vm.mensagemAtual = "Produto " + produtoRemovido.codigoEan + " - " + produtoRemovido.produtoInstance.descricao + " removido da venda";
                        });
                    }).catch(console.log);
                }
            });
        }

        vm.cancelarVenda = () => {
            if ($rootScope.popupAberto) return;

            if (!vm.cupomVenda) {       
                
                if(vm.configInstance.contingenciaOffline){
                    msUtils.alerta("VOcê não pode cancelar uma venda NFC-e enquanto estiver com o modo de contingência offline ativado!");
                    return;
                }

                msUtils.abrirModal({
                    controller: 'CancelarModalController',
                    controllerAs: 'vm',
                    hasBackdrop: true,
                    fullscreen: true,
                    templateUrl: 'frontEnd/main/venda/views/modalCancelarVenda.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    escapeToClose: true,
                    locals: {
                        tipo: 'cupom'
                    }
                }).then((result) => {
                    if (result !== undefined) {
                        msUtils.loading("Cancelando venda...");
                        result.cupom.cancelarVenda(result.justificativa).then(() => {
                            $mdDialog.hide();
                            $timeout(() => {
                                msUtils.alerta("Cupom cancelado com sucesso!");
                            }, 100);                            
                        }).catch((err) => {
                            $mdDialog.hide();
                            $timeout(() => {
                                msUtils.error(err.message, 'Erro ao cancelar venda!');
                            }, 100);
                        });
                    }
                });
            }else{
                return msUtils.confirmar("Deseja realmente cancelar esta venda?", "Cancelar Cupom", true, (res) => {
                    if(res){
                        var cupomAntigo = vm.cupomVenda;                        
                        return cupomAntigo.cancelarVendaAberta().then(() => {
                            $scope.$apply(() => {
                                vm.cupomVenda = null;
                                vm.mensagemAtual = "Cupom "+cupomAntigo.codigoCupom+" cancelado!";
                            });
                        });                        
                    }
                });
            }
            
        }

        vm.produtos = () => {
            if ($rootScope.popupAberto) return;
            return msUtils.abrirModal({
                controller: 'PesquisarProdutoModalController',
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/main/venda/views/modalProdutos.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                escapeToClose: true
            }).then((produto) => {
                if (produto) {
                    vm.mensagemAtual = "Produto " + produto.codigoEan + " - " + produto.descricao + " selecionado";
                    vm.entrada = produto.codigoEan + "*";
                    angular.element(".input-pesquisa").focus();
                }
            });
        }

        vm.abrirAcrescimoDesconto = () => {
            if ($rootScope.popupAberto) return;
            return msUtils.abrirModal({
                controller: 'AcrescimoDescontoModal',
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/main/venda/views/modalAcrescimoDesconto.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                escapeToClose: true,
                locals: {
                    finalizando: false
                }
            }).then((dados) => {
                if (dados) {
                    vm.acrescimoDesconto = dados;
                    vm.acrescimoDesconto.valor = parseFloat(vm.acrescimoDesconto.valor) || null;
                    vm.acrescimoDesconto.porcentagem = parseFloat(vm.acrescimoDesconto.porcentagem) || null;
                    vm.mensagemAtual = "Aplicar Desconto/Acréscimo no Produto...";
                }
            }).finally(() => {
                initHotkeys();
            });
        }

        vm.fecharVenda = () => {
            if (!vm.cupomVenda) { return msUtils.toast("Venda não está aberta!"); }
            if (vm.cupomVenda.produtosArr.length == 0) {
                return msUtils.toast("Você deve informar pelo menos um produto para fechar a venda!");
            }                
            $state.go("app.pagamento");
        }

        vm.adicionarProduto = function () {

            if(!vm.entrada) return;

            if (vm.entrada.toLowerCase().indexOf("*") == -1) {
                return msUtils.toast("Você não informou a quantidade!");
            }

            var params = vm.entrada.split("*");

            if(params[1] === ""){
                params[1] = 1;
            }

            if (isNaN(params[1])) {
                return msUtils.toast("Você não informou a quantidade!");
            }

            ProdutoClass.getPorCodigo(params[0]).then((produto) => {
                
                if (!produto) {
                    msUtils.toast("Produto não encontrado!");
                    $scope.$apply(() => {
                        vm.entrada = "";
                    });                    
                    return;
                }

                var quantidade = parseFloat(params[1]);
                var valorTotal = quantidade * produto.precoDeVenda;
                var valorDesconto = 0;
                var valorAcrescimo = 0;

                if (vm.acrescimoDesconto) {
                    if (vm.acrescimoDesconto.tipo == 'acrescimo') {
                        if (vm.acrescimoDesconto.valor) {
                            if (vm.acrescimoDesconto.valor > valorTotal) {
                                return msUtils.alerta('Não é permitido o acréscimo ser maior que o valor do item');
                            }
                            valorTotal += vm.acrescimoDesconto.valor;
                            valorAcrescimo = vm.acrescimoDesconto.valor;
                        } else if (vm.acrescimoDesconto.porcentagem) {
                            valorAcrescimo = valorTotal * (vm.acrescimoDesconto.porcentagem / 100);
                            valorTotal += valorAcrescimo;
                        }
                    } else if (vm.acrescimoDesconto.tipo == 'desconto') {
                        if (vm.acrescimoDesconto.valor) {
                            if (vm.acrescimoDesconto.valor >= valorTotal) {
                                return msUtils.alerta('Não é permitido o desconto ser igual ou maior que o valor do item');
                            }
                            valorTotal -= vm.acrescimoDesconto.valor;
                            valorDesconto = vm.acrescimoDesconto.valor;
                        } else if (vm.acrescimoDesconto.porcentagem) {
                            valorDesconto = valorTotal * (vm.acrescimoDesconto.porcentagem / 100);
                            valorTotal -= valorDesconto;
                        }
                    }
                }
    
                var produtoNovo = {
                    numero: 0,
                    codigoEan: produto.codigoEan,
                    valorDesconto: valorDesconto,
                    valorAcrescimo: valorAcrescimo,
                    quantidade: quantidade,
                    valorUnitario: produto.precoDeVenda,
                    valorTotal: valorTotal
                }

                var produtoInstance = null;
    
                verificaCupom().then((cupom) => {
                    vm.cupomVenda = cupom;
                    produtoNovo.numero = vm.cupomVenda.produtosArr.length + 1;
                    produtoInstance = ProdutoVendidoClass.build(produtoNovo);
                    return vm.cupomVenda.addProduto(produtoInstance);
                }).then(() => {
                    produtoInstance.produtoInstance = produto;
                    $scope.$apply(() => {
                        vm.entrada = "";
                        vm.acrescimoDesconto = null;
                        vm.mensagemAtual = produto.descricao + " " + produtoNovo.quantidade + " x " + $filter('currency')(produtoNovo.valorUnitario) + " = " + $filter('currency')(produtoNovo.valorTotal);
                    });
                }).catch((err) => {
                    console.log(err);
                });
                
            });

        }

        function verificaCupom(){

            return new Promise((resolve, reject) => {
                if(vm.cupomVenda){
                    resolve(vm.cupomVenda);
                }else{
                    return CupomVendaClass.getUltimoCOO().then((coo) => {
                        return CupomVendaClass.build({
                            codigoCupom: coo,
                            status: TIPOS.CupomVendaStatus.Aberto
                        }).save().then((cupomNovo) => {
                            CupomVendaClass.cupomAberto = cupomNovo;
                            cupomNovo.produtosArr = [];
                            resolve(cupomNovo);
                        }).catch(reject);
                    });
                }
            });

        }

        initHotkeys();

        function initHotkeys(){

            hotkeys.bindTo($scope).add({
                combo: 'f4',
                description: 'Configurações',
                allowIn: ['INPUT'],
                callback: vm.abrirConfiguracoes
            });
    
            hotkeys.bindTo($scope).add({
                combo: 'f1',
                description: 'Cancelar Item',
                allowIn: ['INPUT'],
                callback: vm.cancelarItem
            });
    
            hotkeys.bindTo($scope).add({
                combo: 'f2',
                description: 'Cancelar Venda',
                allowIn: ['INPUT'],
                callback: vm.cancelarVenda
            });
    
            hotkeys.bindTo($scope).add({
                combo: 'f5',
                description: 'Lista Produtos',
                allowIn: ['INPUT'],
                callback: vm.produtos
            });

            hotkeys.bindTo($scope).add({
                combo: 'f8',
                description: 'Adicionar Acréscimo/Desconto',
                allowIn: ['INPUT'],
                callback: vm.abrirAcrescimoDesconto
            });
    
            hotkeys.bindTo($scope).add({
                combo: 'f10',
                description: 'Fechar Venda',
                allowIn: ['INPUT'],
                callback: vm.fecharVenda
            });

        }

    }

})();