(function() {
    'use strict';

    angular
        .module('app.venda', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider) {

        // State
        $stateProvider.state('app.venda', {
            url: '/venda',
            views: {
                'content': {
                    templateUrl: 'frontEnd/main/venda/views/venda.html',
                    controller: 'VendaController as vm'
                }
            },
            bodyClass: 'venda',
            resolve: {
                Cupom: ['$rootScope', function($rootScope){                    
                    const CupomVendaClass = $rootScope.Db.CupomVenda;
                    return CupomVendaClass.getVendaAberta(true);
                }]
            }
        });

    }

})();