(function () {
    'use strict';

    angular
        .module('app.venda')
        .controller('CancelarModalController', CancelarModalController);

    /** @ngInject */
    function CancelarModalController($filter, $rootScope, msUtils, $mdDialog, $scope, hotkeys, tipo) {

        var vm = this;

        var CupomVendaClass = $rootScope.Db.CupomVenda;

        vm.tipo = tipo;

        hotkeys.bindTo($scope)
            .add({
                combo: 'enter',
                allowIn: ['INPUT', 'TEXTAREA'],
                callback: function (eve) {
                    eve.preventDefault();
                    if(eve.target){
                        if($(eve.target).hasClass('ativa-enter')){
                            vm.cancelar();
                        }else{
                            down(null, true);
                        }
                    }                    
                }
            });

        vm.cancelar = function(){
            if(!vm.numero){
                return msUtils.toast("Você deve informar um número!");
            }
            if(vm.tipo == 'produto'){
                $mdDialog.hide(vm.numero);
            }else{
                CupomVendaClass.getVendaPorCoo(vm.numero).then((venda) => {
                    if(!venda){
                        msUtils.error("Venda "+vm.numero+" não encontrada!").then(() => vm.focus()).catch(() => vm.focus());
                    }else if(!venda.autorizacao){
                        msUtils.error("Venda não finalizada!").then(() => vm.focus()).catch(() => vm.focus());
                    }else if(venda.autorizacao.cancelamento){
                        msUtils.error("Venda já cancelada!!").then(() => vm.focus()).catch(() => vm.focus());
                    }else{
                        $mdDialog.hide({
                            cupom: venda,
                            justificativa: vm.justificativa
                        });
                    }
                });
            }            
        }

        hotkeys.bindTo($scope)
            .add({
                combo: 'down',
                description: 'down',
                allowIn: ['INPUT', 'TEXTAREA'],
                callback: down
            });

        hotkeys.bindTo($scope)
            .add({
                combo: 'up',
                description: 'up',
                allowIn: ['INPUT', 'TEXTAREA'],
                callback: up
            });

        function down(eve, ativaEnter){
            var arrBotoesConfig = $('.focus-modal-cancelar');
            var focused = arrBotoesConfig.filter(':focus');

            var elementFocused = null;

            if(focused.length == 0){
                elementFocused = arrBotoesConfig.get(0);
            }else{
                var index = arrBotoesConfig.index(focused);
                index++;
                if(index >= arrBotoesConfig.length){
                    index = 0;
                }
                elementFocused = arrBotoesConfig.get(index);
            }

            elementFocused.focus();
            if(ativaEnter && $(elementFocused).hasClass('ativa-enter')){
                vm.cancelar();
            }
        }

        function up(evt) {
            var arrBotoesConfig = $('.focus-modal-cancelar');
            var focused = arrBotoesConfig.filter(':focus');
            if(focused.length == 0){
                arrBotoesConfig.get(arrBotoesConfig.length-1).focus();
            }else{
                var index = arrBotoesConfig.index(focused);
                index--;
                if(index < 0){
                    index = arrBotoesConfig.length-1;
                }
                arrBotoesConfig.get(index).focus();
            }  
        }

        vm.focus = function(){
            $('#campo-numero').focus();
        }

        vm.fechar = function(){
            $mdDialog.hide();
        }

    }

})();