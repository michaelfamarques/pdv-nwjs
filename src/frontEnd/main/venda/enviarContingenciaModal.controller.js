(function () {
    'use strict';

    angular
        .module('app.venda')
        .controller('EnviarContingenciaModalController', EnviarContingenciaModalController);

    /** @ngInject */
    function EnviarContingenciaModalController($filter, $rootScope, msUtils, $mdDialog, $scope, hotkeys, cupomArr) {

        var vm = this;
        vm.cupomArr = cupomArr;
        var problemaConexao = false;

        vm.arrStatus = [];
        vm.cupomArr.forEach((c) => {
            vm.arrStatus[c.id] = "Enviando...";
        });

        async.eachLimit(vm.cupomArr, 2, (cupom, next) => {
            cupom.enviarContingencia().then(() => {
                vm.arrStatus[cupom.id] = "Autorizada";
                next();
            }).catch((err) => {
                if(err.erroAutorizacao){
                    vm.arrStatus[cupom.id] = "Problema ao autorizar: "+err.message;
                }else{
                    vm.arrStatus[cupom.id] = "Problema de conexão: "+err.message;
                    problemaConexao = true;
                }
                next();
            });
        }, (err) => {
            if(err){
                msUtils.error(err.message);
            }
            $scope.$apply(() => {
                vm.podeFechar = true;
                setTimeout(() => {
                    $('.botao-fechar').focus();
                }, 50);
            });            
        });

        vm.fechar = function(){
            $mdDialog.hide(problemaConexao);
        }

    }

})();