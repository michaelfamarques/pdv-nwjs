(function () {
    'use strict';

    angular
        .module('app.venda')
        .controller('AcrescimoDescontoModal', AcrescimoDescontoModal);

    /** @ngInject */
    function AcrescimoDescontoModal($scope, hotkeys, finalizando, $mdDialog) {

        var vm = this;

        vm.data = {
            tipo: null,
            porcentagem: null,
            valor: null
        };

        $scope.$watch('vm.data.porcentagem', (v, oV) => {
            if (v === oV) return;
            if (v) {
                vm.data.valor = null;
            }
        });

        $scope.$watch('vm.data.valor', (v, oV) => {
            if (v === oV) return;
            if (v) {
                vm.data.porcentagem = null;
            }
        });

        vm.fechar = () => $mdDialog.hide();

        vm.getTitulo = () => {
            switch (vm.data.tipo) {
                case "desconto":
                    return "Informe o Desconto - "+(finalizando?'Venda':'Produto');
                case "acrescimo":
                    return "Informe o Acréscimo - "+(finalizando?'Venda':'Produto');
                default:
                    return "Desconto/Acréscimo - "+(finalizando?'Venda':'Produto');
            }
        }

        vm.escolher = (tipo) => {
            vm.data.tipo = tipo;
            setTimeout(() => {
                angular.element('#campoValor').focus();
            }, 10);
        }

        var arrBotoes = ['btnDesconto', 'btnAcrescimo'];
        var arrCampos = ['campoValor', 'campoPorcentagem'];

        function anterior(eve){
            eve.preventDefault();
            var id = angular.element(document.activeElement).attr('id');
            var index = arrBotoes.indexOf(id);
            if (!index || index == 0) {
                index = 1;
            }
            index--;
            angular.element('#' + arrBotoes[index]).focus();
        }

        function proximo(eve){
            eve.preventDefault();
            var id = angular.element(document.activeElement).attr('id');
            var index = arrBotoes.indexOf(id);
            if (!index || index == arrBotoes.length - 1) {
                index = arrBotoes.length - 2;
            }
            index++;
            angular.element('#' + arrBotoes[index]).focus();
        }

        hotkeys.bindTo($scope)
            .add({
                combo: 'left',
                allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                callback: anterior
            });

        hotkeys.bindTo($scope)
            .add({
                combo: 'right',
                allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                callback: proximo
            });

        hotkeys.bindTo($scope)
            .add({
                combo: 'up',
                allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                callback: function(eve) {
                    eve.preventDefault();
                    if(!vm.data.tipo){
                        anterior(eve);
                        return;
                    }
                    var id = angular.element(document.activeElement).attr('id');
                    var index = arrCampos.indexOf(id);
                    if (!index || index == 0) {
                        index = 1;
                    }
                    index--;
                    setTimeout(() => {
                        angular.element('#' + arrCampos[index]).focus();
                    }, 50);                    
                }
            });

        hotkeys.bindTo($scope)
            .add({
                combo: 'down',
                allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                callback: function(eve) {
                    eve.preventDefault();
                    if(!vm.data.tipo){
                        proximo(eve);
                        return;
                    }
                    var id = angular.element(document.activeElement).attr('id');
                    var index = arrCampos.indexOf(id);
                    if (!index || index == arrCampos.length - 1) {
                        index = arrCampos.length - 2;
                    }
                    index++;
                    setTimeout(() => {
                        angular.element('#' + arrCampos[index]).focus();
                    }, 50);                    
                }
            });

        vm.ok = () => {
            if(
                ((vm.data.porcentagem >= 100 && vm.data.tipo == 'desconto') ||
                (vm.data.porcentagem > 100 && vm.data.tipo == 'acrescimo'))
            ){
                return angular.element('#campoPorcentagem').focus();                
            }
            $mdDialog.hide(vm.data);
        }

    }

})();