(function () {
    'use strict';

    angular
        .module('app.venda')
        .controller('PesquisarProdutoModalController', PesquisarProdutoModalController);

    /** @ngInject */
    function PesquisarProdutoModalController($filter, msUtils, $mdDialog, $scope, hotkeys) {

        const Produto = require("backend/db").Produto;

        var vm = this;
        vm.pesquisa = "";
        vm.selected = null;

        $scope.$watch('vm.pesquisar', (v) => {
            vm.selected = null;
            Produto.getProdutos(v).then((produtos) => {
                $scope.$apply(() => {
                    vm.produtos = produtos;    
                });                
            });
        });

        vm.fechar = () => $mdDialog.hide();

        vm.selecionarMouse = function(index){
            vm.selected = index;
        }

        vm.selecionarMouseEFechar = function(index){
            vm.selecionarMouse(index);
            selecionarEFechar();
        }

        hotkeys.bindTo($scope)
            .add({
                combo: 'up',
                allowIn: ['INPUT'],
                callback: function () {
                    if (vm.selected == null) {
                        vm.selected = 0;
                    } else {
                        vm.selected -= 1;
                        if (vm.selected < 0) vm.selected = 0;
                    }
                }
            });

        hotkeys.bindTo($scope)
            .add({
                combo: 'down',
                allowIn: ['INPUT'],
                callback: function () {
                    if (vm.selected == null) {
                        vm.selected = 0;
                    } else {
                        vm.selected += 1;
                        if (vm.selected > (vm.produtos.length - 1)) vm.selected = vm.produtos.length - 1;
                    }
                }
            });

        hotkeys.bindTo($scope)
            .add({
                combo: 'enter',
                allowIn: ['INPUT'],
                callback: function () {
                    selecionarEFechar();
                }
            });

        function selecionarEFechar(){
            if(vm.selected == null){
                if(vm.produtos.length == 1){
                    $mdDialog.hide(vm.produtos[0]); 
                }else{
                    msUtils.toast("Você deve selecionar um produto!");
                }                        
            }else{
                $mdDialog.hide(vm.produtos[vm.selected]);
            }
        }

    }

})();