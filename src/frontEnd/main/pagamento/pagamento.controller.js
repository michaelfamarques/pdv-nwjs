(function () {
    'use strict';

    angular
        .module('app.pagamento')
        .controller('PagamentoController', PagamentoController);

    /** @ngInject */
    function PagamentoController($filter, msUtils, $rootScope, hotkeys, $scope, $mdDialog, $state, pagamentosArr, $timeout) {

        const CupomVendaClass = $rootScope.Db.CupomVenda;
        const ConfiguracaoClass = $rootScope.Db.Configuracao;

        const utils = require("backend/utils");

        var vm = this;
        
        vm.configInstance = $rootScope.configInstance;

        vm.cupomVenda = CupomVendaClass.cupomAberto;
        if(!Array.isArray(vm.cupomVenda.pagamentosArr)){
            vm.cupomVenda.pagamentosArr = [];
        }

        vm.mensagemAtual = "Confirme o fechamento (F10)";
        vm.formasPagamento = pagamentosArr;
        vm.dadosPagamento = {};

        var indexSelecionado = 0;

        vm.valorAPagar = null;
        vm.formaSelecionada = vm.formasPagamento[indexSelecionado];

        vm.solicitaCliente = false;
        vm.solicitaParcelas = false;
        vm.parcelas = null;

        vm.termoVariacao = termoVariacao;
        vm.valorVariacao = valorVariacao;

        vm.selecionouFormaPagamento = selecionouFormaPagamento;
        vm.adicionarPagamento = adicionarPagamento;
        vm.confirmaCancelar = confirmaCancelar;

        function termoVariacao(){
            if(vm.cupomVenda.valorDesconto > 0){
                return "Desconto";
            }else{
                return "Acréscimo";
            }
        }

        function valorVariacao(){
            return vm.cupomVenda.valorDesconto || vm.cupomVenda.valorAcrescimo || 0;
        }

        var arrCamposNavegacao = [];
        var arrCamposDisponiveis = [
            'campo-valor', 'campo-vencimento', 'campo-documento'
        ];

        var cancelarPagamentoAberto = false;

        function cancelar() {
            vm.cupomVenda.valorPago = 0;
            vm.cupomVenda.pagamentosArr = [];
            $state.go('app.venda');
        }

        function validaParcelas(){
            if (!vm.formaSelecionada) { return false; }
    
            let maxParcelas = vm.formaSelecionada.maxParcelas;
            let valMinParc = vm.formaSelecionada.valorMinimoParcela;
    
            if (vm.dadosPagamento.parcelas > maxParcelas) {
                msUtils.error("Número informado maior que o permitido!").then(() => {
                    vm.dadosPagamento.parcelas = maxParcelas;
                    focusElement('campo-parcela');
                });
                return false;
            }
    
            if (vm.dadosPagamento.parcelas > 1 && valMinParc > 0) {
                let valorParcelas = vm.dadosPagamento.valorPago / vm.dadosPagamento.parcelas;
                if (valorParcelas < valMinParc) {
                    msUtils.error("Cada parcela deve possuir valor mínimo de " + $filter('currency')(valMinParc)).then(() => {
                        focusElement('campo-parcela');
                    });
                    return false;
                }
            }

            return true;
        }
    
        function validaVencimento(){
            if (!vm.formaSelecionada) { return false; }

            var dataVencimento = moment(vm.dadosPagamento.vencimento, "DD/MM/YYYY");
    
            if (!dataVencimento.isValid()) {
                msUtils.error("Você deve informar uma data válida!").then(() => {
                    vm.dadosPagamento.vencimento = moment();
                    focusElement('campo-vencimento');
                });
                return false;
            }
            if (dataVencimento.isBefore(moment(), 'day')) {
                msUtils.error("Você deve informar uma data maior ou igual a hoje!").then(() => {
                    vm.dadosPagamento.vencimento = moment();
                    focusElement('campo-vencimento');
                });
                return false;
            }
            return true;
        }
    
        function validaValor(){
    
            if (vm.dadosPagamento.valorPago <= 0) {
                msUtils.error("Valor informado deve ser mais que 0!").then(() => {
                    focusElement('campo-valor');
                });
                return false;
            }
    
            if (vm.dadosPagamento.valor > vm.dadosPagamento.valorAPagar && !vm.formaSelecionada.troco) {
                msUtils.error("Esta forma de pagamento não permite troco!").then(() => {
                    focusElement('campo-valor');
                });
                return false;
            }
    
            return true;
    
        }

        function confirmaCancelar() {
            if (vm.cupomVenda.pagamentosArr.length > 0) {
                msUtils.confirmar("Deseja voltar aos itens do cupom?", "Cancelar pagamento", true).then((result) => {
                    cancelar();
                });
            } else {
                cancelar();
            }
        }

        function adicionarPagamento() {
            if (!vm.formaSelecionada) {
                msUtils.alerta("Você deve selecionar uma forma de pagamento!");
                return;
            }
    
            if (!validaParcelas()) {
                return;
            }
    
            if (!validaVencimento()) {
                return;
            }
    
            if (!validaValor()) {
                return;
            }
    
            if (vm.formaSelecionada.solicitaCliente && !vm.formaSelecionada.aceitaClienteEmBranco && !vm.dadosPagamento.cpfCnpj) {
                msUtils.error("Cliente deve ser informado!").then(() => {
                    focusElement('campo-documento');
                });
                return;
            }
    
            if (vm.formaSelecionada.solicitaCliente && !vm.formaSelecionada.aceitaClienteEmBranco && vm.formaSelecionada.clienteSoCpfCnpj && !msUtils.validaCpfCnpj(vm.dadosPagamento.cpfCnpj)) {
                msUtils.error("Você deve informar um CPF/CNPJ válido do cliente!").then(() => {
                    focusElement('campo-documento');
                });
                return;
            }

            var focus = "";

            async.waterfall([
                function consultaCliente(next){
                    if (vm.formaSelecionada.consultaCliente && vm.dadosPagamento.cpfCnpj) {
                        $rootScope.Api.consultaCliente(vm.dadosPagamento.cpfCnpj).then((clientes) => {
                            if (clientes.length == 0) {
                                focus = "campo-documento";
                                next(new Error("Cliente não encontrado!"));
                            } else {
                                var cliente = clientes[0];
                                let arrBloqueios = [];
                                if (cliente.cliValorCompras > 0 && cliente.cliLimiteDisponivel < vm.dadosPagamento.valorPago) {
                                    arrBloqueios.push("[BL]");
                                }
                                if (cliente.cadastroBloqueado) {
                                    arrBloqueios.push("[BC]");
                                }
                                if (cliente.cobrancasAtrasadas > 0) {
                                    arrBloqueios.push("[BA]");
                                }
                                if (arrBloqueios.length > 0) {
                                    msUtils.confirmar("Cliente possui os seguintes bloqueios: " + arrBloqueios.join(", ")+", deseja continuar com a venda?", "Atenção!", false).then((res) => {
                                        next(null, res);
                                    }).finally(() => {
                                        next(null, false);
                                    })
                                } else {
                                    vm.dadosPagamento.cliente = cliente;
                                    vm.cupomVenda.clienteObj = cliente; 
                                    next(null, true);
                                }       
                            }
                        }).catch((err) => {
                            if (vm.configInstance.contingenciaOffline) {
                                err.message = "Você está trabalhando em modo de contingência off-line, não é possível realizar consultar o cliente!";
                            }
                            next(err);
                        });
                    }else{
                        next(null, true);
                    }
                }
            ], (err, podeFinalizar) => {
                if(err){
                    msUtils.error(err.message).then(() => {
                        focusElement(focus);
                    });
                    return;
                }

                if(!podeFinalizar){
                    return;
                }

                var pagamentoInstance = $rootScope.Db.PagamentoRealizado.build({
                    idPagamento: vm.formaSelecionada.idPagamento,
                    dataPagamento: new Date(),
                    dataVencimento: moment(vm.dadosPagamento.vencimento, "DD/MM/YYYY").toDate(),
                    parcelas: vm.dadosPagamento.parcelas || 1,
                    documentoCliente: vm.dadosPagamento.cpfCnpj,
                    valor: vm.dadosPagamento.valorPago
                });

                pagamentoInstance.pagamentoInstance = vm.formaSelecionada;
                vm.cupomVenda.addPagamento(pagamentoInstance);               
                resetPagamentoSelecionado();

                if(vm.cupomVenda.valorAPagar <= 0){
                    fecharVenda();
                }

            });
        
        }

        vm.podeFecharVenda = false;
        vm.fecharVenda = fecharVenda;

        function fecharVenda(){
            msUtils.confirmar("Confirma o fechamento desta venda?", "Fechar venda", true).then(() => {      
                $timeout(() => {        
                    msUtils.loading("Emitindo NFC-e...");
                    $scope.$apply(() => {
                        vm.podeFecharVenda = true;
                    });                    
                    vm.cupomVenda.finalizarVenda().then((cupom) => {
                        $mdDialog.hide();
                        CupomVendaClass.cupomAberto = null;
                        $timeout(() => {
                            msUtils.alerta("Venda realizada com sucesso!").then(() => {
                                $state.go('app.venda');
                            }).catch(() => {
                                $state.go('app.venda');
                            });
                        }, 100);
                    }).catch((err) => {
                        $mdDialog.hide();
                        $timeout(() => {
                            console.log(err);
                            if(err.erroAutorizacao) return msUtils.error(err.message, 'NFC-e não autorizada!');
                            var config = ConfiguracaoClass.getConfigGlobal();
                            if (!config.contingenciaOffline) {
                                return msUtils.confirmar("Problema de conexão com o servidor, deseja habilitar o modo de contingência?", "Erro!", true).then(() => {
                                    config.toggleContingencia();
                                    vm.cupomVenda.chaveAnterior = vm.cupomVenda.chaveBuscaNFCe;
                                    vm.cupomVenda.nnfAnterior = vm.cupomVenda.numeroNf;
                                    vm.cupomVenda.numeroNf = config.getProximoNNF();
                                    return vm.cupomVenda.finalizarVenda();
                                });
                            }
                        }, 100);
                    }); 
                }, 100);               
            }).catch((err) => {
                $mdDialog.hide();
                $timeout(() => {
                    if(err) return msUtils.error(err.message, 'NFC-e não autorizada!');
                    msUtils.error("Desistiu do fechamento!").then(() => {
                        cancelar();
                    });
                }, 100);
            });
        }

        function selecionouFormaPagamento(forma) {

            if(vm.podeFecharVenda) return;
            
            indexSelecionado = vm.formasPagamento.indexOf(forma);
            vm.formaSelecionada = forma;           
    
            vm.solicitaCliente = false;
            vm.solicitaParcelas = false;
            vm.parcelas = null;
    
            vm.dadosPagamento.valorPago = utils.floorNumber(vm.cupomVenda.valorAPagar);
        
            vm.dadosPagamento.vencimento = moment().format("DD/MM/YYYY");
            arrCamposNavegacao = ['campo-valor'];

            if (vm.formaSelecionada.solicitaDataVencimento) {
                arrCamposNavegacao.push('campo-vencimento');
            }

            if (vm.formaSelecionada.maxParcelas > 1) {
                vm.dadosPagamento.parcelas = 1;
                vm.solicitaParcelas = true;
                arrCamposNavegacao.push('campo-parcelas');
            }
            if (vm.formaSelecionada.solicitaCliente) {
                vm.solicitaCliente = true;
                arrCamposNavegacao.push('campo-documento');
            }

            focusElement('campo-valor');
    
        }

        function resetPagamentoSelecionado() {
            vm.formaSelecionada = null;            
            vm.dadosPagamento = {};
            indexSelecionado = -1;
            $(document.activeElement).blur();
            vm.solicitaCliente = false;
            vm.solicitaParcelas = false;
            vm.parcelas = null;
        }

        function bindKeys() {

            hotkeys.bindTo($scope)
                .add({
                    combo: 'escape',
                    description: 'Cancelar Pagamento',
                    allowIn: ['INPUT'],
                    callback: function(evt){
                        var idSelected = $(evt.target).attr('id');
                        if(arrCamposDisponiveis.indexOf(idSelected) > -1){
                            resetPagamentoSelecionado();
                        }
                    }
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'f1',
                    description: 'Cancelar Pagamento',
                    allowIn: ['INPUT'],
                    callback: confirmaCancelar
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'f8',
                    description: 'Acréscimo/Desconto',
                    allowIn: ['INPUT'],
                    callback: vm.abrirAcrescimoDesconto
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'up',
                    description: 'up',
                    allowIn: ['INPUT'],
                    callback: function (evt) {
                        if(vm.podeFecharVenda || $rootScope.popupAberto) return;
                        var idSelected = $(evt.target).attr('id');
                        if(arrCamposDisponiveis.indexOf(idSelected) > -1){
                            return;
                        }
                        evt.preventDefault();
                        indexSelecionado -= 1;
                        if (indexSelecionado < 0) indexSelecionado = 0;
                        vm.formaSelecionada = vm.formasPagamento[indexSelecionado];
                        pagamentoScrollRow('up');
                    }
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'down',
                    description: 'down',
                    allowIn: ['INPUT'],
                    callback: function (evt) {
                        if(vm.podeFecharVenda || $rootScope.popupAberto) return;
                        var idSelected = $(evt.target).attr('id');
                        if(arrCamposDisponiveis.indexOf(idSelected) > -1){
                            return;
                        }
                        evt.preventDefault();
                        indexSelecionado += 1;
                        if (indexSelecionado > (vm.formasPagamento.length - 1)) indexSelecionado = vm.formasPagamento.length - 1;
                        vm.formaSelecionada = vm.formasPagamento[indexSelecionado];
                        pagamentoScrollRow('down');
                    }
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'right',
                    description: 'right',
                    allowIn: ['INPUT'],
                    callback: function (evt) {
                        if(!vm.formaSelecionada || vm.podeFecharVenda || $rootScope.popupAberto) return;
                        var id = angular.element(evt.target).attr('id');
                        var indexCampo = arrCamposNavegacao.indexOf(id);
                        if (indexCampo == -1) indexCampo = 0;
                        indexCampo++;
                        if (indexCampo > (arrCamposNavegacao.length - 1)) indexCampo = arrCamposNavegacao.length - 1;
                        focusElement(arrCamposNavegacao[indexCampo]);
                    }
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'left',
                    description: 'left',
                    allowIn: ['INPUT'],
                    callback: function (evt) {
                        if(!vm.formaSelecionada || vm.podeFecharVenda || $rootScope.popupAberto) return;
                        var id = angular.element(evt.target).attr('id');
                        var indexCampo = arrCamposNavegacao.indexOf(id);
                        if (indexCampo == -1) indexCampo = 0;
                        indexCampo--;
                        if (indexCampo < 0) indexCampo = 0;
                        focusElement(arrCamposNavegacao[indexCampo]);
                    }
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'enter',
                    description: 'enter',
                    allowIn: ['INPUT'],
                    callback: function (evt) {   
                        if($rootScope.popupAberto) return;
                        if(vm.podeFecharVenda){
                            return vm.fecharVenda();
                        }
                        var id = angular.element(evt.target).attr('id');
                        var indexCampo = arrCamposNavegacao.indexOf(id);
                        if(indexSelecionado == -1) return;
                        if (indexCampo == -1) {
                            return selecionouFormaPagamento(vm.formasPagamento[indexSelecionado])                            
                        } else if ((indexCampo + 1) == arrCamposNavegacao.length) {
                            return adicionarPagamento();
                        } else {
                            return focusElement(arrCamposNavegacao[indexCampo+1]);
                        }
                    }
                });

            hotkeys.bindTo($scope)
                .add({
                    combo: 'f10',
                    description: 'Concluir venda',
                    allowIn: ['INPUT'],
                    callback: function () {
                        if($rootScope.popupAberto) return;
                        if(vm.podeFecharVenda){
                            vm.fecharVenda();
                        }
                    }
                });

        }

        function focusElement(id) {
            var element = angular.element("#" + id);
            if (element.length == 0) {
                element = angular.element("#" + arrCamposNavegacao[0]);
            };
            element.focus();
            var len = element.val().length;
            element[0].setSelectionRange(len, len);
        }      

        function pagamentoScrollRow(key) {
            var tableContainer = $('#table-container');
            setTimeout(() => {
                let height = tableContainer[0].clientHeight;
                let selected = tableContainer.find('.tr-selected')[0];
                if (!selected) return;
                let start = tableContainer[0].scrollTop;
                let end = (start + height) - 30;
                let startSelecionado = selected.offsetTop;// - tableContainer[0].offsetTop;
                let heightSelecionado = selected.clientHeight;
                if (startSelecionado > end || startSelecionado < start) {
                    if (key == "down") {
                        let startPosition = (startSelecionado + heightSelecionado) - height;
                        tableContainer.scrollTop(startPosition);
                    } else {
                        tableContainer.scrollTop(startSelecionado);
                    }    
                }
            }, 10);
        }

        vm.abrirAcrescimoDesconto = () => {
            if ($rootScope.popupAberto || vm.cupomVenda.pagamentosArr.length > 0) return;
            return msUtils.abrirModal({
                controller: 'AcrescimoDescontoModal',
                controllerAs: 'vm',
                hasBackdrop: true,
                fullscreen: true,
                templateUrl: 'frontEnd/main/venda/views/modalAcrescimoDesconto.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                escapeToClose: true,
                locals: {
                    finalizando: true
                }
            }).then((dados) => {
                if (dados) {
                    if (dados.tipo == 'acrescimo') {
                        if (dados.valor) {
                            vm.cupomVenda.valorAcrescimo = dados.valor;
                        } else if (dados.porcentagem) {
                            vm.cupomVenda.valorAcrescimo = vm.cupomVenda.subTotal * (dados.porcentagem / 100);
                        }
                    } else if (dados.tipo == 'desconto') {
                        if (dados.valor) {
                            vm.cupomVenda.valorDesconto = dados.valor;
                        } else if (dados.porcentagem) {
                            vm.cupomVenda.valorDesconto = vm.cupomVenda.subTotal * (dados.porcentagem / 100);
                        }
                    }
                }else{
                    vm.cupomVenda.valorDesconto = 0;
                    vm.cupomVenda.valorAcrescimo = 0;
                }
            }).finally(() => {
                bindKeys();
            });
        }

        bindKeys();

    }

})();