(function () {
    'use strict';

    angular
        .module('app.pagamento')
        .controller('CancelarPagamentoModalController', CancelarPagamentoModalController);

    /** @ngInject */
    function CancelarPagamentoModalController($filter, msUtils, $mdDialog, $scope, hotkeys) {

        var vm = this;

        hotkeys.bindTo($scope)
            .add({
                combo: 'enter',
                allowIn: ['INPUT'],
                callback: function () {
                    vm.cancelar();
                }
            });

        vm.cancelar = function(){
            if(!vm.numero){
                return msUtils.toast("Você deve informar um número!");
            }
            $mdDialog.hide(vm.numero);
        }

    }

})();