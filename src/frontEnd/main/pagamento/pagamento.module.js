(function() {
    'use strict';

    angular
        .module('app.pagamento', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider) {

        // State
        $stateProvider.state('app.pagamento', {
            url: '/pagamento',
            views: {
                'content': {
                    templateUrl: 'frontEnd/main/pagamento/views/pagamento.html',
                    controller: 'PagamentoController as vm'
                }
            },
            resolve: {
                pagamentosArr: ['$rootScope', function($rootScope){
                    return $rootScope.Db.Pagamento.getTodos();
                }]
            },
            bodyClass: 'pagamento'
        });

    }

})();