(function() {
    'use strict';

    angular
        .module('app.wizard', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider) {

        // State
        $stateProvider.state('app.wizard', {
            url: '/wizard',
            views: {
                'content': {
                    templateUrl: 'frontEnd/main/wizard/views/wizard.html',
                    controller: 'WizardController as vm'
                }
            },
            bodyClass: 'wizard'
        });

    }

})();