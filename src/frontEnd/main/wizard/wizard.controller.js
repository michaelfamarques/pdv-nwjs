(function () {
    'use strict';

    angular
        .module('app.wizard')
        .controller('WizardController', WizardController);

    /** @ngInject */
    function WizardController($filter, msUtils, $rootScope, hotkeys, $scope, $mdDialog, $state, $timeout) {

        var vm = this;
        var ConfigClass = $rootScope.Db.Configuracao;
        const ConfigInstance = $rootScope.Db.Configuracao.build({});

        vm.login = {};
        vm.config = ConfigInstance;
        vm.usuarioErp = null;
        vm.configPdv = {};

        var flagAutomatica = false;
        vm.setDados = function () {
            flagAutomatica = true;
            vm.login = {
                email: 'michael.famarques+pdv@gmail.com',
                senha: '123456'
            };
            vm.configPdv = {
                pdv: 64
            };
            setTimeout(() => {
                vm.continuar();
            }, 50);
        }

        vm.concluir = function () {
            return vm.config.save().then(() => {
                $rootScope.configInstance = vm.config;
                ConfigClass.setConfigGlobal(vm.config);
                $state.go('app.login');
            });
        }

        vm.arrImpressoras = $rootScope.App.getImpressoras();
        vm.driversImpressoras = ["TP280", "BEMATECH", "DARUMA", "EPSON", "ELGIN", "GP-U80300III", "PDF"];

        vm.continuar = function () {
            switch ($scope.msWizard.selectedIndex) {
                case 0:
                    vm.step1.$setSubmitted();
                    vm.step1.$setDirty();
                    if (vm.step1.$valid) {
                        msUtils.loading("Logando usuário...");
                        $rootScope.Api.logaUsuario(vm.login.email, vm.login.senha).then((result) => {
                            $mdDialog.hide();
                            vm.usuarioErp = result;
                            if (flagAutomatica) {
                                vm.configPdv.filial = result.filiais[0];
                            }
                            $timeout(() => {
                                $scope.msWizard.nextStep();
                                if (flagAutomatica) {
                                    setTimeout(() => {
                                        vm.continuar();
                                    }, 500);
                                }
                            }, 100);
                        }).catch((err) => {
                            $mdDialog.hide();
                            msUtils.errorPromise(err);
                        });
                    }
                    break;
                case 1:
                    vm.step2.$setSubmitted();
                    vm.step2.$setDirty();
                    if (vm.step2.$valid) {
                        msUtils.loading("Aguarde...");
                        $rootScope.Api.initFilialEPdv(vm.configPdv.filial.empresa, vm.configPdv.filial.id, vm.usuarioErp.authorization, vm.configPdv.pdv).then((token, pdv) => {
                                $mdDialog.hide();
                                vm.config.serie = 'AV0' + _.padStart(vm.configPdv.filial.cpfCnpj) + _.padStart(vm.configPdv.pdv, 3, '0');
                                vm.config.apiKey = token;
                                vm.config.numPdv = vm.configPdv.pdv;
                                vm.config.dadosFilial = vm.configPdv.filial;
                                vm.config.dadosPdv = pdv;
                                vm.config.idFilial = vm.configPdv.filial.id;
                                vm.config.idEmpresa = vm.configPdv.filial.empresa;
                                $scope.$apply(() => $scope.msWizard.nextStep());
                                return $rootScope.Api.sincronizaDados(vm.config);
                            })
                            .then(() => {
                                $scope.$apply(() => $scope.msWizard.nextStep());
                            })
                            .catch(msUtils.errorPromise);
                    }
                    break;
                case 3:
                    vm.step3.$setSubmitted();
                    vm.step3.$setDirty();
                    if (vm.step3.$valid) {
                        $scope.msWizard.nextStep();
                    }
                    break;
            }
        }

    }

})();