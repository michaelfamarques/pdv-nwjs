(function () {
    'use strict';

    angular
        .module('novoPdv')
        .controller('ConfiguracoesController', ConfiguracoesController);

    /** @ngInject */
    function ConfiguracoesController($filter, msUtils, $rootScope, hotkeys, $scope, $mdDialog, $state, $timeout) {

        var vm = this;

        var CupomVendaClass = $rootScope.Db.CupomVenda;

        vm.configInstance = $rootScope.configInstance;

        vm.fecharPDV = function(){
            nw.App.quit();
        }

        vm.configurarImpressora = function () {
            $mdDialog.hide();
            $timeout(() => {
                msUtils.abrirModal({
                    controller: 'ImpressoraController',
                    controllerAs: 'vm',
                    hasBackdrop: true,
                    fullscreen: true,
                    templateUrl: 'frontEnd/main/impressora/views/impressora.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    escapeToClose: false,
                    locals: {
                        configInstance: vm.configInstance
                    }
                }).then(() => {
                    vm.configInstance.save();
                })
            }, 300);
        }

        vm.sincronizarDados = function () {
            $mdDialog.hide();
            $timeout(() => {
                msUtils.loading("Sincronizando informações...");
                $rootScope.App.sync().then(() => {
                        $rootScope.$broadcast('loadCupom');
                        $mdDialog.hide();
                        $timeout(() => {
                            msUtils.alerta("Dados sincronizados com sucesso!");
                        });
                    })
                    .catch((err) => {
                        $mdDialog.hide();
                        $timeout(() => {
                            msUtils.error(err.message, 'Erro ao sincronizar informações!');
                        });
                    });
            }, 100);
        }

        vm.trocarDeOperador = function () {
            $mdDialog.hide();
            $state.go('app.login');
        }

        vm.resetarPdv = function () {
            $mdDialog.hide();
            $timeout(() => {
                if ($rootScope.Db.CupomVenda.cupomAberto) {
                    msUtils.alerta("Existe venda em andamento!");
                    return;
                }
                msUtils.confirmar("Todos os dados não enviados ao NovoERP serão perdidos. Deseja realmente reiniciar o PDV?", "Reiniciar PDV").then(() => {
                    $timeout(() => {
                        $rootScope.App.resetar().then(() => {
                                $state.go('app.wizard');
                            })
                            .catch((err) => {
                                msUtils.error(err.message, 'Erro ao resetar o PDV!');
                            });
                    });
                });
            });
        }

        vm.toggleContingencia = function () {

            $mdDialog.hide();

            $timeout(() => {

                if (vm.configInstance.contingenciaOffline) {
                    return msUtils.confirmar("Deseja desabilitar o modo de contingência OFF-LINE?", 'Contingência OFF-LINE')
                        .then((result) => {
                            msUtils.loading('Verificando conexão...');
                            return $rootScope.Api.ping();
                        }).then((result) => {
                            $mdDialog.hide();
                            return Promise.resolve(result);
                        }).then((result) => {                        
                            if (result) {
                                vm.configInstance.toggleContingencia();
                                return msUtils.alerta("Contingênca offline desativada, enviando notas emitidas em contingência...");
                            } else {
                                msUtils.alerta("Não foi possível desativar o modo de contingência, PDV ainda não possui conexão com o NovoERP!");
                                return Promise.reject();
                            }                            
                        }).then((result) => {
                            return CupomVendaClass.getVendasEmContingencia().then((cupomArr) => {
                                if (cupomArr.length > 0) {
                                    return msUtils.abrirModal({
                                        controller: 'EnviarContingenciaModalController',
                                        controllerAs: 'vm',
                                        hasBackdrop: true,
                                        fullscreen: true,
                                        templateUrl: 'frontEnd/main/venda/views/modalEnviandoNFeContingencia.html',
                                        parent: angular.element(document.body),
                                        clickOutsideToClose: false,
                                        escapeToClose: false,
                                        locals: {
                                            cupomArr: cupomArr
                                        }
                                    });
                                }
                            });
                        }).catch((err) => {
                            if (err) {
                                msUtils.error(err.message);
                            }
                        });
                } else {
                    return msUtils.confirmar("Deseja habilitar o modo de contingência OFF-LINE?", 'Contingência OFF-LINE')
                        .then((result) => {
                            msUtils.loading('Verificando conexão...');
                            return $rootScope.Api.ping();
                        }).then((result) => {
                            $mdDialog.hide();
                            $timeout(() => {
                                if (result) {
                                    msUtils.alerta("Acesso com o servidor OK! Modo de Contingência OFF-LINE negado!");
                                } else {
                                    vm.configInstance.toggleContingencia();
                                    msUtils.alerta("Contingênca offline ativada!");
                                }
                            });
                        }).catch((err) => {
                            if (err) {
                                msUtils.error(err.message);
                            }
                        });
                }

            }, 100);

        }

        hotkeys.bindTo($scope)
            .add({
                combo: 'down',
                description: 'down',
                allowIn: ['INPUT'],
                callback: function (evt) {
                    var arrBotoesConfig = $('.btn-config');
                    var focused = arrBotoesConfig.filter(':focus');
                    if (focused.length == 0) {
                        arrBotoesConfig.get(0).focus();
                    } else {
                        var index = arrBotoesConfig.index(focused);
                        index++;
                        if (index >= arrBotoesConfig.length) {
                            index = 0;
                        }
                        arrBotoesConfig.get(index).focus();
                    }
                }
            });

        hotkeys.bindTo($scope)
            .add({
                combo: 'up',
                description: 'up',
                allowIn: ['INPUT'],
                callback: function (evt) {
                    var arrBotoesConfig = $('.btn-config');
                    var focused = arrBotoesConfig.filter(':focus');
                    if (focused.length == 0) {
                        arrBotoesConfig.get(arrBotoesConfig.length - 1).focus();
                    } else {
                        var index = arrBotoesConfig.index(focused);
                        index--;
                        if (index < 0) {
                            index = arrBotoesConfig.length - 1;
                        }
                        arrBotoesConfig.get(index).focus();
                    }
                }
            });

    }

})();