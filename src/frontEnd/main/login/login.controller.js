(function ()
{
    'use strict';

    angular
        .module('app.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($state, $rootScope, msUtils, $timeout)
    {
        const path = require("path");
        const UsuarioClass = $rootScope.Db.Usuario;

        // Data
        var vm = this;
        vm.login = login;
        vm.configInstance = $rootScope.configInstance;

        vm.form = {
            operador: '',
            senha: ''
        };

        vm.logarAutomatico = function(){
            vm.form = {
                operador: '1',
                senha: '1'
            };
            $timeout(() => {
                login();  
            }, 50);            
        }

        vm.backgroundUrl = "";
        if(!!vm.configInstance.dadosFilial.logomarca){
            vm.backgroundUrl = 'url(\'file://'+path.join(nw.App.dataPath, path.basename(vm.configInstance.dadosFilial.logomarca)).replace(/\\/g, "/")+'\');';
        }       

        setTimeout(() => {
            $('#campoLogin').focus();
        }, 100);
        
        // Methods
        function login(){

            vm.Form.$submitted = true;
            if (vm.Form.$invalid) {
                msUtils.toast('Você deve preencher os campos com erro!');
                return false;
            }

            UsuarioClass.logar(vm.form.operador, vm.form.senha).then((usuario) => {
                $rootScope.Db.Configuracao.getConfigGlobal().setOperadorLogado(usuario);
                $state.go('app.venda');
            }).catch((e) => {
                msUtils.error(e.message, 'Erro ao logar!');
            });     
            
        }

    }
})();