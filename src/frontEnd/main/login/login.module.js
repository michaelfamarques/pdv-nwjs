(function() {
    'use strict';

    angular
        .module('app.login', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider) {

        // State
        $stateProvider.state('app.login', {
            url: '/login',
            views: {
                'content': {
                    templateUrl: 'frontEnd/main/login/views/login.html',
                    controller: 'LoginController as vm'
                }
            },
            bodyClass: 'login'
        });

    }

})();