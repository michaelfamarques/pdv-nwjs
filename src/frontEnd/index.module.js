(function() {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
    .module('novoPdv', [

            // Modulos de Terceiros
            'idf.br-filters',
            'ngMask',
            'cfp.hotkeys',
            'md.data.table',

            // Modulos do Tema
            'app.core',
            'app.login',
            'app.venda',
            'app.pagamento',
            'app.wizard'

        ]);
})();