(function ()
{
    'use strict';

    angular
    .module('novoPdv')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider){

        // State definitions
        $stateProvider
            .state('app', {
                abstract: true,
                views   : {
                    'main@'         : {
                        templateUrl: 'frontEnd/core/layouts/full.html',
                        controller : 'MainController as vm'
                    }
                }
            });

        $stateProvider
            .state('app.logout', {
                url: '/logout',
                views: {
                    'content@app': {
                        controller: ['$state', function($state){
                            $state.go('app.login');
                        }]
                    }
                }
            });
    }

})();
