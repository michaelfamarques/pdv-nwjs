(function () {
    'use strict';

    angular
        .module('novoPdv')
        .run(runBlock);

    /** @ngInject */
    function runBlock($rootScope, $http, $timeout, $state, msUtils, hotkeys) {

        $rootScope.App = require("backend/app");
        $rootScope.Db = $rootScope.App.db;
        $rootScope.Api = $rootScope.App.api;
        

        $rootScope.popupAberto = false;

        var ConfigClass = $rootScope.Db.Configuracao;

        // Activate loading indicator
        var stateChangeStartEvent = $rootScope.$on('$stateChangeStart', function () {
            $rootScope.loadingProgress = true;
        });

        // De-activate loading indicator
        var stateChangeSuccessEvent = $rootScope.$on('$stateChangeSuccess', function () {
            $timeout(function () {
                $rootScope.loadingProgress = false;
            });
        });

        $http.defaults.transformResponse.push(function (data) {
            $timeout(function () {
                $rootScope.loadingProgress = false;
            });
            return data;
        });

        // Cleanup
        $rootScope.$on('$destroy', function () {
            stateChangeStartEvent();
            stateChangeSuccessEvent();
        });

        $rootScope.configInstance = ConfigClass.getConfigGlobal();
        if($rootScope.configInstance){
            $state.go("app.login");
        }else{
            $state.go("app.wizard");
        }

    }

    function initShortcuts(hotkeys) {

        hotkeys.add({
            combo: 'f3',
            allowIn: ['INPUT'],
            callback: function () {
                nw.App.quit()
            }
        });

    }
})();