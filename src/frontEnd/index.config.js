(function () {
    'use strict';

    angular
        .module('novoPdv')
        .config(config);

    /** @ngInject */
    function config($mdDateLocaleProvider, $mdThemingProvider) {

        $mdThemingProvider.theme('default')
            .primaryPalette('grey');

        $mdDateLocaleProvider.months = [
            'janeiro',
            'fevereiro',
            'março',
            'abril',
            'maio',
            'junho',
            'julho',
            'agosto',
            'setembro',
            'outubro',
            'novembro',
            'dezembro'
        ];

        $mdDateLocaleProvider.shortMonths = [
            'jan',
            'fev',
            'mar',
            'abr',
            'maio',
            'jun',
            'jul',
            'ago',
            'set',
            'out',
            'nov',
            'dez'
        ];

        $mdDateLocaleProvider.days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
        $mdDateLocaleProvider.shortDays = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];

        $mdDateLocaleProvider.msgCalendar = 'Calendário';
        $mdDateLocaleProvider.msgOpenCalendar = 'Abrir Calendário';

        $mdDateLocaleProvider.parseDate = function (dateString) {
            var m = moment(dateString, 'DD/MM/YYYY', true);
            return m.isValid() ? m.toDate() : new Date("Invalid Date");
        };

        $mdDateLocaleProvider.formatDate = function (date) {
            return date ? moment(date).format('DD/MM/YYYY') : '';
        };

        moment.locale('pt-br');

        //Modifica o require original pra inserir o ../ em todos os requires de backend facilitando o dev
        var oldRequire = require;
        require = function () {
            if (arguments[0] && arguments[0].indexOf("backend/") > -1) {
                arguments[0] = "../" + arguments[0];
            }
            return oldRequire.apply(oldRequire, arguments);
        }

        //Hack, redireciona os logs do contexto do node para o console log do contexto do browser
        nw.global.console = console;

    }

})();