const fs = require('fs-extra');
const path = require('path');

var isWin = process.platform === "win32";

if(isWin){
    fs.copySync(path.join(__dirname, 'libs/sqlite3/node-webkit-v0.26.4-win32-x64'), path.join('node_modules/sqlite3/lib/binding/node-webkit-v0.26.4-win32-x64'));
    fs.copySync(path.join(__dirname, 'libs/printer/node-webkit-v0.26.4-win32-x64'), path.join('node_modules/printer/build'));
}
