const math = require('mathjs');

module.exports.dec2 = function(valor) {
    if(valor && Number(valor)){
        return math.format(math.round(valor, 2), { notation: 'fixed', precision: 2 });
    }
    return "0.00";
}

module.exports.dec3 = function(valor) {
    if(valor && Number(valor)){
        return math.format(math.round(valor, 3), { notation: 'fixed', precision: 3 });
    }
    return 0.000;
}
module.exports.dec4 = function(valor) {
    if(valor && Number(valor)){
        return math.format(math.round(valor, 4), { notation: 'fixed', precision: 4 });
    }
    return 0.000;
}

module.exports.dec9 = function(valor) {
    if(valor && Number(valor)){
        return math.format(math.round(valor, 9), { notation: 'fixed', precision: 9 });
    }
    return 0.000000000;
}
module.exports.dec10 = function(valor) {
    if(valor && Number(valor)){
        return math.format(math.round(valor, 10), { notation: 'fixed', precision: 10 });
    }
    return 0.0000000000;
}
module.exports.divide2Dec = function(x, y){
    if(x && y && Number(x) && Number(y)){
        return math.format(math.round(math.divide(x,y), 2), { notation: 'fixed', precision: 2 });
    }
    return 0.00;
}
module.exports.divide10Dec = function(x, y){
    if(x && y && Number(x) && Number(y)){
        return math.format(math.round(math.divide(x,y), 10), { notation: 'fixed', precision: 10 });
    }
    return 0.0000000000;
}

module.exports.multiplica2Dec = function(x, y){
    if(x && y && Number(x) && Number(y)){    
        return math.format(math.round(math.multiply(x,y), 2), { notation: 'fixed', precision: 2 });
    }
    return 0.00;
}

module.exports.multiplica4Dec = function(x, y){
    if(x && y && Number(x) && Number(y)){    
        return math.format(math.round(math.multiply(x,y), 4), { notation: 'fixed', precision: 4 });
    }
    return 0.00;
}