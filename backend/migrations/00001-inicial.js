'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {

        return createConfig()               
            .then(() => createGrupo())
            .then(() => createFormaPagamento())
            .then(() => createUsuario())
            .then(() => createGrupoUsuario())
            .then(() => createProduto())
            .then(() => createCupomVenda())
            .then(() => createProdutoVendido())
            /*
            .then(() => createPagamentoRealizado());
            */

        function createConfig() {
            return queryInterface.createTable('configuracao', {
                id_configuracao: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false
                },
                api_key: {
                    type: DataTypes.STRING
                },
                id_empresa: {
                    type: DataTypes.BIGINT
                },
                id_filial: {
                    type: DataTypes.BIGINT
                },
                num_pdv: {
                    type: DataTypes.BIGINT
                },
                serie: {
                    type: DataTypes.STRING
                },
                id_operador_logado: {
                    type: DataTypes.BIGINT
                },
                config_geral: {
                    type: DataTypes.JSON
                },
                dados_pdv: {
                    type: DataTypes.JSON
                },
                dados_filial: {
                    type: DataTypes.JSON
                },
                certificado: {
                    type: DataTypes.STRING
                }
            });
        }

        

        function createGrupo(){
            return queryInterface.createTable('grupo', {
                id_grupo: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                descricao: {
                    type: Sequelize.STRING
                }
            });
        }

        
        function createFormaPagamento(){
            return queryInterface.createTable('forma_pagamento', {
                id_pagamento: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                codigo: {
                    type: Sequelize.INTEGER
                },
                descricao: {
                    type: Sequelize.STRING
                },
                max_parcelas: {
                    type: Sequelize.INTEGER
                },
                valor_minimo_parcela: {
                    type: Sequelize.FLOAT
                },
                troco: {
                    type: Sequelize.BOOLEAN
                },
                solicita_cliente: {
                    type: Sequelize.BOOLEAN
                },
                aceita_cliente_em_branco: {
                    type: Sequelize.BOOLEAN
                },
                cliente_so_cpf_cnpj: {
                    type: Sequelize.BOOLEAN
                },
                consulta_cliente: {
                    type: Sequelize.BOOLEAN
                }
            });
        }

        function createUsuario(){
            return queryInterface.createTable('usuario', {
                id_usuario: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                codigo: {
                    type: Sequelize.INTEGER
                },
                nome: {
                    type: Sequelize.STRING
                },
                senha: {
                    type: Sequelize.STRING
                },
                cpf: {
                    type: Sequelize.STRING
                },
                id_grupo_usuario: {
                    type: Sequelize.INTEGER
                }
            });
        }

        function createGrupoUsuario(){
            return queryInterface.createTable('grupo_usuario', {
                id_grupo_usuario: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                codigo: {
                    type: Sequelize.INTEGER
                },
                descricao: {
                    type: Sequelize.STRING
                },
                abrir_operador: {
                    type: Sequelize.BOOLEAN
                },
                fechar_operador: {
                    type: Sequelize.BOOLEAN
                },
                retorno_operador: {
                    type: Sequelize.BOOLEAN
                },
                saida_operador: {
                    type: Sequelize.BOOLEAN
                },
                cancela_cupom: {
                    type: Sequelize.BOOLEAN
                },
                cancela_item: {
                    type: Sequelize.BOOLEAN
                },
                cliente_impedimento: {
                    type: Sequelize.BOOLEAN
                },
                contingencia_offline: {
                    type: Sequelize.BOOLEAN
                },
                desconto_item: {
                    type: Sequelize.BOOLEAN
                },
                produto_bloqueado: {
                    type: Sequelize.BOOLEAN
                },
            });
        }

        function createProduto(){
            return queryInterface.createTable('produto', {
                cfop: {
                    type: Sequelize.STRING
                },
                codigo_ean: {
                    type: Sequelize.STRING,
                    unique: true
                },
                codigo_produto: {
                    type: Sequelize.INTEGER
                },
                origem: {
                    type: Sequelize.INTEGER
                },
                id_erp: {
                    type: Sequelize.INTEGER
                },
                descricao: {
                    type: Sequelize.STRING
                },
                ncm: {
                    type: Sequelize.STRING
                },
                ex_ncm: {
                    type: Sequelize.STRING
                },
                pesavel: {
                    type: Sequelize.BOOLEAN
                },
                venda_fracionada: {
                    type: Sequelize.BOOLEAN
                },
                produto_suspenso: {
                    type: Sequelize.BOOLEAN
                },
                preco_venda: {
                    type: Sequelize.FLOAT
                },
                quantidade_min_venda: {
                    type: Sequelize.FLOAT
                },
                quantidade_max_venda: {
                    type: Sequelize.FLOAT
                },
                tributacao: {
                    type: Sequelize.JSON
                },
                promocoes: {
                    type: Sequelize.JSON
                },
                unidade: {
                    type: Sequelize.STRING
                },
                id_grupo: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'grupo',
                        key: 'id_grupo'
                    },
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                },
            });
        }

        
        function createCupomVenda(){
            return queryInterface.createTable('cupom_venda', {
                id_cupom: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER
                },
                status: {
                    type: Sequelize.INTEGER
                },
                codigo_cupom: {
                    type: Sequelize.INTEGER
                },
                concluida: {
                    type: Sequelize.BOOLEAN
                },
                a_prazo: {
                    type: Sequelize.BOOLEAN
                },
                data_vencimento: {
                    type: Sequelize.DATE
                },
                data_venda: {
                    type: Sequelize.DATE
                },
                documento_cliente: {
                    type: Sequelize.STRING
                },
                sub_total: {
                    type: Sequelize.FLOAT
                },
                valor_total: {
                    type: Sequelize.FLOAT
                },
                valor_desconto: {
                    type: Sequelize.FLOAT
                },
                valor_pago: {
                    type: Sequelize.FLOAT
                },
                valor_troco: {
                    type: Sequelize.FLOAT
                },
                valor_acrescimo: {
                    type: Sequelize.FLOAT
                },
                id_usuario: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'usuario',
                        key: 'id_usuario'
                    },
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                },
            });
        }

        
        function createProdutoVendido(){
            return queryInterface.createTable('produto_vendido', {
                codigo_ean: {
                    type: Sequelize.STRING,
                    references: {
                        model: 'produto',
                        key: 'codigo_ean'
                    },
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                },
                id_cupom: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'cupom_venda',
                        key: 'id_cupom'
                    },
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                },
                numero: {
                    type: Sequelize.INTEGER
                },
                valor_desconto: {
                    type: Sequelize.FLOAT
                },
                valor_total: {
                    type: Sequelize.FLOAT
                },
                quantidade: {
                    type: Sequelize.FLOAT
                },
                valor_unitario: {
                    type: Sequelize.FLOAT
                },
                valor_acrescimo: {
                    type: Sequelize.FLOAT
                },
                valor_desconto_rateado: {
                    type: Sequelize.FLOAT
                },
                valor_acrescimo_rateado: {
                    type: Sequelize.FLOAT
                }
            });
        }

        /*
        function createPagamentoRealizado(){
            return queryInterface.createTable('pagamento_realizado', {
                id_pagamento: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'pagamento',
                        key: 'id_pagamento'
                    },
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                },
                id_cupom: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'cupom_venda',
                        key: 'id_cupom'
                    },
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                },
                data_pagamento: {
                    type: Sequelize.DATE
                },
                data_vencimento: {
                    type: Sequelize.DATE
                },
                pagamento_realizado: {
                    type: Sequelize.BOOLEAN
                },
                valor: {
                    type: Sequelize.FLOAT
                }
            });
        }*/

    },
    down: (queryInterface, Sequelize) => {

    }
};