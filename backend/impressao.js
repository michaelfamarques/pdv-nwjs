const _ = require('lodash');
const moment = require('moment');
const math = require('mathjs');
const app = require('./app');
const ChaveAcesso = require('./chave-acesso');
const pdf = require('./pdf');
const printer = require('printer');
const fs = require('fs');
const path = require('path');
const logger = require('./logger');
const async = require('async');
const cp = require('child_process');

const drive = require('./config/driveImpressora.json');

class ImpressaoService {

    constructor(cupom) {
        this.config = app.db.Configuracao.getConfigGlobal();
        this.cupom = cupom;

        this.driveImpressora = drive[this.config.driveImpressora || 'BEMATECH'];
        this.colunas = Number(this.driveImpressora.Colunas);
        this.colunasCompactadas = Number(this.driveImpressora.Colunas1);

        if(this.config.driveImpressora == "PDF"){
            this.quebraLinha = "{{QuebraLinha}}";
        }else{
            this.quebraLinha = String.fromCharCode(10) + '' + String.fromCharCode(13);
        }        
        this.space = ' ';
        this.traco = _.repeat(this.cmd("Traco"), this.colunas);

        this.chave = '';
    }

    cmd(comando) {
        comando = this.driveImpressora[comando];
        if(!comando) return;
        if(this.config.driveImpressora == "PDF"){
            return comando;
        }        
        var comandos = _.split(comando, '|');        
        var linha = '';
        comandos.forEach((e) => {
            linha += String.fromCharCode(Number(e));
        }, this);
        return linha;
    }

    cabecalho() {
        var conteudo = '' + this.quebraLinha;

        if (_.get(this.config, 'dadosFilial.nome', '').length <= this.colunas) {
            conteudo += this.cmd("AlinhaCentro") + this.cmd("AlturaDuplaOn") + this.cmd("NegritoOn");
            conteudo += _.get(this.config, 'dadosFilial.nome', '');
            conteudo += this.cmd("AlturaDuplaOff") + this.cmd("NegritoOff") + this.quebraLinha;
        } else {
            conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + this.cmd("AlturaDuplaOn") + this.cmd("NegritoOn");
            conteudo += _.get(this.config, 'dadosFilial.nome', '');
            conteudo += this.cmd("CondensadoOff") + this.cmd("AlturaDuplaOff") + this.cmd("NegritoOff") + this.quebraLinha;
        }
        conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + 'CNPJ:' + this.mascaraCnpj(_.get(this.config, 'dadosFilial.cpfCnpj', '')) + this.quebraLinha;
        conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + _.get(this.config, 'dadosFilial.tipoLogradouro', '') + ' ' + _.get(this.config, 'dadosFilial.descricaoEndereco', '') + ', ' + _.get(this.config, 'dadosFilial.numeroEndereco', '') + " - " + _.get(this.config, 'dadosFilial.nomeBairro', '') + this.quebraLinha;
        conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + _.trim(_.get(this.config, 'dadosFilial.nomeCidade', '')) + "-" + _.get(this.config, 'dadosFilial.uf', '') + " - " + _.get(this.config, 'dadosFilial.cep', '') + this.quebraLinha;
        conteudo += this.cmd("CondensadoOff") + this.cmd("AlinhaCentro") + this.traco + this.quebraLinha;
        conteudo += this.cmd("FonteNormal") + this.quebraLinha;

        return conteudo;
    }

    rodape() {
        var conteudo = "";
        var tmp = '';
        var tmp1 = '';
        conteudo += this.cmd("CondensadoOff") + this.cmd("AlinhaCentro") + this.traco + this.quebraLinha;
        tmp = "PDV.: " + this.config.numPdv;
        tmp1 = "Serie: " + (this.config.numPdv || '');
        conteudo += "PDV.: " + this.config.numPdv + _.repeat(this.space, this.colunas - tmp.length - tmp1.length - 1) + tmp1 + this.quebraLinha;
        conteudo += "DATA: " + moment(moment()).format("DD/MM/YYYY HH:mm:ss") + _.repeat(this.space, this.colunas - 39);

        if (this.cupom) {
            conteudo += "Seq.: " + _.padStart((this.cupom.codigoCupom || 0).toString(), 6, '0') + this.quebraLinha
        } else {
            conteudo += this.quebraLinha;
        }

        conteudo += this.cmd("AlinhaCentro") + this.traco + this.quebraLinha + this.cmd("CortaPapel");
        conteudo += this.cmd("FonteNormal") + this.quebraLinha;

        return conteudo;
    }

    cancelamento(protocolo, chaveDeAcesso) {

        if(!this.config.nomeImpressora) return;

        var conteudo = '';

        conteudo += this.cabecalho();
        conteudo += this.cmd("AlinhaCentro") + this.cmd("NegritoOn") + "CANCELAMENTO NFC-e" + this.cmd("NegritoOff") + this.quebraLinha + this.quebraLinha;
        conteudo += this.cmd("AlinhaCentro") + "CHAVE DE ACESSO CANCELADA" + this.quebraLinha;
        conteudo += this.cmd("CondensadoOn") + chaveDeAcesso + this.cmd("CondensadoOff") + this.quebraLinha + this.quebraLinha;
        conteudo += this.cmd("AlinhaCentro") + this.cmd("FonteNormal") + "PROTOCOLO DE CANCELAMENTO" + this.quebraLinha;
        conteudo += protocolo + this.cmd("AlinhaEsquerda") + this.quebraLinha;
        conteudo += this.rodape();

        this.print(conteudo);

    }

    danfe(nfc) {

        if(!this.config.nomeImpressora) return;

        var me = this;

        var autorizacao = this.cupom.autorizacao;

        var conteudo = '';

        var nfcRaiz = nfc.NFe.infNFe;

        var objChave = {
            chave: nfcRaiz.attr.Id.replace('NFe', '')
        }
        var chave = new ChaveAcesso(objChave);

        var cliente = nfcRaiz.dest || null;

        var numeroVias = 1;
        var contingencia = nfcRaiz.ide.tpEmis == 9 ? true : false;
        var via = 1;
        var itens = 0;

        conteudo += me.cabecalho();

        //Corpo da danfe
        conteudo += me.cmd("AlinhaCentro") + me.cmd("CondensadoOn") + me.cmd("NegritoOn") + "DOCUMENTO AUXILIAR DA NOTA FISCAL DE CONSUMIDOR ELETRÔNICA" + me.cmd("CondensadoOff") + me.cmd("NegritoOff") + me.quebraLinha;

        if (contingencia) {
            conteudo += _.repeat(me.cmd("Traco"), me.colunas) + me.cmd("NegritoOn") + me.quebraLinha;
            conteudo += me.cmd("AlinhaCentro") + me.cmd("NegritoOn") + "EMITIDA EM CONTINGENCIA" + me.quebraLinha;
            conteudo += me.cmd("AlinhaCentro") + me.cmd("CondensadoOn") + me.cmd("NegritoOn") + "Pendente de autorizacao" + me.cmd("NegritoOff") + me.cmd("CondensadoOff") + me.quebraLinha;
            conteudo += me.cmd("AlinhaEsquerda") + _.repeat(me.cmd("Traco"), me.colunas) + me.quebraLinha;
        }

        switch (this.config.driveImpressora) {
            case 'EPSON':
                conteudo += me.cmd("AlinhaEsquerda") +  "ITEM| COD |        DESC         |QTD| UN |VL UNIT.|   VL TOTAL  " + me.quebraLinha;
                break;
            case 'GP-U80300III':
                conteudo += me.cmd("AlinhaEsquerda") + "ITEM| COD |        DESC         |QTD| UN |VL UNIT.|   VL TOTAL  " + me.quebraLinha;
                break;
            case 'ELGIN':
                conteudo += me.cmd("AlinhaEsquerda") + "ITEM| COD |        DESC         |QTD| UN |VL UNIT.|   VL TOTAL  " + me.quebraLinha;
                break;
            case 'BEMATECH':
                conteudo += me.cmd("AlinhaEsquerda") + "ITEM| COD | DESC | QTD | UN | VL UNIT |  VL TOTAL" + me.quebraLinha;
                break;
            case 'DARUMA':
                conteudo += me.cmd("AlinhaEsquerda") + "ITEM| COD | DESC | QTD | UN | VL UNIT.| VL TOTAL" + me.quebraLinha;
                break;
            case 'PDF':
                conteudo += me.cmd("AlinhaCentro") + me.cmd("FontePequena") + "ITEM | COD | DESC | QTD | UN | VL UNIT.| VL TOTAL" + me.cmd("FonteNormal") + me.quebraLinha;
                break;
            default:
                break;
        }
        conteudo += _.repeat(me.cmd("Traco"), me.colunas) + me.cmd("CondensadoOn") + me.quebraLinha;

        nfcRaiz.det.forEach(function (item) {
            itens += 1;
            item = item.prod;
            var produto = '';
            var valores = '';
            var total = '';
            var tamanhoLinha = 0;

            conteudo += me.cmd("AlinhaEsquerda");

            if (me.format(item.qCom, 3).toString().substr(-3) == '000')
                valores += me.format(item.qCom, 0) + me.space;
            else
                valores += me.format(item.qCom, 3) + me.space;

            valores += item.uCom + me.space + 'X' + me.space;
            valores += me.format(item.vUnCom, 2) + me.space + '=' + me.space;

            total = me.format(item.vProd, 2);

            if(this.config.driveImpressora == "PDF"){

                conteudo += me.cmd("AlinhaEsquerda") + _.trim(_.padStart(itens.toString(), 3, '0')) + me.space + (item.cEAN || _.padStart(item.cProd, 14, '0')) + me.space + _.trim(item.xProd) + me.quebraLinha;
                conteudo += me.cmd("AlinhaDireita") + valores + total + this.quebraLinha;

                if (item.vDesc > 0) {
                    var tmp = "Desconto no Item: " + me.format(item.vDesc, 2);
                    conteudo += me.cmd("AlinhaDireita") + tmp + this.quebraLinha;
                }
                if (item.vOutro > 0) {
                    var tmp = "Acrescimo no Item: " + me.format(item.vOutro, 2);
                    conteudo += me.cmd("AlinhaDireita") + tmp + this.quebraLinha;
                }

            }else{
                produto = _.trim(_.padStart(itens.toString(), 3, '0')) + me.space + (item.cEAN || _.padStart(item.cProd, 14, '0')) + me.space + _.trim(item.xProd) + me.space;
                tamanhoLinha = produto.length;

                if ((produto + valores.toString() + total.toString()).length <= me.colunasCompactada) { //TODO pode quebrar a impressão aqui verificar.
                    conteudo += produto + valores;
                    conteudo += _.padStart(total, (me.colunasCompactada - 1) - (produto + valores).length, me.space) + this.quebraLinha;
                } else {
                    conteudo += produto.substr(0, tamanhoLinha) + this.quebraLinha;
                    var tmp = _.repeat(me.space, 5) + valores;
                    conteudo += tmp + _.padStart(total, (me.colunasCompactada - 1) - tmp.length, me.space) + this.quebraLinha;
                }

                if (item.vDesc > 0) {
                    var tmp = "Desconto no Item: " + me.format(item.vDesc, 2);
                    conteudo += _.repeat(me.space, this.colunasCompactada - tmp.length - 1) + tmp + this.quebraLinha;
                }
                if (item.vOutro > 0) {
                    var tmp = "Acrescimo no Item: " + me.format(item.vOutro, 2);
                    conteudo += _.repeat(me.space, this.colunasCompactada - tmp.length - 1) + tmp + this.quebraLinha;
                }
            }

        }, this);

        conteudo += this.cmd("CondensadoOff") + this.cmd("AlinhaEsquerda") + this.quebraLinha;

        var tmp = '';
        var tmp1 = '';
        var tmp2 = '';
        tmp = "QTD. TOTAL DE ITENS";
        tmp1 = me.format(itens, 0);
        conteudo += tmp + _.repeat(me.space, this.colunas - tmp.length - tmp1.length) + tmp1 + this.quebraLinha;

        tmp = "VALOR TOTAL R$";
        tmp1 = me.format(this.cupom.subTotal, 2);
        conteudo += tmp + _.repeat(me.space, this.colunas - tmp.length - tmp1.length) + tmp1 + this.quebraLinha;

        if (this.cupom.valorDesconto > 0) {
            tmp = "DESCONTO R$";
            tmp1 = me.format(this.cupom.valorDesconto, 2);
            conteudo += tmp + _.repeat(me.space, this.colunas - tmp.length - tmp1.length) + tmp1 + this.quebraLinha;
        }
        if (this.cupom.valorAcrescimo > 0) {
            " "
            tmp = "ACRESCIMO R$";
            tmp1 = me.format(this.cupom.valorAcrescimo, 2);
            conteudo += tmp + _.repeat(me.space, this.colunas - tmp.length - tmp1.length) + tmp1 + this.quebraLinha;
        }

        tmp = "VALOR A PAGAR R$";
        tmp1 = me.format(this.cupom.valorTotal, 2);
        conteudo += tmp + _.repeat(me.space, this.colunas - tmp.length - tmp1.length) + tmp1 + this.quebraLinha;

        if (!this.cupom.pagamentosArr) this.cupom.pagamentosArr = [];
        this.cupom.pagamentosArr.forEach((pagamento) => {

            var tmp = pagamento.pagamentoInstance.descricao;
            var tmp1 = me.format(pagamento.valor, 2);

            conteudo += tmp + _.repeat(me.space, this.colunas - tmp.length - tmp1.length) + tmp1 + this.quebraLinha

        }, this);


        if (this.cupom.valorTroco > 0) {
            tmp = "TROCO R$";
            tmp1 = me.format(this.cupom.valorTroco, 2);
            conteudo += tmp + _.repeat(me.space, this.colunas - tmp.length - tmp1.length) + tmp1 + this.quebraLinha;
        }
        conteudo += this.cmd("CondensadoOff") + this.cmd("AlinhaEsquerda") + this.quebraLinha;
        conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + "Consulte pela Chave de Acesso em:" + this.quebraLinha;
        conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + autorizacao.urlcons + this.quebraLinha;
        conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + this.cmd("NegritoOn") + autorizacao.chaveFormatada + this.cmd("NegritoOff") + this.cmd("CondensadoOff") + this.quebraLinha

        if (cliente) {

            conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + 'CONSUMIDOR';
            if (cliente.CNPJ) {
                conteudo += " CNPJ: " + this.mascaraCnpj(cliente.CNPJ);
            } else {
                conteudo += " CPF: " + this.mascaraCpf(cliente.CPF);
            }

            if (cliente.xNome) conteudo += "  " + cliente.xNome;
            if (cliente.enderDest) {
                conteudo += this.quebraLinha + this.cmd("AlinhaCentro");
                if (cliente.enderDest.xLgr || cliente.descricaoEndereco) conteudo += "  " + cliente.enderDest.xLgr || '';
                if (cliente.enderDest.nro) conteudo += "  " + cliente.enderDest.nro;
                if (cliente.enderDest.xBairro) conteudo += "  " + cliente.enderDest.xBairro;
                conteudo += this.quebraLinha;
                if (cliente.enderDest.xMun) conteudo += "  " + cliente.enderDest.xMun;
                if (cliente.enderDest.UF) conteudo += "  " + cliente.enderDest.UF;
                if (cliente.enderDest.CEP) conteudo += "  " + cliente.enderDest.CEP;
                conteudo += this.cmd("AlinhaEsquerda");
            }


        } else {
            conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + "CONSUMIDOR NAO IDENTIFICADO";
        }


        conteudo += this.cmd("CondensadoOff") + this.quebraLinha;

        if (via == 1) {
            conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + "NFC-e No.: " + this.cmd("NegritoOn") +
                chave.nNF + this.cmd("NegritoOff") + "  Serie: " + this.cmd("NegritoOn") +
                nfcRaiz.ide.serie + this.cmd("NegritoOff") + "   " + this.cmd("NegritoOn") + this.mascaraDDMMYYYY(nfc.NFe.infNFe.ide.dhEmi) + this.cmd("NegritoOff") + this.quebraLinha;
        }

        if (contingencia) {
            conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + (via == 1 ? "Via Consumidor" : "Via Empresa") + this.quebraLinha;
        } else {
            conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + "Protocolo de Autorizacao: " + this.cmd("NegritoOn") + autorizacao.protocolo + this.quebraLinha;
            conteudo += this.cmd("NegritoOff") + "Data de autorizacao: " + this.cmd("NegritoOn") + this.mascaraDDMMYYYY(autorizacao.dataInclusao) + this.cmd("NegritoOff") + this.quebraLinha
        }

        if (nfcRaiz.infAdfisco) conteudo += this.cmd("AlinhaCentro") + nfcRaiz.infAdfisco + this.quebraLinha;

        if (contingencia) {
            conteudo += this.cmd("CondensadoOff") + this.cmd("AlinhaCentro") + this.cmd("NegritoOn") + "EMITIDA EM CONTINGENCIA" + this.quebraLinha;
            conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + this.cmd("NegritoOn") + "Pendente de autorizacao" + this.cmd("NegritoOff") + this.quebraLinha;
        }

        if (autorizacao.ambiente == '2') {
            conteudo += this.cmd("CondensadoOn") + this.cmd("AlinhaCentro") + this.cmd("NegritoOn") + "EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL" + this.cmd("NegritoOff");
            conteudo += this.cmd("CondensadoOff") + this.cmd("AlinhaEsquerda") + this.quebraLinha;
        }

        var sQRCode = autorizacao.url;
        var tam;
        var tam1;
        var tam2;

        switch (this.config.driveImpressora) {
            case "EPSON":
                tam = sQRCode.length + 3;
                tam1 = tam % 256;
                tam2 = parseInt((tam / 256).toString());
                sQRCode = String.fromCharCode(49) + String.fromCharCode(80) + String.fromCharCode(48) + sQRCode;
                break;
            case "GP-U80300III":
                tam = sQRCode.length + 3;
                tam1 = tam % 256;
                tam2 = parseInt((tam / 256).toString());
                sQRCode = String.fromCharCode(49) + String.fromCharCode(80) + String.fromCharCode(48) + sQRCode;
                break;
            case "TP280":
                tam = sQRCode.length + 3;
                tam1 = tam % 256;
                tam2 = parseInt((tam / 256).toString());
                sQRCode = String.fromCharCode(49) + String.fromCharCode(80) + String.fromCharCode(48) + sQRCode;
                break;
            case "ELGIN":
                tam = sQRCode.length + 3;
                tam1 = tam % 256;
                tam2 = parseInt((tam / 256).toString());
                sQRCode = String.fromCharCode(49) + String.fromCharCode(80) + String.fromCharCode(48) + sQRCode;
                break;
            case "BEMATECH":
                tam = sQRCode.length;
                tam1 = tam % 255;
                tam2 = parseInt((tam / 256).toString());
                break;
            case "DARUMA":
                tam = sQRCode.length;
                tam1 = (tam % 256) + 2;
                tam2 = parseInt((tam / 256).toString());
                break;
            default:
                break;
        }

        if (me.config.driveImpressora == "DARUMA") {
            conteudo += this.cmd("QRCode_1");
            conteudo += this.cmd("QRCode_2") + String.fromCharCode(tam1) + String.fromCharCode(tam2) + String.fromCharCode(4) + String.fromCharCode(0) + sQRCode;
        } else if(me.config.driveImpressora == "PDF"){
            conteudo += "{{QrCode}}"+sQRCode+"{{QrCode}}";
        } else {
            conteudo += this.cmd("QRCode_1");
            conteudo += this.cmd("QRCode_2") + String.fromCharCode(tam1) + String.fromCharCode(tam2) + sQRCode;
            conteudo += this.cmd("QRCode_3") + this.quebraLinha;
        }

        conteudo += this.quebraLinha;

        if (nfcRaiz.infCpl) {
            conteudo += this.cmd("CondensadoOn") + this.cmd("AlinhaEsquerda") + this.cmd("NegritoOn") + nfcRaiz.infCpl + this.cmd("NegritoOff") + this.quebraLinha;
        }

        conteudo += this.cmd("AlinhaCentro") + this.cmd("CondensadoOn") + "Tributos Incidentes Lei Federal 12.741/12" + this.quebraLinha
        conteudo += "Federal: R$" + me.format(this.cupom.total.vTotTribFed || 0, 2) + me.space;
        conteudo += "Estadual: R$" + me.format(this.cupom.total.vTotTribEst || 0, 2) + me.space;
        conteudo += "Municipal: R$" + me.format(this.cupom.total.vTotTribMun, 2) + " - Fonte: IBPT" + this.quebraLinha;

        conteudo += this.rodape();

        this.print(conteudo);
    }

    print(conteudo) {
        if(this.config.driveImpressora == "PDF"){
            this._printPDF(conteudo);
        }else{
            this._printRaw(conteudo);
        }
    }

    _printRaw(conteudo){
        printer.printDirect({
            data: conteudo,
            printer: this.config.nomeImpressora,
            type: 'RAW',
            success: function (jobID) {
                logger.info('Impressão realizada com sucesso!');
            },
            error: function (err) {
                logger.error(err);
            }
        });
    }

    _printPDF(conteudo){
        var me = this;
        async.waterfall([
            function (next){
                pdf(conteudo, next);
            },
            function(pdfPath, next){
                if(process.platform === "win32"){
                    var cwd;
                    if(process.env.NODE_ENV != 'development'){
                        cwd = path.join(path.dirname(process.execPath));
                    }else{
                        cwd = path.join(__dirname, '../tools');
                    }                    
                    cp.exec('PDFtoPrinter.exe "'+pdfPath+'" "'+me.config.nomeImpressora+'"', {
                        cwd: cwd
                    }, (err) => next(err));
                }else{
                    printer.printDirect({
                        data: fs.readFileSync(pdfPath, next),
                        printer: me.config.nomeImpressora,
                        type: 'PDF',
                        success: function (jobID) {
                            logger.info('Impressão realizada com sucesso!');
                        },
                        error: function (err) {
                            next(err);
                        }
                    });
                }                
            }
        ], (err) => {
            if(err) logger.error(err);
        });
    }

    format(valor, decimais) {
        return math.format(math.round(valor || 0, decimais), {
            notation: 'fixed',
            precision: decimais
        });
    }

    mascaraCpf(valor) {
        return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");
    }

    mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }

    mascaraDDMMYYYY(data) {
        return moment(data).format('DD/MM/YYYY HH:mm');
    }

}

module.exports = ImpressaoService;