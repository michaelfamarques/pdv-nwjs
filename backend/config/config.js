module.exports = {
    development: {
        database: "pdv-dev",
        dialect: "sqlite",
        storage: __dirname + "/../../pdv-dev.sqlite"
    },
    production: {
        database: "pdv",
        dialect: "sqlite",
        storage: __dirname + "/../../pdv.sqlite"
    }
}