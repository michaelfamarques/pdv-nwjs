const _ = require('lodash');
const moment = require('moment');

class ChaveAcesso {

    constructor(chave) {
        if (!chave.chave) {
            this.cUF = _.padStart(chave.cUF, 2, '0')
            this.dhEmi = moment(chave.dhEmi, 'YYYY-MM-DD[T]hh:mm:ss').format('YYMM')
            this.cnpj = _.padStart(chave.cnpj, 14, '0')
            this.mod = _.padStart(chave.mod, 2, '0')
            this.serie = _.padStart(chave.serie, 3, '0')
            this.nNF = _.padStart(chave.nNF, 9, '0')
            this.tpEmis = chave.tpEmis;
            this.cNF = _.padStart(chave.cNF, 8, '0');
            let composicao = this.cUF
                + this.dhEmi
                + this.cnpj
                + this.mod
                + this.serie
                + this.nNF
                + this.tpEmis
                + this.cNF;
            this.cDv = this._modulo11(composicao).toString();
            this.chave = composicao + this.cDv;
            this.id = 'NFe' + this.chave;
        } else {
            this.cUF = chave.chave.substring(0, 2);
            this.dhEmi = chave.chave.substring(2, 6);
            this.cnpj = chave.chave.substring(6, 20);
            this.mod = chave.chave.substring(20, 22);
            this.serie = chave.chave.substring(22, 25);
            this.nNF = chave.chave.substring(25, 34);
            this.tpEmis = chave.chave.substring(34, 35);
            this.cNF = chave.chave.substring(35, 42);
            this.cDv = chave.chave.substring(42, 44);
            this.chave = chave.chave;
            this.id = 'NFe' + this.chave;
        }
    }


    _modulo11(chave) {
        var total = 0;
        var peso = 2;
        for (var i = 0; i < chave.length; i++) {
            //+ converte string para number
            total += (+chave.charAt((chave.length - 1) - i) - +'0') * peso;
            peso++;
            if (peso == 10) {
                peso = 2;
            }
        }
        var resto = total % 11;
        return (resto == 0 || resto == 1) ? 0 : (11 - resto);
    }

}

module.exports = ChaveAcesso;