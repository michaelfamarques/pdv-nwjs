'use strict';
module.exports = (sequelize, DataTypes) => {
	var Usuario = sequelize.define('Usuario', {
		idUsuario: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            field: 'id_usuario'
        },
		codigo: DataTypes.INTEGER,
		nome: DataTypes.STRING,
		senha: DataTypes.STRING,
		cpf: DataTypes.STRING
	}, {
		timestamps: false,
		tableName: 'usuario'
	});

	Usuario.logar = function(codigo, senha){
		var Op = sequelize.Op;
		return Usuario.findOne({
			where: {
				codigo: {
					[Op.eq]: codigo
				}
			}
		}).then((usuario) => {
			if(!usuario) return Promise.reject(new Error('Operador não encontrado!'));
			if(usuario.senha != senha){
				return Promise.reject(new Error('Senha incorreta!'))
			}else{
				return Promise.resolve(usuario);
			}
		});
	}

	return Usuario;
};