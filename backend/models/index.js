'use strict';

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var sqlite3 = require('sqlite3');
var logger= require("../logger");
var basename = path.basename(__filename);
var env = process.env.NODE_ENV || 'production';
var config = require(__dirname + '/../config/config.js')[env];
var db = {};

if (config.use_env_variable) {
	var sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
	var sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs.readdirSync(__dirname)
	.filter(file => {
		return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
	})
	.forEach(file => {
		try{
			require(path.join(__dirname, file));
			var model = sequelize['import'](path.join(__dirname, file));
			db[model.name] = model;
		}catch(e){
			logger.error(e.message);
		}
	});

Object.keys(db).forEach(modelName => {
	if (db[modelName].associate) {
		db[modelName].associate(db);
	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.config = config;

module.exports = db;

//sequelize model:generate --name Config --attributes chave:string,valor:json
//sequelize model:generate --name Setor --attributes descricao:string,grupo:integer
//sequelize model:generate --name Grupo --attributes descricao:string
//sequelize model:generate --name Pagamento --attributes codigo:integer,descricao:string,maxparcelas:integer,numerovias:integer,prazopagamento:integer
//sequelize model:generate --name Produto --attributes cfop:string,codigo_ean:string,descricao:string,ncm:string,pesavel:boolean,preco_venda:float,tributacao:json,unidade:string,idsetor:integer,idgrupo:integer
//sequelize model:generate --name Promocao --attributes codigo_ean:string,preco_venda:float,validade_de:date,validade_ate:date
//sequelize model:generate --name Dispositivo --attributes id_dispositivo:integer,ativo:boolean,configuracao_json:json,descricao:string,tipo:integer,nome_arquivo:string
//sequelize model:generate --name Modulo --attributes id_modulo:integer,ativo:boolean,configuracao_json:json,descricao:string,nome_arquivo:string
//sequelize model:generate --name Usuario --attributes id_usuario:integer,bloqueado:boolean,nome:string,senha:string,tipo:integer,usuario:string
//sequelize model:generate --name CupomVenda --attributes id_cupom:integer,codigo_cupom:integer,concluida:boolean,a_prazo:boolean,data_vencimento:date,data_venda:date,documento_cliente:string,valor_total:float,valor_desconto:float,valor_pago:float,valor_troco:float,valor_acrescimo:float
//sequelize model:generate --name ProdutoVendido --attributes codigo_ean:string,id_cupom:integer,valor_desconto:float,valor_total:float,quantidade:float,valor_unitario:float,valor_acrescimo:float
//sequelize model:generate --name PagamentoRealizado --attributes id_pagamento:integer,id_cupom:integer,data_pagamento:date,data_vencimento:date,pagamento_realizado:boolean,valor:float