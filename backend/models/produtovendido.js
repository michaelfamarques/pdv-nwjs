'use strict';

const utils = require('../utils');

module.exports = (sequelize, DataTypes) => {
	var ProdutoVendido = sequelize.define('ProdutoVendido', {
		codigoEan: {
            type: DataTypes.STRING,
            field: 'codigo_ean'
        },
		idCupom: {
            type: DataTypes.INTEGER,
            field: 'id_cupom'
        },
        numero: {
            type: DataTypes.INTEGER,
            field: 'numero'
        },
		valorDesconto: {
            type: DataTypes.FLOAT,
            field: 'valor_desconto'
		},
		valorDescontoRateado: {
            type: DataTypes.FLOAT,
            field: 'valor_desconto_rateado'
        },
		valorTotal: {
            type: DataTypes.FLOAT,
            field: 'valor_total'
        },
		quantidade: DataTypes.FLOAT,
		valorUnitario: {
            type: DataTypes.FLOAT,
            field: 'valor_unitario'
        },
		valorAcrescimo: {
            type: DataTypes.FLOAT,
            field: 'valor_acrescimo'
        },
		valorAcrescimoRateado: {
            type: DataTypes.FLOAT,
            field: 'valor_acrescimo_rateado'
        },
		totais: DataTypes.JSON
	}, {
        timestamps: false,
        tableName: 'produto_vendido',
		getterMethods: {
			valorLiquido: function(){
                return this.valorBruto + (this.valorAcrescimo||0) - (this.valorDesconto||0);
            },            
			valorBruto: function(){
                return (this.quantidade||0) * (this.valorUnitario||0);
			}
		}
    });
    
    ProdutoVendido.associate = function (models) {
        ProdutoVendido.belongsTo(models.CupomVenda, { as: 'cupomVendaInstance', foreignKey: 'idCupom', targetKey: 'idCupom' });
        ProdutoVendido.belongsTo(models.Produto, { as: 'produtoInstance', foreignKey: 'codigoEan', targetKey: 'codigoEan' });
    }

    ProdutoVendido.prototype.calculaValores = function() {
        this.resetTotal();
        var cstCsosn = Number(this.produtoInstance.tributacao.cstCsosnICMS);
        if (
            cstCsosn < 100 && cstCsosn != 30 && cstCsosn != 40 && cstCsosn != 41 && cstCsosn != 50 && cstCsosn != 60
        ) {
            this.total.vBC = this.valorBruto;
        }
        this.total.vICMS = utils.floorNumber((this.produtoInstance.tributacao.aliquotaICMS || 0) * this.valorBruto / 100);
        this.total.vIPI = utils.floorNumber((this.produtoInstance.tributacao.aliquotaIPI || 0) * this.valorBruto / 100);
        this.total.vPIS = utils.floorNumber((this.produtoInstance.tributacao.aliquotaPIS || 0) * this.valorBruto / 100);
        this.total.vCOFINS = utils.floorNumber((this.produtoInstance.tributacao.aliquotaCOFINS || 0) * this.valorBruto / 100);
        var aliqIbptEstadual = this.produtoInstance.tributacao.aliqIbptEstadual || 0;
        var aliqIbptFederal = this.produtoInstance.tributacao.aliqIbptFederal || 0;
        this.total.vTotTribFed = utils.floorNumber((this.produtoInstance.tributacao.aliqIbptFederal || 0) * this.valorBruto / 100);
        this.total.vTotTribEst = utils.floorNumber((this.produtoInstance.tributacao.aliqIbptEstadual || 0) * this.valorBruto / 100);
    }

    ProdutoVendido.prototype.resetTotal = function() {
        this.total = {
            vTotTribFed: 0,
            vTotTribEst: 0,
            vTotTribMun: 0,
            vBC: 0,
            vICMS: 0,
            vBCST: 0,
            vST: 0,
            vIPI: 0,
            vPIS: 0,
            vCOFINS: 0
        };
    }

	return ProdutoVendido;
};