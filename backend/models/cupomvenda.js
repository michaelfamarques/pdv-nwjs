'use strict';

const TIPOS = require('../tipos');
const _ = require('lodash');
const moment = require('moment');
const utils = require('../utils');
const NfceService = require('../nfce');
const app = require('../app');
const ImpressaoService = require('../impressao');

module.exports = (sequelize, DataTypes) => {
    var CupomVenda = sequelize.define('CupomVenda', {
        idCupom: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            field: 'id_cupom'
        },
        status: {
            type: DataTypes.INTEGER,
            field: 'status'
        },
        codigoCupom: {
            type: DataTypes.INTEGER,
            field: 'codigo_cupom'
        },
        numeroNf: {
            type: DataTypes.INTEGER,
            field: 'numero_nf'
        },
        nnfAnterior: {
            type: DataTypes.INTEGER,
            field: 'nnf_anterior'
        },
        chaveAnterior: {
            type: DataTypes.STRING,
            field: 'chave_anterior'
        },
        dataVenda: {
            type: DataTypes.DATE,
            field: 'data_venda'
        },
        documentoCliente: {
            type: DataTypes.STRING,
            field: 'documento_cliente'
        },
        subTotal: {
            type: DataTypes.FLOAT,
            field: 'sub_total'
        },
        valorTotal: {
            type: DataTypes.FLOAT,
            field: 'valor_total'
        },
        valorDesconto: {
            type: DataTypes.FLOAT,
            field: 'valor_desconto'
        },
        valorPago: {
            type: DataTypes.FLOAT,
            field: 'valor_pago'
        },
        valorTroco: {
            type: DataTypes.FLOAT,
            field: 'valor_troco'
        },
        valorAcrescimo: {
            type: DataTypes.FLOAT,
            field: 'valor_acrescimo'
        },
        idUsuario: {
            type: DataTypes.FLOAT,
            field: 'id_usuario'
        },
		totais: DataTypes.JSON,
        chave: DataTypes.STRING,
        xml: DataTypes.STRING,
        autorizacao: DataTypes.JSON
    }, {
        timestamps: false,
        tableName: 'cupom_venda',
		getterMethods: {
			valorTotalLiquido: function(){
                return (this.valorTotal||0) - (this.valorDesconto||0) + (this.valorAcrescimo||0);
            },            
			valorAPagar: function(){
                return (this.valorTotalLiquido||0) - (this.valorPago||0);
            },
            chaveBuscaNFCe: function(){
                return moment(this.dataVenda).format("YYYYMMDD") + _.padStart(String(sequelize.models.Configuracao.getConfigGlobal().numPdv), 3, "0") + _.padStart(String(this.codigoCupom), 9, "0")
            }
		}
    });

    CupomVenda.associate = function (models) {
        CupomVenda.belongsTo(models.Usuario, {
            as: 'usuarioInstance',
            foreignKey: 'idUsuario',
            targetKey: 'idUsuario'
        });
        CupomVenda.hasMany(models.ProdutoVendido, {
            as: 'produtosArr',
            foreignKey: 'idCupom',
            otherKey: 'idCupom'
        });
        CupomVenda.hasMany(models.PagamentoRealizado, {
            as: 'pagamentosArr',
            foreignKey: 'idCupom',
            otherKey: 'idCupom'
        });
    }

    CupomVenda.cupomAberto = null;

    CupomVenda.prototype.cancelarVendaAberta = function () {
        CupomVenda.cupomAberto = null;
        this.status = TIPOS.CupomVendaStatus.Cancelado;
        return this.save();
    }

    CupomVenda.prototype.cancelarVenda = function (justificativa) {        
        return app.api.cancelarVenda(this, justificativa).then((result) => {
            if (result && (result.cstat == 135 || result.cstat == 155)) {
                //var impressaoInstance = new ImpressaoService(cupom);
                //impressaoInstance.cancelamento(cupom.autorizacao.protocolo, cupom.autorizacao.chave);
                this.status = TIPOS.CupomVendaStatus.Cancelado;
                this.autorizacao.cancelamento = result;
                return this.save();
            } else {
                return Promise.reject(new Error(result.xmotivo));
            }
        });
    }

    CupomVenda.getVendaAberta = function (forceDB) {
        if (CupomVenda.cupomAberto && !forceDB) {
            return Promise.resolve(CupomVenda.cupomAberto);
        }
        return CupomVenda.findOne({
            where: {
                status: TIPOS.CupomVendaStatus.Aberto
            },
            include: [{
                model: sequelize.models.ProdutoVendido,
                as: 'produtosArr',
                include: [{
                    model: sequelize.models.Produto,
                    as: 'produtoInstance',
                }]
            }]
        }).then((cupom) => {
            CupomVenda.cupomAberto = cupom;
            return Promise.resolve(cupom);
        });
    }

    CupomVenda.getVendaPorCoo = function (coo) {
        return CupomVenda.findOne({
            where: {
                codigoCupom: coo,
                status: TIPOS.CupomVendaStatus.Finalizado
            }
        });
    }

    CupomVenda.getVendasEmContingencia = function () {
        return CupomVenda.findAll({
            where: {
                status: TIPOS.CupomVendaStatus.Contingencia
            },
            order: [
				['codigoCupom', 'asc']
            ]
        });
    }

    CupomVenda.getUltimoCOO = function () {
        return CupomVenda.max('codigoCupom').then(max => {
            return (max || 0) + 1;
        });
    }

    CupomVenda.prototype.addProduto = function (produtoInstance) {
        return produtoInstance.save().then((produtoInstance) => {
            this.subTotal = utils.floorNumber((this.subTotal || 0) + produtoInstance.valorTotal);
            this.valorTotal = utils.floorNumber((this.valorTotal || 0) + produtoInstance.valorTotal);
            return this.addProdutosArr(produtoInstance).then(() => {
                this.produtosArr.push(produtoInstance);
                return this.save();
            });
        });
    }

    CupomVenda.prototype.addPagamento = function(pagamento){
        if(!Array.isArray(this.pagamentosArr)){
            this.pagamentosArr = [];
        }
        this.pagamentosArr.push(pagamento);
        this.valorPago = utils.floorNumber((this.valorPago||0) + pagamento.valor);
    }

    CupomVenda.prototype.removeProduto = function (produtoInstance) {
        return this.removeProdutosArr(produtoInstance).then(() => {
            this.subTotal = utils.floorNumber((this.subTotal || 0) - produtoInstance.valorTotal);
            this.valorTotal = utils.floorNumber((this.valorTotal || 0) - produtoInstance.valorTotal);
            return this.save();
        }).then(() => {
            return produtoInstance.destroy();
        }).then(() => {
            return this.getProdutosArr({
                include: [{
                    model: sequelize.models.Produto,
                    as: 'produtoInstance',
                }]
            });
        }).then((produtos) => {
            this.produtosArr = _.sortBy(produtos, 'numero');
            var numero = 1;
            var promiseArr = [];
            this.produtosArr.forEach((p) => {
                p.numero = numero++;
                promiseArr.push(p.save());
            });
            return Promise.all(promiseArr)
        }).then(() => {
            return Promise.resolve(this);
        });
    }

    CupomVenda.prototype.salvarPagamentos = function(pagamentosArr){
        return Promise.all(pagamentosArr.map((p) => p.save())).then((result) => {
            return this.setPagamentosArr(pagamentosArr);
        }).then(() => {
            return this.save();
        });
    }

    CupomVenda.prototype.enviarContingencia = function(){
        
        var config = sequelize.models.Configuracao.getConfigGlobal();

        let dados = {
            xml: this.xml,
            pdv: config.numPdv,
            chaveBuscaNFCeAnterior: this.chaveAnterior,
            nnfAnterior: this.nnfAnterior,
            chaveBuscaNFCe: this.chaveBuscaNFCe,
            contingencia: true
        };

        return app.api.enviarNota(dados).then((retorno) => {
            if (retorno.codigoSituacao == '0100' || retorno.codigoSituacao == '0150') {
                this.autorizacao = retorno;
                this.status = TIPOS.CupomVendaStatus.Finalizado;
                return this.save();
            } else { 
                this.status = TIPOS.CupomVendaStatus.ContingenciaErro;
                this.save();
                var err = new Error(retorno.codigoSituacao + ' - ' + retorno.descricao);
                err.erroAutorizacao = true;
                return Promise.reject(err);
            };
        }).catch((err) => {
            return Promise.reject(err);
        }); 

    }

    CupomVenda.prototype.finalizarVenda = function(){

        this.status = TIPOS.CupomVendaStatus.Finalizada;
        this.dataVenda = new Date();

        var produtos = this.produtosArr;
        var pagamentos = this.pagamentosArr;

        var config = sequelize.models.Configuracao.getConfigGlobal();

        this.produtosArr = produtos;
        this.pagamentosArr = pagamentos;
        this.calculaTotais();
        var nfceService = new NfceService(this);
        nfceService.parser();

        var xml = nfceService.getXML();
        xml = nfceService.assinarXml(xml);
        xml = nfceService.geraQrCode(xml);
        this.chave = nfceService.chave.chave;
        this.xml = xml;

        return this.updateAttributes({
            numeroNf: this.numeroNf,
            chave: this.chave,
            xml: this.xml
        }).then(() => {

            if (config.contingenciaOffline) {
                this.status = TIPOS.CupomVendaStatus.Contingencia;
                var aut = {
                    urlcons: nfceService.getUrl(config.dadosFilial.uf, config.dadosFilial.configuracaoObj.tpAmb, 'consultaNfcCliente'),
                    ambiente: config.dadosFilial.configuracaoObj.tpAmb,
                    chaveFormatada: nfceService.chave.chave.match(/(.{1,4})/g).join(" "),
                    url: nfceService.qrCode
                }
                this.autorizacao = aut;
                var impressaoInstance = new ImpressaoService(this);
                impressaoInstance.danfe(nfceService.nfc);
                return this.salvarPagamentos(pagamentos);
            } else {
                let dados = {
                    xml: xml,
                    chaveBuscaNFCe: this.chaveBuscaNFCe,
                    //cupom: cupom.toJSONNovoErp()
                };
                return app.api.enviarNota(dados).then((retorno) => {
                    if (retorno.codigoSituacao == '0100' || retorno.codigoSituacao == '0150') {
                        this.autorizacao = retorno;
                        var impressaoInstance = new ImpressaoService(this);
                        impressaoInstance.danfe(nfceService.nfc);                        
                        this.status = TIPOS.CupomVendaStatus.Finalizado;
                        return this.salvarPagamentos(pagamentos);
                    } else { 
                        var err = new Error('Erro ao tramitar NFC:' + retorno.codigoSituacao + ' - ' + retorno.descricao);
                        err.erroAutorizacao = true;
                        return Promise.reject(err);
                    };
                });   
            }

        });

    }

    CupomVenda.prototype.resetTotal = function() {
        this.total = {
            vBC: 0,
            vICMS: 0,
            vICMSDeson: 0,
            vFCPUFDest: 0,
            vICMSUFDest: 0,
            vICMSUFRemet: 0,
            vBCST: 0,
            vST: 0,
            vProd: 0,
            vFrete: 0,
            vSeg: 0,
            vDesc: 0,
            vII: 0,
            vIPI: 0,
            vPIS: 0,
            vCOFINS: 0,
            vOutro: 0,
            vNF: 0,
            vTotTrib: 0,
            vTotTribFed: 0,
            vTotTribEst: 0,
            vTotTribMun: 0
        };
    }

    CupomVenda.prototype.rateioDescontoAcrescimoCupom = function() {

        if (this.valorDesconto > 0) {
            var itemMaiorDesconto = this.produtosArr[0];
            var totalDescontoRateado = 0;

            this.produtosArr.forEach((i) => {
                if (i.quantidade > itemMaiorDesconto.quantidade) {
                    itemMaiorDesconto = i;
                }
                var valorDesconto = utils.floorNumber(this.valorDesconto * (((i.valorTotal * 100) / this.valorTotal) / 100));
                i.valorDescontoRateado = valorDesconto;
                totalDescontoRateado += valorDesconto;
            });

            var difDesconto = this.valorDesconto - totalDescontoRateado;

            if (difDesconto != 0) {
                itemMaiorDesconto.valorDescontoRateado += difDesconto;
            }
        }

        if (this.valorAcrescimo > 0) {
            var itemMaiorAcrescimo = this.produtosArr[0];
            var totalAcrescimoRateado = 0;

            this.produtosArr.forEach((i) => {
                if (i.quantidade > itemMaiorAcrescimo.quantidade) {
                    itemMaiorAcrescimo = i;
                }
                var valorAcrescimo = utils.floorNumber(this.valorAcrescimo * (((i.valorTotal * 100) / this.valorTotal) / 100));
                i.valorAcrescimoRateado = valorAcrescimo;
                totalAcrescimoRateado += valorAcrescimo;
            });

            var difAcrescimo = this.valorAcrescimo - totalAcrescimoRateado;

            if (difAcrescimo != 0) {
                itemMaiorAcrescimo.valorAcrescimoRateado += difAcrescimo;
            }
        }

    }

    CupomVenda.prototype.calculaTotais = function(){

        var configGeral = sequelize.models.Configuracao.getConfigGlobal();

        this.resetTotal();
        this.rateioDescontoAcrescimoCupom();
        this.produtosArr.forEach((i) => {
            i.calculaValores();
            this.total.vNF += i.valorLiquido + (i.valorAcrescimoRateado||0) - (i.valorDescontoRateado||0);
            this.total.vProd += i.valorBruto;
            if (configGeral.dadosFilial.regime != 1) {
                this.total.vBC += i.total.vBC;
                this.total.vICMS += i.total.vICMS;
                this.total.vBCST += i.total.vBCST;
                this.total.vST += i.total.vST;
                this.total.vPIS += i.total.vPIS;
                this.total.vCOFINS += i.total.vCOFINS;
                this.total.vIPI += i.total.vIPI;
            }
            this.total.vDesc += (i.valorDesconto||0) + (i.valorDescontoRateado||0);
            this.total.vOutro += (i.valorAcrescimo||0) + (i.valorAcrescimoRateado||0);
            this.total.vTotTribFed += i.total.vTotTribFed;
            this.total.vTotTribEst += i.total.vTotTribEst;
            this.total.vTotTribMun += i.total.vTotTribMun;
            this.total.vTotTrib += i.total.vTotTribFed + i.total.vTotTribEst + i.total.vTotTribMun;
        });
    }

    return CupomVenda;
};