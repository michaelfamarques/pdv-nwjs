'use strict';
module.exports = (sequelize, DataTypes) => {
	var PagamentoRealizado = sequelize.define('PagamentoRealizado', {
		idPagamento: {
            type: DataTypes.INTEGER,
            field: 'id_pagamento'
        },
		idCupom: {
            type: DataTypes.INTEGER,
            field: 'id_cupom'
        },
		dataPagamento: {
            type: DataTypes.DATE,
            field: 'data_pagamento'
        },
		dataVencimento: {
            type: DataTypes.DATE,
            field: 'data_vencimento'
        },
		parcelas: DataTypes.INTEGER,
		documentoCliente: {
            type: DataTypes.STRING,
            field: 'documento_cliente'
		},
		valor: DataTypes.FLOAT
	}, {
		timestamps: false,
		tableName: 'pagamento_realizado'
	});

	PagamentoRealizado.associate = function (models) {
		PagamentoRealizado.belongsTo(models.CupomVenda, { as: 'cupomVendaInstance', foreignKey: 'idCupom', targetKey: 'idCupom' });
		PagamentoRealizado.belongsTo(models.Pagamento, { as: 'pagamentoInstance', foreignKey: 'idPagamento', targetKey: 'idPagamento' });				
	}

	return PagamentoRealizado;
};