'use strict';
var moment = require('moment');
var _ = require('lodash');
module.exports = (sequelize, DataTypes) => {

	const Op = sequelize.Op;

	var Produto = sequelize.define('Produto', {
		codigoEan: {
			type: DataTypes.STRING,
			field: 'codigo_ean'
		},
		codigoProduto: {
			type: DataTypes.INTEGER,
			field: 'codigo_produto'
		},
		origem: DataTypes.INTEGER,
		idErp: DataTypes.INTEGER,
		descricao: DataTypes.STRING,
		ncm: DataTypes.STRING,
		exNcm: DataTypes.STRING,
		pesavel: DataTypes.BOOLEAN,
		vendaFracionada: {
			type: DataTypes.BOOLEAN,
			field: 'venda_fracionada'
		},
		produtoSuspenso: {
			type: DataTypes.BOOLEAN,
			field: 'produto_suspenso'
		},
		precoVenda: {
			type: DataTypes.FLOAT,
			field: 'preco_venda'
		},
		quantidadeMinVenda: {
			type: DataTypes.FLOAT,
			field: 'quantidade_min_venda'
		},
		quantidadeMaxVenda: {
			type: DataTypes.FLOAT,
			field: 'quantidade_max_venda'
		},
		tributacao: DataTypes.JSON,
		promocoes: DataTypes.JSON,
		unidade: DataTypes.STRING,
		dataAlteracao: {
			type: DataTypes.DATE,
			field: 'data_alteracao'
		}
	}, {
		timestamps: false,
		tableName: 'produto',
		indexes: [{
			unique: true,
			fields: ['codigo_ean']
		}],
		getterMethods: {
			precoDeVenda() {
				var valorUnitario = this.precoVenda;
				if (this.promocoes && this.promocoes.length > 0) {
					this.promocoes.some(function (e) {
						if (e.promocaoObj) {
							promocao = e;
							valorUnitario = e.precoPromocional;
							return true;
						}
					});
				}
				return valorUnitario;
			}
		},
	});

	Produto.getUltimaDataAlteracao = function () {

		return Produto.findOne({
			order: [
				['dataAlteracao', 'DESC']
			]
		}).then((produto) => {
			if (produto) {
				return Promise.resolve(moment(produto.dataAlteracao));
			} else {
				return Promise.resolve(moment('1980-01-01'));
			}
		});

	}

	Produto.getPorCodigo = function (codigoEan) {

		return Produto.findOne({
			where: {
				codigoEan: {
					[Op.eq]: _.padStart(codigoEan, 14, '0')
				}
			}
		});

	}

	Produto.getProdutos = function (termo) {

		var whereObj = {};
		if (termo) {
			whereObj[Op.or] = [{
					codigoEan: {
						[Op.like]: '%'+termo+'%'
					}
				},
				{
					descricao: {
						[Op.like]: '%'+termo+'%'
					}
				},
				{
					codigoProduto: {
						[Op.eq]: parseInt(termo) || 0
					}
				}
			];
		}

		return Produto.findAll({
			where: whereObj,
			limit: 30,
			order: [
				['descricao', 'asc']
			]
		});

	}

	return Produto;
};