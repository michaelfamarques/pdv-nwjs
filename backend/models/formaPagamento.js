'use strict';
module.exports = (sequelize, DataTypes) => {
	var Pagamento = sequelize.define('Pagamento', {
        idPagamento: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            field: 'id_pagamento'
        },
		codigo: DataTypes.INTEGER,
		descricao: DataTypes.STRING,
		maxParcelas: {
            type: DataTypes.INTEGER,
            field: 'max_parcelas'
		},
		valorMinimoParcela: {
            type: DataTypes.FLOAT,
            field: 'valor_minimo_parcela'
        },
		troco: {
			type: DataTypes.BOOLEAN
		},
		solicitaDataVencimento: {
			type: DataTypes.BOOLEAN,
            field: 'solicita_data_vencimento'
		},
		solicitaCliente: {
			type: DataTypes.BOOLEAN,
            field: 'solicita_cliente'
		},
		aceitaClienteEmBranco: {
			type: DataTypes.BOOLEAN,
            field: 'aceita_cliente_em_branco'
		},
		clienteSoCpfCnpj: {
			type: DataTypes.BOOLEAN,
            field: 'cliente_so_cpf_cnpj'
		},
		consultaCliente: {
			type: DataTypes.BOOLEAN,
            field: 'consulta_cliente'
		},
		tipoFormaNFCe: {
            type: DataTypes.INTEGER,
            field: 'tipo_forma_nfce'
		}
	}, {
		timestamps: false,
		tableName: 'forma_pagamento',
		classMethods: {
			associate: function (models) {}
		}
	});

	Pagamento.getTodos = function(){
		return Pagamento.findAll({
			order: [
				['codigo', 'asc']
			]
		});
	}

	return Pagamento;
};