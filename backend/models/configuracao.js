'use strict';
module.exports = (sequelize, DataTypes) => {
	var Config = sequelize.define('Configuracao', {
		idConfiguracao: {
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true,
			field: 'id_configuracao'
		},
		apiKey: {
			type: DataTypes.STRING,
			field: 'api_key'
		},
		idEmpresa: {
			type: DataTypes.BIGINT,
			field: 'id_empresa'
		},
		idFilial: {
			type: DataTypes.BIGINT,
			field: 'id_filial'
		},
		numPdv: {
			type: DataTypes.BIGINT,
			field: 'num_pdv'
		},
		configGeral: {
			type: DataTypes.JSON,
			field: 'config_geral'
		},
		dadosFilial: {
			type: DataTypes.JSON,
			field: 'dados_filial'
		},
		dadosPdv: {
			type: DataTypes.JSON,
			field: 'dados_pdv'
		},
		contingenciaOffline: {
			type: DataTypes.BOOLEAN,
			field: 'contingencia_offline'
		},
        dataEntradaContingencia: {
            type: DataTypes.DATE,
            field: 'data_entrada_contingencia'
        },
        driveImpressora: {
            type: DataTypes.STRING,
            field: 'drive_impressora'
        },
        nomeImpressora: {
            type: DataTypes.STRING,
            field: 'nome_impressora'
        }
	}, {
		timestamps: false,
		tableName: 'configuracao'
	});

	Config.associate = function (models) {
		Config.belongsTo(models.Usuario, {
			as: 'operadorLogadoInstance',
			foreignKey: 'idOperadorLogado',
			targetKey: 'idUsuario'
		});

	}

	var configGlobal = null;

	Config.carregaConfiguracao = function () {
		return Config.findOne().then((config) => {
			configGlobal = config;
			return Promise.resolve(configGlobal);
		});
	}

	Config.setConfigGlobal = function (config) {
		configGlobal = config;
	}

	Config.getConfigGlobal = function () {
		return configGlobal;
	}

	Config.prototype.toggleContingencia = function(){
		this.contingenciaOffline = !this.contingenciaOffline;
		if(this.contingenciaOffline){
			this.dataEntradaContingencia = new Date();
		}else{
			this.dataEntradaContingencia = null;
		}
		this.save();
	}

	Config.prototype.getProximoNNF = function () {
		var serie = this.dadosFilial.serieObj.find((e, i, series) => {
			return (e.codigo == this.numPdv && e.modelo == 65)
		});
		let result;
		if (!serie) {
			serie = {
				codigo: String(this.numPdv),
				numeroNf: 0,
				modelo: 65
			}
			this.dadosFilial.serieObj.push(serie);
		}
		serie.numeroNf++;
		this.changed('dadosFilial', true);
		this.save();
		return serie.numeroNf;
	}

	Config.prototype.setOperadorLogado = function(usuario){
		this.operadorLogado = usuario;
	}

	Config.prototype.getOperadorLogado = function(){
		return this.operadorLogado;
	}

	return Config;
};