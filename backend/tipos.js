module.exports = {
    CupomVendaStatus: {
        Aberto: 0,
        Finalizado: 1,
        Cancelado: 2,
        Contingencia: 3,
        ContingenciaErro: 4
    }
}