const PDFDocument = require('pdfkit');
const fs = require('fs');
const os = require('os');
const path = require('path');
const qrcode = require('qr-image');

var parsingQrcode, qrCodeUrl, docWidth, docHeight, fontSizePequena, fontSizeNormal, fontSizeGrande, optAlign;
var doc;

function printText(bufferText) {
    if (parsingQrcode) {        
        qrCodeUrl = bufferText;
        return;
    }
    doc.text(bufferText, {
        align: optAlign,
    });
}

function parseComando(comando) {

    switch (comando) {
        case "AlinhaEsquerda":
            optAlign = "left";
            break;
        case "AlinhaCentro":
            optAlign = "center";
            break;
        case "AlinhaDireita":
            optAlign = "right";
            break;
        case "AlturaDuplaOn":
            doc.fontSize(fontSizeGrande);
            break;
        case "AlturaDuplaOff":
        case "FonteNormal":
            doc.fontSize(fontSizeNormal);
            break;
        case "FontePequena":
            doc.fontSize(fontSizePequena);
            break;
        case "NegritoOn":
            doc.font('Bold');
            break;
        case "NegritoOff":
            doc.font('Normal');
            break;
        case "QrCode":
            if (!parsingQrcode) {
                parsingQrcode = true;
            } else {
                parsingQrcode = false;
                var qrImage = qrcode.imageSync(qrCodeUrl, {
                    type: 'png'
                });
                doc.moveDown();
                doc.image(qrImage, (doc.page.width - (docWidth - 50)) / 2, doc.y, {
                    fit: [docWidth - 50, docWidth - 50],
                    align: 'center',
                    valign: 'center'
                });
                doc.moveDown();
            }
            break;
        case "CondensadoOn":
        case "CondensadoOff":
        case "CortaPapel":
        case "ExtendidoOn":
        case "ExtendidoOff":
            //FazNada
            break;
    }

}

module.exports = function (conteudo, callback) {

    fontSizePequena = 5;
    fontSizeNormal = 6;
    fontSizeGrande = 8;
    optAlign = 'left';

    var pdfTmp = path.join(os.tmpdir(), 'cupom.pdf');

    var tmpFileStream = fs.createWriteStream(pdfTmp);
    var linhas = conteudo.split("{{QuebraLinha}}");

    docWidth = 150;
    docHeight = (linhas.length * 14) + (docWidth - 50);

    doc = new PDFDocument({
        margins: {
            left: 1,
            right: 1,
            top: 5,
            bottom: 5
        },
        size: [docWidth, docHeight]
    });

    doc.pipe(tmpFileStream);
    doc.fontSize(fontSizeNormal);
    doc.registerFont('Normal', path.join(__dirname, 'fonts/DejaVuSansMono.ttf'));
    doc.registerFont('Bold', path.join(__dirname, 'fonts/DejaVuSansMono-Bold.ttf'));

    doc.font('Normal');
    
    parsingQrcode = false;
    qrCodeUrl = "";

    linhas.forEach((linha) => {       

        if (linha.trim() == "") return;

        var bufferText = "";

        for (var i = 0; i < linha.length; i++) {
            if (linha[i] == "{" && linha[i + 1] == "{") {
                if (bufferText) {
                    printText(bufferText);
                    bufferText = "";
                }
                var indexFinal = linha.substring(i + 2).indexOf("}}");
                var comando = linha.substring(i + 2, i + 2 + indexFinal);
                i = i + 2 + indexFinal + 1;
                parseComando(comando);
            } else {
                bufferText += linha[i];
            }
        }

        if (bufferText) {
            printText(bufferText);
            bufferText = "";
        }

        doc.moveDown(0.5);

    });

    doc.end();

    doc = null;

    tmpFileStream.on('finish', (err) => {
        callback(err, pdfTmp);
    });

}