var winston = require('winston');
var path = require('path');
var pathLog;

if(process.env.NODE_ENV == 'development'){
    pathLog = path.join(__dirname, '../');
}else{
    pathLog = path.join(path.dirname(process.execPath));
}     

console.log(pathLog);

const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            handleExceptions: true,
            filename: path.join(pathLog, 'pdv.log'),
            level: 'info',
            maxFiles: 1,
            maxsize: 10485760
        })
    ]
});

module.exports = logger;