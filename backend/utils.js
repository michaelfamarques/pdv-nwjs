var path = require('path');

/**
 * Pega a pasta atual aonde a aplicação está instalada
 * 
 * @returns path do local da aplicação
 */
module.exports.getProcessPath = function(){
    if(process.env.NODE_ENV == "development"){
        return ".";
    }
    var nwPath = process.execPath;
    return path.dirname(nwPath);
}

module.exports.floorNumber = function(num){
    num = num || 0;
    num = num * 100;
    var int = num % Number.parseInt(num+"");
    if(int > 0.9999 && int < 1){
        num = Math.ceil(num);
    }else{
        num = Math.floor(num);
    }        
    return num / 100;
}