const app = require('./app');
const urls = require('./config/urls.json');
const json2xml = require('json2xml');
const moment = require('moment');
const utils = require('./utils');
const _ = require('lodash');
const crypto = require('crypto');
const SignedXml = require('xml-crypto').SignedXml;
const numUtil = require('./num');
const ChaveAcesso = require('./chave-acesso');

const CUF = { 'AC': 12, 'AL': 27, 'AM': 13, 'AP': 16, 'BA': 29, 'CE': 23, 'DF': 53, 'ES': 32, 'EX': 99, 'GO': 52, 'MA': 21, 'MG': 31, 'MS': 50, 'MT': 51, 'PA': 15, 'PB': 25, 'PE': 26, 'PI': 22, 'PR': 41, 'RJ': 33, 'RN': 24, 'RO': 11, 'RR': 14, 'RS': 43, 'SC': 42, 'SE': 28, 'SP': 35, 'TO': 17 };
const AMBIENTE = { 'Produção': 1, 'Homologação': 2 };
const GMT = ['-03:00', '-02:00', '-03:00', '-04:00', '-01:00', '-02:00', '-03:00'];

const envioAssincrono = ['29', '35'];

class NfceService {

    constructor(
        cupom
    ) { 
        this.cupom = cupom;
        this.config = app.db.Configuracao.getConfigGlobal();
        this.nfc = null;
        this.versaoNfe = _.get(this.config, 'dadosFilial.configuracaoObj.versao');
        this.versaoAutoriza = _.get(this.config, 'dadosFilial.configuracaoObj.versao') == "400" ? "4.00" : "3.10";
        this.loteAbre;
        this.loteFecha;
        this.itemMaior;
        this.chave;
        this.qrCode;
    }

    getXML() {
        var jsonxml = JSON.parse(JSON.stringify(this.nfc));
        var xml = json2xml(jsonxml, {
            attributes_key: 'attr'
        });
        xml = xml.replace(/<infCpl.*?>/i, '<infCpl>');
        return xml;
    }

    getNfc() {
        return this.nfc;
    }

    getUrl(uf, tpAmb, chave){
        let pathJson = tpAmb == 1 ? "producao" : "homologacao";
        pathJson += "." + uf + "."+chave;
        return _.get(urls, pathJson);
    }

    geraQrCode(xmlAssinado) {       

        let cscCod = app.db.Configuracao.getConfigGlobal().dadosFilial.configuracaoObj.csc;

        let url = this.getUrl(this.nfc.NFe.infNFe.emit.enderEmit.UF, this.nfc.NFe.infNFe.ide.tpAmb, 'NfcConsulta');

        if (!url) {
            throw new Error("Não foi encontrado a URL de consulta de QRCode para o estado " + this.nfc.NFe.infNFe.emit.enderEmit.UF);
        }

        let parametros = {};

        parametros.chNFe = this.nfc.NFe.infNFe.attr.Id.replace("NFe", "");
        parametros.nVersao = "100";
        parametros.tpAmb = this.nfc.NFe.infNFe.ide.tpAmb;

        if (this.nfc.NFe.infNFe.dest) {
            if (this.nfc.NFe.infNFe.dest.CNPJ) {
                parametros.cDest = this.nfc.NFe.infNFe.dest.CNPJ;
            } else if (this.nfc.NFe.infNFe.dest.CPF) {
                parametros.cDest = this.nfc.NFe.infNFe.dest.CPF;
            }
        }

        parametros.dhEmi = Buffer.from(moment(this.nfc.NFe.infNFe.ide.dhEmi).format()).toString('hex');

        parametros.vNF = this.nfc.NFe.infNFe.total.ICMSTot.vNF;
        parametros.vICMS = this.nfc.NFe.infNFe.total.ICMSTot.vICMS;

        let regex = /<DigestValue>([^<]*)<\/DigestValue>/g;
        let match = regex.exec(xmlAssinado);

        parametros.digVal = Buffer.from(match[1]).toString('hex');
        parametros.cIdToken = "000001";

        let urlHash = this.serialize(parametros) + cscCod;
        let urlHashDigest = crypto.createHash('sha1').update(urlHash).digest('hex').toUpperCase();

        parametros.cHashQRCode = urlHashDigest;

        let urlQrCode = url + "?" + this.serialize(parametros);

        this.qrCode = urlQrCode;

        this.nfc.NFe.infNFeSupl = {
            qrCode: "<![CDATA[" + urlQrCode + "]]>"
        };
        let xmlInfNFeSupl = "<infNFeSupl><qrCode>" + this.nfc.NFe.infNFeSupl.qrCode + "</qrCode></infNFeSupl>";
        xmlAssinado = xmlAssinado.replace("</infNFe><Signature", "</infNFe>" + xmlInfNFeSupl + "<Signature");

        return xmlAssinado;

    }

    serialize(obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }

    parser() {
        let me = this;
        me.nfc = {
            attr: {
                xmlns: 'http://www.portalfiscal.inf.br/nfe'
            }
        };

        me.loteAbre = '';
        if (me.versaoNfe != "400") {
            me.loteAbre += '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeAutorizacao">';
            me.loteAbre += '<enviNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="' + me.versaoAutoriza + '"><idLote>';
            me.loteAbre += me.cupom.codigoCupom;
            if (envioAssincrono.indexOf(CUF[_.upperCase(me.config.dadosFilial.uf)]) > -1) { //Bahia
                me.loteAbre += '</idLote><indSinc>0</indSinc>';
            } else {
                me.loteAbre += '</idLote><indSinc>1</indSinc>';
            }
            me.loteFecha = '</enviNFe>';
            if (me.versaoNfe != "400")
                me.loteFecha += '</nfeDadosMsg>';


            me.nfc.NFe = {
                attr: {
                    xmlns: "http://www.portalfiscal.inf.br/nfe"
                }
            };
            me.nfc.NFe.infNFe = {
                attr: {
                    Id: null,
                    versao: me.versaoAutoriza
                }
            };
            me.nfc.NFe.infNFe.ide = me._geraIde();
            me.nfc.NFe.infNFe.emit = me._geraEmit();

            if (me.cupom.clienteObj) {
                me.nfc.NFe.infNFe.dest = me._geraDest();
            }

            if (me.cupom.produtosArr && me.cupom.produtosArr.length > 0) {
                var items = [];
                for (var index = 0; index < me.cupom.produtosArr.length; index++) {
                    var item = me.cupom.produtosArr[index];
                    if (index == 0 && me.config.dadosFilial.configuracaoObj.tpAmb == 2) {
                        item.Xprod = 'NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
                    }
                    items.push(me._geraDetNitem(item));
                }
                me.nfc.NFe.infNFe.det = items;
            }

            me.nfc.NFe.infNFe.total = me._geraTotal();
            me.nfc.NFe.infNFe.transp = me._geraTransp();

            if (me.cupom.pagamentosArr && me.cupom.pagamentosArr.length > 0) {
                var formas = [];
                for (var index = 0; index < me.cupom.pagamentosArr.length; index++) {
                    var item = me.cupom.pagamentosArr[index];
                    formas.push(me._geraForma(item));
                }
                if (formas.length > 0 && me.cupom.valorTroco > 0) {
                    formas[formas.length - 1].vPag = numUtil.dec2(me.cupom.pagamentosArr[me.cupom.pagamentosArr.length - 1].valor - me.cupom.valorTroco);
                }
                me.nfc.NFe.infNFe['pag'] = formas;
            }

        };
    }

    _geraIde() {
        var dataHoraSaida = null;
        var tpEmis = this.config.contingenciaOffline ? 9 : 1;

        var serieCodigo = this.config.numPdv;
        var numeroNf = this.cupom.numeroNf || this.config.getProximoNNF();

        this.cupom.numeroNf = numeroNf;

        var ide = {
            cUF: _.padStart(CUF[_.upperCase(this.config.dadosFilial.uf)], 2, '0'),
            cNF: _.padStart(this.cupom.codigoCupom.toString(), 8, '0'),
            natOp: 'VENDAS MERC. REC. TERC.',
            indPag: 1,
            mod: 65,
            serie: _.trimStart(String(serieCodigo), '0'),
            nNF: numeroNf,
            dhEmi: moment(this.cupom.dataVenda).format('YYYY-MM-DD[T]HH:mm:ss') + GMT[this.config.dadosFilial.configuracaoObj.gmt],
            tpNF: 1,
            idDest: 1,
            cMunFG: this.config.dadosFilial.codMunicipio ? _.pad(this.config.dadosFilial.codMunicipio, 7, '0') : _.pad(this.config.dadosFilial.codigoIbge, 7, '0'),
            tpImp: 4,
            tpEmis: tpEmis,
            cDV: null,
            tpAmb: this.config.dadosFilial.configuracaoObj.tpAmb,
            finNFe: 1,
            indFinal: 1,
            indPres: 1,
            // Processo de Emissão NF-e (0) aplicativo do contrib.
            procEmi: "0",
            verProc: "PDV Michael"
        };

        let objChave = {
            cUF: ide.cUF,
            dhEmi: ide.dhEmi,
            cnpj: this.config.dadosFilial.cpfCnpj,
            mod: '65',
            serie: ide.serie,
            nNF: String(ide.nNF),
            tpEmis: ide.tpEmis.toString(),
            cNF: ide.cNF
        }

        this.chave = new ChaveAcesso(objChave);
        this.nfc.NFe.infNFe.attr.Id = this.chave.id;

        ide['cDV'] = this.chave.cDv;

        if (ide.tpEmis != 1) {
            ide['dhCont'] = moment(this.config.dataEntradaContingencia).format('YYYY-MM-DD[T]HH:mm:ss') + GMT[this.config.dadosFilial.configuracaoObj.gmt];
            ide['xJust'] = this.config.dadosFilial.configuracaoObj.xJust || "Contingência por problemas de conexão com a internet.";
        }

        return ide;

    }

    _geraEmit() {
        var emit = {
            CNPJ: this.config.dadosFilial.cpfCnpj,
            xNome: this.config.dadosFilial.nomeRazaoSocial,
            xFant: this.config.dadosFilial.nome,
            enderEmit: {
                xLgr: this.config.dadosFilial.tipoLogradouro + ' ' + this.config.dadosFilial.descricaoEndereco,
                nro: this.config.dadosFilial.numeroEndereco,
                xBairro: this.config.dadosFilial.nomeBairro,
                cMun: this.config.dadosFilial.codMunicipio ? _.pad(this.config.dadosFilial.codMunicipio, 7, '0') : _.pad(this.config.dadosFilial.codigoIbge, 7, '0'),
                xMun: this.config.dadosFilial.nomeCidade,
                UF: this.config.dadosFilial.uf,
                CEP: this.config.dadosFilial.cep,
                cPais: '1058',
                xPais: 'BRASIL',
                fone: this.config.dadosFilial.telefone
            },
            IE: this.config.dadosFilial.ie,
        }

        if (this.cupom.clienteObj && this.cupom.clienteObj.IEST && this.cupom.clienteObj.IEST != '') {
            emit['IEST'] = this.cupom.clienteObj.IEST.trim();
        }

        emit['CRT'] = this.config.dadosFilial.regime == 1 ? 1 : 3 //Não tratamos 2 Simples Nacional, excesso sublimite de receita bruta  
        return emit;
    }

    _geraDest() {
        var dest = null;
        dest = {
            CNPJ: '',
            CPF: '',
            idEstrangeiro: ''
        };
        var naoPossuiIdEstrangeiro = true;
        if (this.cupom.clienteObj.cpfCnpj.trim().length == 14) {
            dest['CNPJ'] = this.cupom.clienteObj.cpfCnpj;
            delete dest.CPF;
        } else {
            dest['CPF'] = this.cupom.clienteObj.cpfCnpj;
            delete dest.CNPJ;
        }
        if (this.config.dadosFilial.configuracaoObj.tpAmb == AMBIENTE.Homologação) { //Homologação
            dest['xNome'] = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
        } else {
            dest['xNome'] = this.cupom.clienteObj.nome;
        }
        let geraDest = true;
        if (this.cupom.clienteObj.descricaoEndereco == '' ||
            this.cupom.clienteObj.numeroEndereco == '' ||
            this.cupom.clienteObj.nomeBairro == '' ||
            this.cupom.clienteObj.codigoIbge == '' ||
            this.cupom.clienteObj.nomeCidade == '' ||
            this.cupom.clienteObj.uf == '' ||
            this.cupom.clienteObj.cep == ''
        ) {
            geraDest = false;
        }
        if (geraDest) {
            dest.enderDest = {
                xLgr: _.trim(this.cupom.clienteObj.descricaoEndereco),
                nro: _.trim(this.cupom.clienteObj.numeroEndereco),
                xCpl: this.cupom.clienteObj.descricaoComplemento ? _.trim(this.cupom.clienteObj.descricaoComplemento) : null,
                xBairro: _.trim(this.cupom.clienteObj.nomeBairro),
                cMun: _.trim(this.cupom.clienteObj.codigoIbge),
                xMun: _.trim(this.cupom.clienteObj.nomeCidade),
                UF: _.trim(this.cupom.clienteObj.uf),
                CEP: _.trim(this.cupom.clienteObj.cep),
                cPais: '1058',
                xPais: _.trim(this.cupom.clienteObj.pais)
            }
            if (!dest.enderDest.xCpl) {
                delete dest.enderDest.xCpl;
            }
            if (_.trimStart(this.cupom.clienteObj.telefone, '0') != '') {
                dest.enderDest['fone'] = this.cupom.clienteObj.telefone.trim();
            }
        }
        dest['indIEDest'] = '9';

        if (this.cupom.clienteObj.codigoSuframa && _.trim(_.trimStart(this.cupom.clienteObj.codigoSuframa), '0') != '') {
            dest['ISUF'] = _.trim(_.trimStart(this.cupom.clienteObj.codigoSuframa), '0');
        }
        if (this.cupom.clienteObj.email && this.cupom.clienteObj.email.trim() != '') {
            dest['email'] = this.cupom.clienteObj.email;
        }
        if (naoPossuiIdEstrangeiro) {
            delete dest.idEstrangeiro;
        }

        return dest;

    };

    _geraDetNitem(item) {

        var detnitem = {
            prod: {
                cProd: item.produtoInstance.codigoProduto
            }
        }
        
        let ncm = '';

        if (!item.produtoInstance.ncm) item.produtoInstance.ncm = '';

        if (item.produtoInstance.ncm && item.produtoInstance.ncm.length == 2) {
            ncm = item.produtoInstance.ncm;
        } else {
            ncm = _.padStart(item.produtoInstance.ncm, 8, '0');
        }

        let eanOk = false;
        detnitem.prod['cEAN'] = '';
        if (item.codigoEan) {
            let digito = this.calcDigitoEans(item.codigoEan.substring(0, item.codigoEan.length - 1));
            if (String(digito) == item.codigoEan.substring(item.codigoEan.length - 1, item.codigoEan.length)) {
                detnitem.prod['cEAN'] = (item.codigoEan && _.trimStart(item.codigoEan, '0') != '') ? item.codigoEan : '';
                eanOk = true;
            }
        }

        if(item.numero == 1 && this.config.dadosFilial.configuracaoObj.tpAmb == 2){
            detnitem.prod['xProd'] = 'NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
        }else{
            detnitem.prod['xProd'] = _.trim(item.produtoInstance.descricao);
        }
        
        detnitem.prod['NCM'] = ncm;
        if (item.produtoInstance.tributacao.cest && item.produtoInstance.tributacao.cest != '') detnitem.prod['CEST'] = _.trim(item.produtoInstance.tributacao.cest) || '';
        detnitem.prod['CFOP'] = item.produtoInstance.tributacao.CFOP;
        detnitem.prod['uCom'] = _.trim(item.produtoInstance.unidade);
        detnitem.prod['qCom'] = numUtil.dec3(item.quantidade);
        detnitem.prod['vUnCom'] = numUtil.dec4(item.valorUnitario);
        detnitem.prod['vProd'] = numUtil.dec2(item.valorBruto);
        detnitem.prod['cEANTrib'] = '';
        if (eanOk) {
            detnitem.prod['cEANTrib'] = (item.codigoEan && _.trimStart(item.codigoEan, '0') != '') ? item.codigoEan : '';
        }
        detnitem.prod['uTrib'] = _.trim(item.produtoInstance.unidade);
        detnitem.prod['qTrib'] = numUtil.dec3(item.quantidade);
        detnitem.prod['vUnTrib'] = numUtil.dec4(item.valorUnitario);

        var desconto = (item.valorDesconto||0) + (item.valorDescontoRateado||0);

        if (desconto > 0) {
            detnitem.prod['vDesc'] = numUtil.dec2(this.floor(desconto));
        }

        if (detnitem.prod['vDesc'] == "0.00") { delete detnitem.prod['vDesc']; }

        var acrescimo = (item.valorAcrescimo||0) + (item.valorAcrescimoRateado||0);

        if (acrescimo > 0) {
            detnitem.prod['vOutro'] = numUtil.dec2(this.floor(acrescimo));
        }

        if (detnitem.prod['vOutro'] == "0.00") { delete detnitem.prod['vOutro']; }

        detnitem.prod['indTot'] = 1;

        let itemNormalizado = this._normalizaAtributosNFCe(item);

        detnitem['imposto'] = {
            vTotTrib: numUtil.dec2(item.total.vTotTribFed + item.total.vTotTribEst + item.total.vTotTribMun)
        };
        detnitem['attr'] = {
            nItem: item.numero
        };

        detnitem.imposto['ICMS'] = this._geraIcms(itemNormalizado);
        if (item.total.vIPI > 0) {
            detnitem.imposto['IPI'] = this._geraIPI(itemNormalizado);
        }
        detnitem.imposto['PIS'] = this._geraPIS(itemNormalizado);
        detnitem.imposto['COFINS'] = this._geraCOFINS(itemNormalizado);

        return detnitem;
    }

    _geraTotal() {
        return {
            ICMSTot: {
                vBC: numUtil.dec2(this.floor(this.cupom.total.vBC)),
                vICMS: numUtil.dec2(this.floor(this.cupom.total.vICMS)),
                vICMSDeson: numUtil.dec2(this.floor(this.cupom.total.vICMSDeson)),
                vFCPUFDest: numUtil.dec2(this.floor(this.cupom.total.vFCPUFDest)),
                vICMSUFDest: numUtil.dec2(this.floor(this.cupom.total.vICMSUFDest)),
                vICMSUFRemet: numUtil.dec2(this.floor(this.cupom.total.vICMSUFRemet)),
                vBCST: numUtil.dec2(this.floor(this.cupom.total.vBCST)),
                vST: numUtil.dec2(this.floor(this.cupom.total.vST)),
                vProd: numUtil.dec2(this.floor(this.cupom.total.vProd)),
                vFrete: numUtil.dec2(this.floor(this.cupom.total.vFrete)),
                vSeg: numUtil.dec2(this.floor(this.cupom.total.vSeg)),
                vDesc: numUtil.dec2(this.floor(this.cupom.total.vDesc)),
                vII: numUtil.dec2(this.floor(this.cupom.total.vII)),
                vIPI: numUtil.dec2(this.floor(this.cupom.total.vIPI)),
                vPIS: numUtil.dec2(this.floor(this.cupom.total.vPIS)),
                vCOFINS: numUtil.dec2(this.floor(this.cupom.total.vCOFINS)),
                vOutro: numUtil.dec2(this.floor(this.cupom.total.vOutro)),
                vNF: numUtil.dec2(this.floor(this.cupom.total.vNF)),
                vTotTrib: numUtil.dec2(this.floor(this.cupom.total.vTotTrib))
            }
        };
    }

    floor(num) {
        return utils.floorNumber(num);
    }

    _geraTransp() {
        var transp = {
            modFrete: 9
        };
        return transp;
    }
    _geraIcms(item) {
        if (item) {
            if (this.nfc.NFe.infNFe.emit.CRT == 3) {
                switch (item.cstIcms) {
                    case '00':
                        return this._geraIcms00(item);
                    case '10':
                        return this._geraIcms10(item);
                    case '20':
                        return this._geraIcms20(item);
                    case '30':
                        return this._geraIcms30(item);
                    case "40":
                    case "41":
                    case "50":
                        return this._geraIcms50(item);
                    case "51":
                        return this._geraIcms51(item);
                    case '60':
                        return this._geraIcms60(item);
                    case "70":
                        return this._geraIcms70(item);
                    case "90":
                        return this._geraIcms90(item);
                }
            } else {
                return this._geraCsosn(item);
            }
        }
        return;
    }
    _geraIcms00(item) {
        return {
            ICMS00: {
                orig: item.orig,
                CST: item.cstIcms,
                modBC: item.modBC,
                vBC: numUtil.dec2(item.vrBC),
                pICMS: numUtil.dec2(item.picms),
                vICMS: numUtil.dec2(item.vIcms)
            }
        }
    }
    _geraIcms10(item) {
        var icms10 = {
            ICMS10: {}
        };
        icms10.ICMS10['orig'] = item.orig;
        icms10.ICMS10['CST'] = item.cstIcms;
        icms10.ICMS10['modBC'] = item.modBC;
        icms10.ICMS10['vBC'] = numUtil.dec2(item.vrBC);
        icms10.ICMS10['pICMS'] = numUtil.dec2(item.picms);
        icms10.ICMS10['vICMS'] = numUtil.dec2(item.vIcms);
        icms10.ICMS10['modBCST'] = item.modBCST;
        icms10.ICMS10['pRedBCST'] = numUtil.dec2(item.pRedbCst)
        icms10.ICMS10['vBCST'] = numUtil.dec2(item.vbCst);
        icms10.ICMS10['pICMSST'] = numUtil.dec2(item.pIcmsSt);
        icms10.ICMS10['vICMSST'] = numUtil.dec2(item.vicmsst);
        return icms10;
    }
    _geraIcms20(item) {
        var icms20 = {
            ICMS20: {}
        };
        icms20.ICMS20['orig'] = item.orig;
        icms20.ICMS20['CST'] = item.cstIcms;
        icms20.ICMS20['modBC'] = item.modBC;
        icms20.ICMS20['pRedBC'] = numUtil.dec2(item.pRedbCst); //Estranho Redução de CST?
        icms20.ICMS20['vBC'] = numUtil.dec2(item.vrBC);
        icms20.ICMS20['pICMS'] = numUtil.dec2(item.picms);
        icms20.ICMS20['vICMS'] = numUtil.dec2(item.vIcms);
        if (item.motDesIcms && item.VICMSDeson && item.VICMSDeson > 0) {
            icms20.ICMS20['vICMSDeson'] = numUtil.dec2(item.VICMSDeson);
            icms20.ICMS20['motDesICMS'] = item.motDesIcms;
        }

        return icms20;
    }
    _geraIcms30(item) {
        var icms30 = {
            ICMS30: {}
        };
        icms30.ICMS30['orig'] = item.orig;
        icms30.ICMS30['CST'] = item.cstIcms;
        icms30.ICMS30['pMVAST'] = "";
        icms30.ICMS30['pRedBCST'] = numUtil.dec2(item.pRedbCst);
        icms30.ICMS30['vBCST'] = numUtil.dec2(item.vbCst);
        icms30.ICMS30['pICMSST'] = numUtil.dec2(item.pIcmsSt);
        icms30.ICMS30['vICMSST'] = numUtil.dec2(item.vicmsst);
        if (item.VICMSDeson > 0) {
            icms30.ICMS30['vICMSDeson'] = numUtil.dec2(item.VICMSDeson);
        }
        icms30.ICMS30['PRedBCST'] = numUtil.dec2(item.pRedbCst)

        return icms30;

    }
    _geraIcms50(item) {
        var icms40 = {
            ICMS40: {}
        };
        icms40.ICMS40['orig'] = item.orig;
        icms40.ICMS40['CST'] = item.cstIcms;
        if (item.motDesIcms && item.VICMSDeson && item.VICMSDeson > 0) {
            icms40.ICMS40['vICMSDeson'] = numUtil.dec2(item.VICMSDeson);
            icms40.ICMS40['motDesICMS'] = item.motDesIcms;
        };
        return icms40;

    }
    _geraIcms51(item) {
        var icms51 = {
            ICMS51: {}
        };
        icms51.ICMS51['orig'] = item.orig;
        icms51.ICMS51['CST'] = item.cstIcms;
        if (item.vrBC > 0) {
            icms51.ICMS51['modBC'] = item.modBC;
            icms51.ICMS51['pRedBC'] = numUtil.dec2(item.pRedbCst); //Estranho Redução de CST?
            icms51.ICMS51['vBC'] = numUtil.dec2(item.vrBC);
            icms51.ICMS51['pICMS'] = numUtil.dec2(item.picms);
            icms51.ICMS51['vICMS'] = numUtil.dec2(item.vIcms);
            if (item.vIcmsOp > 0) {
                icms51.ICMS51['vICMSOp'] = numUtil.dec2(item.vIcmsOp);
                icms51.ICMS51['PDif'] = numUtil.dec2(item.pDif);
                icms51.ICMS51['vICMSDif'] = numUtil.dec2(item.vIcmsDif);
            }
        }
        return icms51;
    }
    _geraIcms60(item) {
        return {
            ICMS60: {
                orig: item.orig,
                CST: item.cstIcms,
                vBCSTRet: "0.00",
                vICMSSTRet: "0.00"
            }
        }

    }
    _geraIcms70(item) {
        var icms70 = {
            ICMS70: {}
        };
        icms70.ICMS70['orig'] = item.orig;
        icms70.ICMS70['CST'] = item.cstIcms;
        icms70.ICMS70['modBC'] = item.modBC;
        icms70.ICMS70['pRedBC'] = numUtil.dec2(item.pRedbCst)
        icms70.ICMS70['vBC'] = numUtil.dec2(item.vrBC);
        icms70.ICMS70['pICMS'] = numUtil.dec2(item.picms);
        icms70.ICMS70['vICMS'] = numUtil.dec2(item.vIcms);
        icms70.ICMS70['modBCST'] = item.modBCST;
        icms70.ICMS70['pRedBCST'] = numUtil.dec2(item.pRedbCst);
        icms70.ICMS70['vBCST'] = numUtil.dec2(item.vbCst);
        icms70.ICMS70['pICMSST'] = numUtil.dec2(item.pIcmsSt);
        icms70.ICMS70['vICMSST'] = numUtil.dec2(item.vicmsst);
        if (item.motDesIcms && item.VICMSDeson && item.VICMSDeson > 0) {
            icms70.ICMS70['vICMSDeson'] = numUtil.dec2(item.VICMSDeson);
            icms70.ICMS70['motDesICMS'] = item.motDesIcms;
        };
        return icms70;
    }
    _geraIcms90(item) {
        var icms90 = {
            ICMS90: {}
        };
        icms90.ICMS90['orig'] = item.orig;
        icms90.ICMS90['CST'] = item.cstIcms;
        icms90.ICMS90['modBC'] = item.modBC;
        icms90.ICMS90['vBC'] = numUtil.dec2(item.vrBC);
        icms90.ICMS90['pRedBC'] = numUtil.dec2(item.pRedbCst); //Estranho Redução de CST?
        icms90.ICMS90['pICMS'] = numUtil.dec2(item.picms);
        icms90.ICMS90['vICMS'] = numUtil.dec2(item.vIcms);
        icms90.ICMS90['modBCST'] = item.modBCST;

        icms90.ICMS90['pRedBCST'] = numUtil.dec2(item.pRedbCst);
        icms90.ICMS90['vBCST'] = numUtil.dec2(item.vbCst);
        icms90.ICMS90['pICMSST'] = numUtil.dec2(item.pIcmsSt);
        icms90.ICMS90['vICMSST'] = numUtil.dec2(item.vicmsst);
        if (item.motDesIcms && item.VICMSDeson && item.VICMSDeson > 0) {
            icms90.ICMS90['vICMSDeson'] = numUtil.dec2(item.VICMSDeson);
            icms90.ICMS90['motDesICMS'] = item.motDesIcms;
        };
        return icms90;
    }
    _geraCOFINS(item) {
        var COFINS = {};
        switch (item.cstCofins) {
            case "01":
            case "02":
                return {
                    COFINSAliq: {
                        CST: item.cstCofins,
                        vBC: numUtil.dec2(item.vBCCofins),
                        pCOFINS: numUtil.dec2(item.pPofins),
                        vCOFINS: numUtil.dec2(item.vCofins)
                    }
                }
            case "03":
                return {
                    COFINSQtde: {
                        CST: item.cstCofins,
                        qBCProd: numUtil.dec3(item.qBcProdCofins),
                        vAliqProd: numUtil.dec2(item.vAliqProdCofins),
                        vCOFINS: numUtil.dec2(item.vCofins),
                    }
                }
            case "04":
            case "05":
            case "06":
            case "07":
            case "08":
            case "09":
                return {
                    COFINSNT: {
                        CST: item.cstCofins
                    }
                }

            case "49":
            case "50":
            case "51":
            case "52":
            case "53":
            case "54":
            case "55":
            case "56":
            case "60":
            case "61":
            case "62":
            case "63":
            case "64":
            case "65":
            case "66":
            case "67":
            case "70":
            case "71":
            case "72":
            case "73":
            case "74":
            case "75":
            case "98":
            case "99":
                return {
                    COFINSOutr: {
                        CST: item.cstCofins,
                        vBC: numUtil.dec2(item.vBCCofins),
                        pCOFINS: numUtil.dec2(item.pPofins),
                        vCOFINS: numUtil.dec2(item.vCofins)
                    }
                }
        }

    }
    _geraICMSUFDest(item) {

        return {
            vBCUFDest: numUtil.dec2(item.vbcufdest),
            pFCPUFDest: numUtil.dec2(item.pfcpufdest),
            pICMSUFDest: numUtil.dec2(item.icmsufdest),
            pICMSInter: numUtil.dec2(item.picmsinter),
            pICMSInterPart: numUtil.dec2(item.picmsinterpart),
            vFCPUFDest: numUtil.dec2(item.vFCPUFDest),
            vICMSUFDest: numUtil.dec2(item.vICMSUFDest),
            vICMSUFRemet: numUtil.dec2(item.vICMSUFRemet)
        }


    }
    _geraPIS(item) {

        switch (item.cstPis) {

            case "01":
            case "02":
                return {
                    PISAliq: {
                        CST: item.cstPis,
                        vBC: numUtil.dec2(item.vBcPis),
                        pPIS: numUtil.dec2(item.pPis),
                        vPIS: numUtil.dec2(item.vPis)
                    }
                }
            case "03":
                return {
                    PISQtde: {
                        CST: item.cstPis,
                        qBCProd: numUtil.dec3(item.qBcProdPis),
                        vAliqProd: numUtil.dec2(item.vAliqProdPis),
                        vPIS: numUtil.dec2(item.vPis)
                    }
                }
            case "04":
            case "05":
            case "06":
            case "07":
            case "08":
            case "09":
                return {
                    PISNT: {
                        CST: item.cstPis,
                    }
                }
            case "49":
            case "50":
            case "51":
            case "52":
            case "53":
            case "54":
            case "55":
            case "56":
            case "60":
            case "61":
            case "62":
            case "63":
            case "64":
            case "65":
            case "66":
            case "67":
            case "70":
            case "71":
            case "72":
            case "73":
            case "74":
            case "75":
            case "98":
            case "99":
                return {
                    PISOutr: {
                        CST: item.cstPis,
                        vBC: numUtil.dec2(item.vBcPis),
                        pPIS: numUtil.dec2(item.pPis),
                        vPIS: numUtil.dec2(item.vPis)
                    }
                }
        }

    }
    _geraIPI(item) {
        switch (item.cstIpi) {

            case "00":
            case "49":
            case "50":
            case "99":
                var IPI = {}
                if (item.vipi > 0) {
                    if (item.cenq != '') {
                        IPI['cEnq'] = item.cenq;
                    } else {
                        IPI['cEnq'] = '999';
                    }
                    IPI['IPITrib'] = {
                        CST: item.cstipi,
                        vBC: numUtil.dec2(item.bipi),
                        pIPI: numUtil.dec2(item.pIpi),
                        vIPI: numUtil.dec2(item.vipi)
                    }
                }
                return IPI;
            case "01":
            case "02":
            case "03":
            case "04":
            case "51":
            case "52":
            case "53":
            case "54":
            case "55":
                var IPI = {}

                if (item.cenq != '') {
                    IPI['cEnq'] = item.cenq;
                } else {
                    IPI['cEnq'] = '999';
                }
                IPI['IPINT'] = {
                    CST: item.cstipi
                }
                return IPI;
        }
    }
    _geraForma(item) {
        //@TODO pegar tipo de pagamento da NFCe
        var pag = {
            tPag: _.padStart(item.pagamentoInstance.tipoFormaNFCe, 2, '0'),
            vPag: numUtil.dec2(item.valor)
        }
        if (pag.tPag == '03' || pag.tPag == '04') {
            pag['card'] = {
                'tpIntegra': 2
            };
        }
        return pag;
    }
    _geraCsosn(item) {
        if (item) {
            switch (item.csosn) {
                case '101':
                    return this._geraIcms101(item);
                case '102':
                case '103':
                case '300':
                case '400':
                    return this._geraIcms102(item);
                case '201':
                    return this._geraIcms201(item);
                case '202':
                case '203':
                    return this._geraIcms202(item);
                case '500':
                    return this._geraIcms500(item);
                case '900':
                    return this._geraIcms900(item);
            }
        }
        return;
    }
    _geraIcms101(item) {

        var icms101 = {
            ICMSSN101: {}
        };

        icms101.ICMSSN101['orig'] = item.orig;
        icms101.ICMSSN101['CSOSN'] = item.csosn;
        icms101.ICMSSN101['pCredSN'] = item.pCredSn;
        icms101.ICMSSN101['vCredICMSSN'] = numUtil.dec2(item.vCredIcmsSn);

        return icms101;
    }
    _geraIcms102(item) {

        var icms102 = {
            ICMSSN102: {}
        };

        icms102.ICMSSN102['orig'] = item.orig;
        icms102.ICMSSN102['CSOSN'] = item.csosn;

        return icms102;
    }
    _geraIcms201(item) {

        var icms201 = {
            ICMSSN201: {}
        };

        icms201.ICMSSN201['orig'] = item.orig;
        icms201.ICMSSN201['CSOSN'] = item.csosn;
        icms201.ICMSSN201['modBCST'] = item.modBCST;

        if (item.pRedbCst > 0) {
            icms201.ICMSSN201['pRedBCST'] = numUtil.dec2(item.pRedbCst);
        }

        icms201.ICMSSN201['vBCST'] = numUtil.dec2(item.vbCst);
        icms201.ICMSSN201['pICMSST'] = numUtil.dec2(item.pIcmsSt);
        icms201.ICMSSN201['vICMSST'] = numUtil.dec2(item.vicmsst);

        icms201.ICMSSN201['pCredSN'] = item.pCredSn || "0.00";
        icms201.ICMSSN201['vCredICMSSN'] = numUtil.dec2(item.vCredIcmsSn || 0);

        return icms201;
    }
    _geraIcms202(item) {

        var icms202 = {
            ICMSSN202: {}
        };

        icms202.ICMSSN202['orig'] = item.orig;
        icms202.ICMSSN202['CSOSN'] = item.csosn;
        icms202.ICMSSN202['modBCST'] = item.modBCST;

        if (item.pRedbCst > 0) {
            icms202.ICMSSN202['pRedBCST'] = numUtil.dec2(item.pRedbCst);
        }

        icms202.ICMSSN202['vBCST'] = numUtil.dec2(item.vbCst);
        icms202.ICMSSN202['pICMSST'] = numUtil.dec2(item.pIcmsSt);
        icms202.ICMSSN202['vICMSST'] = numUtil.dec2(item.vicmsst);

        return icms202;
    }
    _geraIcms500(item) {

        var icms500 = {
            ICMSSN500: {}
        };

        icms500.ICMSSN500['orig'] = item.orig;
        icms500.ICMSSN500['CSOSN'] = item.csosn;
        icms500.ICMSSN500['vBCSTRet'] = numUtil.dec2(item.vbCst);
        icms500.ICMSSN500['vICMSSTRet'] = numUtil.dec2(item.vicmsst);

        return icms500;
    }
    _geraIcms900(item) {

        var icms900 = {
            ICMSSN900: {}
        };

        icms900.ICMSSN900['orig'] = item.orig;
        icms900.ICMSSN900['CSOSN'] = item.csosn;
        icms900.ICMSSN900['modBC'] = item.modBC;


        icms900.ICMSSN900['vBC'] = numUtil.dec2(item.vrBC);
        if (item.ricms > 0) {
            icms900.ICMSSN900['pRedBC'] = numUtil.dec2(item.ricms);
        }

        icms900.ICMSSN900['pICMS'] = numUtil.dec2(item.picms);
        icms900.ICMSSN900['vICMS'] = numUtil.dec2(item.vIcms);

        icms900.ICMSSN900['modBCST'] = item.modBCST;

        if (item.pRedbCst > 0) {
            icms900.ICMSSN900['pRedBCST'] = numUtil.dec2(item.pRedbCst);
        }
        icms900.ICMSSN900['vBCST'] = numUtil.dec2(item.vbCst);
        icms900.ICMSSN900['pICMSST'] = numUtil.dec2(item.pIcmsSt);
        icms900.ICMSSN900['vICMSST'] = numUtil.dec2(item.vicmsst);
        icms900.ICMSSN900['pCredSN'] = item.pCredSn || "0.00";
        icms900.ICMSSN900['vCredICMSSN'] = numUtil.dec2(item.vCredIcmsSn || 0);

        return icms900;
    }

    _normalizaAtributosNFCe(itemCupom) {

        let item = {};

        //ICMS
        let aliquotaIcms = itemCupom.produtoInstance.tributacao.aliquotaICMS || 0;

        let cstIcms = itemCupom.produtoInstance.tributacao.cstCsosnICMS || 0;
        let csosn = '0';

        if (cstIcms.toString().length >= 3) {
            item.csosn = _.padStart(cstIcms, 3, '0');
            item.cstIcms = 0;
        } else {
            item.cstIcms = _.padStart(cstIcms, 2, '0');
        }

        item.orig = itemCupom.produtoInstance.origem;
        item.modBC = '0'; //TODO @modalidade de base de calculo de ICMS item.produto;
        item.vrBC = itemCupom.total.vBC;
        item.pRedbCst = 0; //Estranho Redução de CST?
        item.picms = itemCupom.produtoInstance.tributacao.aliquotaICMS || 0;
        item.vIcms = itemCupom.total.vICMS;
        item.modBCST = '0'; //TODO @modalidade de base de calculo de ICMS item.produto;
        item.pRedbCst = 0;
        item.vbCst = 0;
        item.pIcmsSt = 0;
        item.vicmsst = 0;
        if (this.config.dadosFilial.regime == 1) {
            item.ricms = 0;
            item.pCredSn = 0;
            item.vCredIcmsSn = 0;
        }

        //PIS
        let aliquotaPis = itemCupom.produtoInstance.tributacao.aliquotaPIS || 0;
        let valorPis = itemCupom.total.vPIS;
        item.cstPis = _.padStart(itemCupom.produtoInstance.tributacao.cstPIS, 2, '0');
        if (valorPis > 0) {
            item.vBcPis = itemCupom.valorBruto;
            item.pPis = aliquotaPis;
            item.vPis = valorPis
        }

        //COFINS
        let aliquotaCofins = itemCupom.produtoInstance.tributacao.aliquotaCOFINS || 0;
        let valorCofins = itemCupom.total.vCOFINS;
        item.cstCofins = _.padStart(itemCupom.produtoInstance.tributacao.cstCOFINS, 2, '0');
        if (valorCofins > 0) {
            item.vBCCofins = itemCupom.valorBruto;
            item.pPofins = aliquotaCofins;
            item.vCofins = valorCofins
        }

        //IPI
        let aliquotaIpi = itemCupom.produtoInstance.tributacao.aliquotaIPI || 0;
        let valorIpi = itemCupom.total.vIPI;
        item.cstIpi = _.padStart(itemCupom.produtoInstance.tributacao.cstIPI, 2, '0');
        if (valorIpi > 0) {
            item.bipi = itemCupom.valorBruto;
            item.pIpi = aliquotaIpi;
            item.vipi = valorIpi
        }

        //IBPT
        item.vTotTribEst = itemCupom.total.vTotTribEst;
        item.vTotTrib = itemCupom.total.vTotTribFed;
        item.vTotTribMun = itemCupom.total.vTotTribMun;

        return item;

    }

    calcDigitoEans(numero){

        let factor = 3;
        let sum = 0;
        let index;
        let digito;

        for (index = numero.length; index > 0; --index) {
            sum = sum + parseInt(numero.substring(index - 1, index)) * factor;
            factor = 4 - factor;
        }

        let calculado = ((1000 - sum) % 10);

        digito = calculado;
        let numlen = numero.length;

        if (numlen > 17) {
            digito = -1;
        }
        if (((((numlen != 7) && (numlen != 11)) && (numlen != 12)) && (numlen != 13)) && (numlen != 17)) {
            digito = -1;
        }
        return digito;
    };

    assinarXml(xml){

        var crt = this.config.dadosFilial.certificadoObj.pem;
        
        function XMLKeyInfo() {
            this.getKeyInfo = function (key, prefix) {
                let regex = /-+BEGIN CERTIFICATE-+([^-]+)-+END CERTIFICATE-+/im;
                let match = regex.exec(crt);
                let certificate = match[1].trim().replace(/\r/g, '').replace(/\n/g, '');
                return "<X509Data><X509Certificate>"+certificate+"</X509Certificate></X509Data>";
            }
            this.getKey = function (keyInfo) {
                return crt;
            }
        }

        let sig = new SignedXml();
        sig.addReference("//*[local-name(.)='infNFe']", ['http://www.w3.org/2000/09/xmldsig#enveloped-signature', 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315']);
        sig.canonicalizationAlgorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
        sig.signingKey = crt;
        sig.keyInfoProvider = new XMLKeyInfo();
        sig.computeSignature(xml);       
        return sig.getSignedXml();
    };

}


module.exports = NfceService;