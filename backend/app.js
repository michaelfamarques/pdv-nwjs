const async = require("async");
const fs = require("fs");
const moment = require("moment");
const utils = require("./utils");
const db = require("./db");
const api = require("./api");
const logger = require("./logger");
const printer = require("printer");
const util = require('util');

const ConfigClass = db.Configuracao;
const CupomVendaClass = db.CupomVenda;

module.exports.init = function (cb) {
    return checkDatabase()
        .then(() => carregaConfig());
}

module.exports.db = db;
module.exports.api = api;

function checkDatabase(){
    var exists = fs.existsSync(db.config.storage);
    if(!exists){
        logger.info('Criando DB');
        return module.exports.resetar();
    }else{
        return Promise.resolve();
    }    
}

function carregaConfig(){
    return ConfigClass.carregaConfiguracao();
}

module.exports.sync = function(){   
    return api.sincronizaDados();
}

module.exports.resetar = function(){   
    return db.sequelize.sync({force: true});
}

module.exports.getImpressoras = function(){
    return printer.getPrinters();
}

//npm install sqlite3 --build-from-source --runtime=node-webkit --target_arch=x64 --target=0.26.5