var request = require('superagent');
var moment = require('moment');
var async = require('async');
var fs = require('fs');
var path = require('path');
var app = require('./app');
var https = require('https');
var Stream = require('stream').Transform;

var endpoint = "https://erpqas.avancoinfo.net/api";
var endpointNfc = "https://erpqas.avancoinfo.net";

function ping(dados){

    var configInstance = app.db.Configuracao.getConfigGlobal();

    return request
        .get(endpointNfc + '/ping')
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            return Promise.resolve(true);
        }).catch((err) => {
            return Promise.resolve(false);
        });

}

function enviarNota(dados){

    var configInstance = app.db.Configuracao.getConfigGlobal();

    return request
        .post(endpointNfc + '/nfc/novo/insert')
        .send(dados)
        .set('authorization', configInstance.apiKey)
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            return Promise.resolve(res.body);
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function consultaCliente(documento){

    var configInstance = app.db.Configuracao.getConfigGlobal();

    return request
        .get(endpoint + '/Clientes/consultaFrente')
        .query({
            filter: JSON.stringify({ where: { or: [{ cpfCnpj: documento }, { cliCartao: documento }] } })
        })
        .set('authorization', configInstance.apiKey)
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            return Promise.resolve(res.body);
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function cancelarVenda(cupom, justificativa) {

    var configInstance = app.db.Configuracao.getConfigGlobal();

    return request
        .post(endpointNfc + '/nfc/novo/cancelar')
        .send({ justificativa: justificativa, chave: cupom.autorizacao.chave, cupom: null })
        .set('authorization', configInstance.apiKey)
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            return Promise.resolve(res.body);
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function logaUsuario(email, senha) {

    return request
        .post(endpoint + '/Usuarios/login')
        .query({
            include: 'user',
            rememberMe: 'true',
            sistema: 'novoerp'
        })
        .send({
            email: email,
            password: senha
        }) // sends a JSON post body
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            let result = res.body;
            return request
                .get(endpoint + '/Filials')
                .query({
                    filter: JSON.stringify({
                        where: {
                            empresa: result.user.empresa
                        },
                        include: ['configuracaoObj', 'serieObj', 'certificadoObj']
                    })
                })
                .set('authorization', result.id)
                .set('atualizaLicenca', 'true')
                .type('application/json')
                .accept('application/json')
                .then((res) => {
                    var filiais = res.body;
                    if (filiais && filiais.length > 0) {
                        return Promise.resolve({
                            empresa: result.user.empresa,
                            authorization: result.id,
                            filiais: filiais
                        });
                    } else {
                        return Promise.reject(new Error('Nenhuma filial cadastrada no novoERP para este usuário!'));
                    }
                }).catch((err) => {
                    return Promise.reject(err);
                });
        }).catch((err) => {
            if (err.status == 401) {
                return Promise.reject(new Error('Email ou senha inválidos!'));
            }
            return Promise.reject(err);
        });

}

function initFilialEPdv(empresa, filial, token, pdv) {

    return request
        .get(endpoint + '/Apikeys')
        .query({
            filter: JSON.stringify({
                where: {
                    empresa: empresa,
                    filial: filial,
                    descricao: "novoPDV"
                }
            }),
            todasfiliais: 'true'
        })
        .set('authorization', token)
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            var apis = res.body;
            if (apis && apis.length > 0) {
                return Promise.resolve(apis[0].key);
            } else {
                return request
                    .post(endpoint + '/Apikeys')
                    .send({
                        empresa: empresa,
                        filial: filial,
                        descricao: 'novoPDV'
                    })
                    .set('authorization', token)
                    .query({
                        'todasfiliais': 'true'
                    })
                    .type('application/json')
                    .accept('application/json')
                    .then((res) => {
                        return Promise.resolve(res.body.key);
                    }).catch((err) => {
                        return Promise.reject(err);
                    });
            }
        }).then((token) => {

            return request
                .get(endpoint + '/Pdvs')
                .query({
                    filter: JSON.stringify({
                        where: {
                            empresa: empresa,
                            filial: filial,
                            pdv: pdv
                        }
                    })
                })
                .set('authorization', token)
                .type('application/json')
                .accept('application/json')
                .then((res) => {
                    var pdvs = res.body;
                    if (pdvs && pdvs.length > 0) {
                        return Promise.resolve(token, pdvs[0]);
                    } else {
                        return request
                            .post(endpoint + '/Pdvs')
                            .send({
                                empresa: empresa,
                                filial: filial,
                                pdv: pdv
                            })
                            .set('authorization', token)
                            .type('application/json')
                            .accept('application/json')
                            .then((res) => Promise.resolve(token, res.body));
                    }
                }).catch((err) => {
                    return Promise.reject(err);
                });

        }).catch((err) => {
            return Promise.reject(err);
        });

}

function syncProdutos(configInstance) {

    var dataAlteracao = null;
    var Op = app.db.Sequelize.Op;

    return app.db.Produto.getUltimaDataAlteracao().then((data) => {
        dataAlteracao = data;
        return request
            .get(endpoint + '/Produtos/countprodutosalterados')
            .query({
                dataUltimaAlteracao: data.format('YYYY-MM-DD')
            })
            .set('authorization', configInstance.apiKey)
            .type('application/json')
            .accept('application/json').catch((err) => {
                return Promise.reject(err);
            });
    }).then((res) => {
        var count = res.body;
        var arrProdutos = [];
        var pagina = 0;
        var limit = 100;
        return new Promise((resolve, reject) => {
            async.doUntil(function (cb) {
                request
                    .get(endpoint + '/Produtos/produtosalteradosfrente')
                    .query({
                        dataUltimaAlteracao: dataAlteracao.format('YYYY-MM-DD'),
                        limit: limit,
                        skip: limit * pagina++
                    })
                    .set('authorization', configInstance.apiKey)
                    .type('application/json')
                    .accept('application/json')
                    .then((res) => {
                        arrProdutos = arrProdutos.concat(res.body);
                        cb();
                    }).catch(cb);
            }, function () {
                return arrProdutos.length >= count;
            }, function (err) {
                if (err) reject(err);
                resolve(arrProdutos);
            });
        });
    }).then((arrProdutos) => {
        var models = [];
        var idsRemover = [];
        arrProdutos.forEach((p) => {
            p.eans.forEach((e) => {                
                idsRemover.push(e.codigoEan);
                if (p.fichaFinanceiraObjeto && p.fichaFinanceiraObjeto.precoVenda > 0) {
                    models.push({
                        codigoEan: e.codigoEan,
                        descricao: e.descricao,
                        ncm: p.ncm,
                        exNcm: p.exNcm,
                        pesavel: !!p.pesoVariavel,
                        vendaFracionada: !!p.vendaFracionada,
                        produtoSuspenso: !!p.produtoSuspenso,
                        quantidadeMinVenda: p.quantidadeMinVenda,
                        quantidadeMaxVenda: p.quantidadeMaxVenda,
                        unidade: p.unidadeVenda,
                        tributacao: p.classificacaoProduto,
                        precoVenda: p.fichaFinanceiraObjeto.precoVenda,
                        promocoes: p.arrPromocoes,
                        idGrupo: p.grupo,
                        codigoProduto: p.codigoProduto,
                        origem: p.origem,
                        idErp: p.id,
                        dataAlteracao: moment(p.dataAlteracao).format()
                    });
                }
            });
        });
        return app.db.Produto.destroy({
            where: {
                codigoEan: {
                    [Op.in]: idsRemover
                }
            }
        }).then(() => {
            return app.db.Produto.bulkCreate(models).then(() => {
                return Promise.resolve(true);
            });
        });
    }).catch((err) => {
        return Promise.reject(err);
    });

}

function syncUsuarios(configInstance) {

    return request
        .get(endpoint + '/PdvSenhas')
        .set('authorization', configInstance.apiKey)
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            var usuarios = res.body;
            return app.db.Usuario.destroy({
                where: {},
                truncate: true
            }).then(() => {
                var models = [];
                res.body.forEach((u) => {
                    models.push({
                        idUsuario: u.id,
                        codigo: u.codigooper,
                        nome: u.nomeoper,
                        senha: u.senhaoper,
                        cpf: u.cpfoper,
                        idGrupoUsuario: u.idgrupooper
                    })
                });
                return app.db.Usuario.bulkCreate(models).then(() => {
                    return Promise.resolve(true);
                });
            });
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function syncFormasPagamento(configInstance) {

    return request
        .get(endpoint + '/PdvFormaspgs')
        .set('authorization', configInstance.apiKey)
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            var usuarios = res.body;
            return app.db.Pagamento.destroy({
                where: {},
                truncate: true
            }).then(() => {
                var models = [];
                res.body.forEach((u) => {
                    models.push({
                        idPagamento: u.id,
                        codigo: u.codigo,
                        descricao: u.descricao,
                        maxParcelas: Number(u.maxparcela),
                        valorMinimoParcela: parseFloat(u.valminparc),
                        troco: u.dartroco == 'S',
                        solicitaDataVencimento: u.pedevencto == 'S',
                        solicitaCliente: u.pedeclient == 'S',
                        aceitaClienteEmBranco: u.cliebranco == 'S',
                        clienteSoCpfCnpj: u.socpfcnpj == 'S',
                        consultaCliente: u.consclient == 'S',
                        tipoFormaNFCe: u.tipoformaNfce
                    });
                });
                return app.db.Pagamento.bulkCreate(models).then(() => {
                    return Promise.resolve(true);
                });
            });
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function syncFilial(configInstance) {

    return request
        .get(endpoint + '/Filials/findOne')
        .set('authorization', configInstance.apiKey)
        .query({
            filter: JSON.stringify({
                where: {
                    id: configInstance.dadosFilial.id
                },
                include: ['configuracaoObj', 'serieObj', 'certificadoObj']
            })
        })
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            configInstance.dadosFilial = res.body;
            return new Promise((resolve, reject) => {
                if (res.body.logomarca) {
                    var filePath = path.join(nw.App.dataPath, path.basename(res.body.logomarca));
                    fs.closeSync(fs.openSync(filePath, 'w'));
                    https.request(res.body.logomarca, function (response) {
                        var data = new Stream();
                        response.on('data', function (chunk) {
                            data.push(chunk);
                        });
                        response.on('end', function () {
                            fs.writeFileSync(filePath, data.read());
                            resolve(res.body);
                        });
                        response.on('error', reject);
                    }).end();
                }else{
                    return resolve(res.body);
                }                
            });            
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function syncPdv(configInstance) {

    return request
        .get(endpoint + '/Pdvs/findOne')
        .set('authorization', configInstance.apiKey)
        .query({
            filter: JSON.stringify({
                where: {
                    pdv: configInstance.numPdv
                }
            })
        })
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            configInstance.dadosPdv = res.body;
            return Promise.resolve(res.body);
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function syncConfigGeral(configInstance) {

    return request
        .get(endpoint + '/PdvConfiguracaos/verificaPdv')
        .set('authorization', configInstance.apiKey)
        .type('application/json')
        .accept('application/json')
        .then((res) => {
            configInstance.configGeral = res.body;
            return Promise.resolve(res.body);
        }).catch((err) => {
            return Promise.reject(err);
        });

}

function sincronizaDados(configInstance) {

    if (!configInstance) {
        configInstance = app.db.Configuracao.getConfigGlobal();
    }
    var options = { type: app.db.sequelize.QueryTypes.RAW };
    return app.db.sequelize.query('PRAGMA foreign_keys = OFF;', null, options)
        .then(() => {
            return Promise.all([
                syncProdutos(configInstance),
                syncUsuarios(configInstance),
                syncFormasPagamento(configInstance),
                syncFilial(configInstance),
                syncPdv(configInstance),
                syncConfigGeral(configInstance)
            ]);
        }).then(() => {
            app.db.sequelize.query('PRAGMA foreign_keys = ON;', null, options); 
        });   

}

module.exports.logaUsuario = logaUsuario;
module.exports.initFilialEPdv = initFilialEPdv;
module.exports.sincronizaDados = sincronizaDados;
module.exports.enviarNota = enviarNota;
module.exports.cancelarVenda = cancelarVenda;
module.exports.ping = ping;
module.exports.consultaCliente = consultaCliente;