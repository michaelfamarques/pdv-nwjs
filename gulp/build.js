'use strict';

var path = require('path');
var gulp = require('gulp');
var browserSync = require('browser-sync');
var execSync = require('child_process').execSync;
var execFileSync = require('child_process').execFileSync;
var conf = require('./conf');
var fsExtra = require("fs-extra");

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function ()
{
    return gulp.src([
            path.join(conf.paths.src, '/frontEnd/**/*.html'),
            path.join(conf.paths.tmp, '/serve/frontEnd/**/*.html')
        ])
        .pipe($.htmlmin({
            collapseWhitespace: true,
            maxLineLength     : 120,
            removeComments    : true
        }))
        .pipe($.angularTemplatecache('templateCacheHtml.js', {
            module: 'novoPdv',
            root  : 'frontEnd'
        }))
        .pipe(gulp.dest(conf.paths.tmp + '/partials/'));
});

gulp.task('partials-reload', ['partials'], function ()
{
    return browserSync.reload();
});

gulp.task('html', ['inject', 'partials'], function ()
{
    var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), {read: false});
    var partialsInjectOptions = {
        starttag    : '<!-- inject:partials -->',
        ignorePath  : path.join(conf.paths.tmp, '/partials'),
        addRootSlash: false
    };

    var cssFilter = $.filter('**/*.css', {restore: true});
    var jsFilter = $.filter('**/*.js', {restore: true});
    var htmlFilter = $.filter('*.html', {restore: true});

    return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
        .pipe($.inject(partialsInjectFile, partialsInjectOptions))
        .pipe($.useref())
        .pipe(jsFilter)
        //.pipe($.sourcemaps.init())
        .pipe($.babel({
            presets: ['es2015-script']
        }))
        .pipe($.ngAnnotate())
        .pipe($.uglify({preserveComments: $.uglifySaveLicense})).on('error', conf.errorHandler('Uglify'))
        .pipe($.rev())
        //.pipe($.sourcemaps.write('maps'))
        .pipe(jsFilter.restore)
        .pipe(cssFilter)
        //.pipe($.sourcemaps.init())
        .pipe($.cleanCss())
        .pipe($.rev())
        //.pipe($.sourcemaps.write('maps'))
        .pipe(cssFilter.restore)
        .pipe($.revReplace())
        .pipe(htmlFilter)
        .pipe($.htmlmin({
            collapseWhitespace: true,
            maxLineLength     : 120,
            removeComments    : true
        }))
        .pipe(htmlFilter.restore)
        .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
        .pipe($.size({
            title    : path.join(conf.paths.dist, '/'),
            showFiles: true
        }));
});

// Only applies for fonts from bower dependencies
// Custom fonts are handled by the "other" task
gulp.task('fonts', function ()
{
    return gulp.src($.mainBowerFiles())
        .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe($.flatten())
        .pipe(gulp.dest(path.join(conf.paths.dist, '/fonts/')));
});

gulp.task('other', function ()
{
    var fileFilter = $.filter(function (file)
    {
        return file.stat.isFile();
    });

    return gulp.src([
            path.join(conf.paths.src, '/**/*'),
            path.join('!' + conf.paths.src, '/**/*.{html,css,js,scss}')
        ])
        .pipe(fileFilter)
        .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('clean', function ()
{
    return $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')]);
});

gulp.task('build', ['html', 'fonts', 'other'], function(){

    console.log('Limpando diretorios');
    fsExtra.emptyDirSync("./build");
    fsExtra.copySync('./app', './build/app');
    fsExtra.copySync('./backend', './build/backend');
    fsExtra.copySync('./package.json', './build/package.json');
    fsExtra.copySync('./main.js', './build/main.js');
    console.log('NPM install somente production');
    execSync("npm install --only=production --ignore-scripts", {
        cwd: "./build/"
    });
    buildNw();

});

gulp.task('build-nw', function(){
    buildNw();
});

function buildNw(){
    console.log('Executando build nw');    
    fsExtra.emptyDirSync("./dist");
    var file = require.resolve("../node_modules/.bin/build.cmd");
    execFileSync(file,  ["--tasks", "win-x64,linux-x64,mac-x64,", "."], {
        cwd: "./build/"
    });

    console.log('Copiando libs');

    fsExtra.copySync(path.join(__dirname, '../tools/PDFtoPrinter.exe'), path.join(__dirname, '../dist/pdv-nwjs-1.0.0-win-x64/PDFtoPrinter.exe'));
    fsExtra.copySync(path.join(__dirname, '../libs/sqlite3/node-webkit-v0.26.4-win32-x64'), path.join(__dirname, '../dist/pdv-nwjs-1.0.0-win-x64/node_modules/sqlite3/lib/binding/node-webkit-v0.26.4-win32-x64'));
    fsExtra.copySync(path.join(__dirname, '../libs/printer/node-webkit-v0.26.4-win32-x64'), path.join(__dirname, '../dist/pdv-nwjs-1.0.0-win-x64/node_modules/printer/build'));   

    fsExtra.copySync(path.join(__dirname, '../libs/sqlite3/node-webkit-v0.26.4-linux-x64'), path.join(__dirname, '../dist/pdv-nwjs-1.0.0-linux-x64/node_modules/sqlite3/lib/binding/node-webkit-v0.26.4-linux-x64'));
    fsExtra.copySync(path.join(__dirname, '../libs/printer/node-webkit-v0.26.4-linux-x64'), path.join(__dirname, '../dist/pdv-nwjs-1.0.0-linux-x64/node_modules/printer/build'));

    fsExtra.copySync(path.join(__dirname, '../libs/sqlite3/node-webkit-v0.26.4-osx-x64'), path.join(__dirname, '../dist/pdv-nwjs-1.0.0-mac-x64/pdv-nwjs.app/Contents/Resources/app.nw/node_modules/sqlite3/lib/binding/node-webkit-v0.26.4-darwin-x64'));
    fsExtra.copySync(path.join(__dirname, '../libs/printer/node-webkit-v0.26.4-osx-x64'), path.join(__dirname, '../dist/pdv-nwjs-1.0.0-mac-x64/pdv-nwjs.app/Contents/Resources/app.nw/node_modules/printer/build'));
    
}