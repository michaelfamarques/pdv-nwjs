'use strict';

var path = require('path');
var fs = require('fs');
var gulp = require('gulp');
var conf = require('./conf');
var spawn = require("child_process").spawn;
var exec = require("child_process").exec;
var execSync = require("child_process").execSync;

var browserSync = require('browser-sync');
var browserSyncSpa = require('browser-sync-spa');

var util = require('util');

var proxyMiddleware = require('http-proxy-middleware');

function browserSyncInit(baseDir, browser)
{
    browser = browser === undefined ? 'default' : browser;

    var routes = null;
    if ( baseDir === conf.paths.src || (util.isArray(baseDir) && baseDir.indexOf(conf.paths.src) !== -1) )
    {
        routes = {
            '/bower_components': 'bower_components'
        };
    }

    var server = {
        baseDir: baseDir,
        routes : routes
    };

    browserSync.instance = browserSync.init({
        startPath: '/',
        host     : 'localhost',
        port     : 5000,
        server   : server,
        browser  : browser,
        localOnly: true,
        socket   : {
            domain  : 'localhost:5001'
        },
        open     : false
    });
}

browserSync.use(browserSyncSpa({
    selector: '[ng-app]'// Only needed for angular apps
}));

gulp.task('serve', ['watch'], function ()
{
    browserSyncInit([path.join(conf.paths.tmp, '/serve'), conf.paths.src]);
});

gulp.task('serve:dist', ['build'], function ()
{
    browserSyncInit(conf.paths.dist);
});

gulp.task('serve:e2e', ['inject'], function ()
{
    browserSyncInit([conf.paths.tmp + '/serve', conf.paths.src], []);
});

gulp.task('serve:e2e-dist', ['build'], function ()
{
    browserSyncInit(conf.paths.dist, []);
});

gulp.task('serve:background', ['serve:kill-background'], function ()
{
    var out = fs.openSync('./outBS.log', 'a');
    var err = fs.openSync('./errBS.log', 'a');

    spawn('gulp', ['serve'], {
        stdio: [ 'ignore', out, err ], // piping all stdio to /dev/null
        detached: true
    }).unref();
    console.log("Iniciado browsersync");
    process.exit();
});

gulp.task('serve:kill-background', function ()
{
    try{
        execSync("fuser -n tcp -k 5000");
        console.log("Encerrado servidor já em uso.");
    }catch(e){
        console.log("Porta não estava em uso.");
    }

    if(process.env.ENCERRAR == "1"){
        process.exit();
    }

});