'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep').stream;
var _ = require('lodash');

var browserSync = require('browser-sync');

gulp.task('serve:assets', function(){
    return gulp.src(path.join(conf.paths.src, '/assets/**/*'))
        .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/assets')));
});

gulp.task('serve:inject', ['scripts', 'styles', 'partials', 'serve:assets'], function () {
    var injectStyles = gulp.src([
        path.join(conf.paths.tmp, '/serve/frontEnd/**/*.css'),
        path.join('!' + conf.paths.tmp, '/frontEnd/vendor.css')
    ], {
        read: false
    });

    var injectScripts = gulp.src([
            path.join(conf.paths.src, '/frontEnd/**/*.module.js'),
            path.join(conf.paths.src, '/frontEnd/**/*.js'),
        ])
        .pipe($.babel({
            presets: ['es2015-script']
        }))
        .pipe($.angularFilesort()).on('error', conf.errorHandler('AngularFilesort'));

    var injectOptionsStyles = {
        ignorePath: [conf.paths.tmp],
        addRootSlash: false
    };

    var injectOptionsScripts = {
        ignorePath: [conf.paths.tmp],
        addRootSlash: false,
        addPrefix: ".."
    };   

    var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), {read: false});
    var partialsInjectOptions = {
        starttag    : '<!-- inject:partials -->',
        ignorePath  : conf.paths.tmp,
        addRootSlash: false
    }; 

    var scriptBrowserSyncScript = "<script src='http://localhost:5000/browser-sync/browser-sync-client.js?v=2.18.8'></script>";

    return gulp.src(path.join(conf.paths.src, '/*.html'))
        .pipe($.inject(injectStyles, injectOptionsStyles))
        .pipe($.inject(injectScripts, injectOptionsScripts))
        .pipe($.inject(partialsInjectFile, partialsInjectOptions))
        .pipe($.replace("<!-- replace:browsersyncScript -->", scriptBrowserSyncScript))
        .pipe(wiredep(_.extend({}, conf.wiredep)))
        .pipe(gulp.dest(conf.paths.tmp));
});