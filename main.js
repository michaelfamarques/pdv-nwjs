const logger = require("./backend/logger");
const path = require("path");
const app = require("./backend/app");

var urlIndex = "app/index.html";

//Se o node_env for dev abrir o arquivo do browsersync
if(process.env.NODE_ENV == "development"){
    urlIndex = ".tmp/index.html";
}

function callbackWindowOpen(win){

    win.height = 768;
    win.width = 1024;
    win.setResizable(false);
    win.setAlwaysOnTop(false);

    win.on('loaded', function(){
        win.setPosition('center');
        win.show();
        if(process.env.NODE_ENV == "development"){
            win.showDevTools(); 
        }
        setTimeout(function(){
            win.focus();
        },1000);
    });

}

function start(){
    return new Promise((resolve, reject) => {
        if(process.env.NODE_ENV == "development" && global.chrome){
            chrome.developerPrivate.openDevTools({
                renderViewId: -1,
                renderProcessId: -1,
                extensionId: chrome.runtime.id
            });
            setTimeout(() => {
                resolve();
            }, 1000);
        }else{
            resolve();
        }
    });
}

start().then(() => {
    return app.init().then(() => {
        nw.Window.open(urlIndex, {}, callbackWindowOpen);
    });
}).catch((err) => {
    console.log(err);
});