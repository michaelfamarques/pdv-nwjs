const PDFDocument  = require('pdfkit');
const fs  = require('fs');
const os  = require('os');
const path  = require('path');
const qrcode  = require('qr-image');

var fontSizePequena = 5;
var fontSizeNormal = 6;
var fontSizeGrande = 8;
var optAlign = 'left';

var tmpFileStream = fs.createWriteStream(path.join(__dirname, '../cupom2.pdf'));

var conteudo = '{{QuebraLinha}}{{AlinhaCentro}}{{AlturaDuplaOn}}{{NegritoOn}}MACDEL NFCE PR{{AlturaDuplaOff}}{{NegritoOff}}{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}CNPJ:05.361.065/0001-86{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}Avenida Parana, 3818 - Zona I{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}Umuarama-PR - 87501030{{QuebraLinha}}{{CondensadoOff}}{{AlinhaCentro}}----------------------------------------{{QuebraLinha}}{{FonteNormal}}{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}{{NegritoOn}}DOCUMENTO AUXILIAR DA NOTA FISCAL DE CONSUMIDOR ELETRÔNICA{{CondensadoOff}}{{NegritoOff}}{{QuebraLinha}}{{AlinhaCentro}}{{FontePequena}}ITEM | COD | DESC | QTD | UN | VL UNIT.| VL TOTAL{{FonteNormal}}{{QuebraLinha}}----------------------------------------{{CondensadoOn}}{{QuebraLinha}}{{AlinhaEsquerda}}{{AlinhaEsquerda}}001 00000000000017 NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL{{QuebraLinha}}{{AlinhaDireita}}1 UN X 4.99 = 4.99{{QuebraLinha}}{{CondensadoOff}}{{AlinhaEsquerda}}{{QuebraLinha}}QTD. TOTAL DE ITENS                    1{{QuebraLinha}}VALOR TOTAL R$                      4.99{{QuebraLinha}}VALOR A PAGAR R$                    4.99{{QuebraLinha}}DINHEIRO                            4.99{{QuebraLinha}}{{CondensadoOff}}{{AlinhaEsquerda}}{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}Consulte pela Chave de Acesso em:{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}www.sped.fazenda.pr.gov.br{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}{{NegritoOn}}4118 0505 3610 6500 0186 6506 4000 0131 2010 0000 0180 {{NegritoOff}}{{CondensadoOff}}{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}CONSUMIDOR NAO IDENTIFICADO{{CondensadoOff}}{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}NFC-e No.: {{NegritoOn}}000013120{{NegritoOff}}  Serie: {{NegritoOn}}64{{NegritoOff}}   {{NegritoOn}}18/05/2018 18:25{{NegritoOff}}{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}Protocolo de Autorizacao: {{NegritoOn}}141180000569122{{QuebraLinha}}{{NegritoOff}}Data de autorizacao: {{NegritoOn}}18/05/2018 18:25{{NegritoOff}}{{QuebraLinha}}{{CondensadoOn}}{{AlinhaCentro}}{{NegritoOn}}EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL{{NegritoOff}}{{CondensadoOff}}{{AlinhaEsquerda}}{{QuebraLinha}}{{QrCode}}http://www.dfeportal.fazenda.pr.gov.br/dfe-portal/rest/servico/consultaNFCe?chNFe=41180505361065000186650640000131201000000180&nVersao=100&tpAmb=2&dhEmi=323031382d30352d31385431383a32353a30322d30333a3030&vNF=4.99&vICMS=0.00&digVal=3054563572764b416f3167724f4c7a69734878474f4f6d6f524d773d&cIdToken=000001&cHashQRCode=CA6966BCDE82CF6B3D9BFB75639CD8660F6D7025{{QrCode}}{{QuebraLinha}}{{AlinhaCentro}}{{CondensadoOn}}Tributos Incidentes Lei Federal 12.741/12{{QuebraLinha}}Federal: R$0.20 Estadual: R$0.89 Municipal: R$0.00 - Fonte: IBPT{{QuebraLinha}}{{CondensadoOff}}{{AlinhaCentro}}----------------------------------------{{QuebraLinha}}PDV.: 64                      Serie: 64{{QuebraLinha}}DATA: 18/05/2018 18:25:06 Seq.: 000018{{QuebraLinha}}{{AlinhaCentro}}----------------------------------------{{QuebraLinha}}{{CortaPapel}}{{FonteNormal}}{{QuebraLinha}}';

var linhas = conteudo.split("{{QuebraLinha}}");

var docWidth = 150;
var docHeight = (linhas.length * 14) + (docWidth - 50);

var doc = new PDFDocument({
    margins: {
        left: 1,
        right: 1,
        top: 5,
        bottom: 5
    },
    size: [docWidth, docHeight]
});

doc.pipe(tmpFileStream);
doc.fontSize(fontSizeNormal);
doc.registerFont('Normal', path.join(__dirname, '../backend/fonts/DejaVuSansMono.ttf'));
doc.registerFont('Bold', path.join(__dirname, '../backend/fonts/DejaVuSansMono-Bold.ttf'));

doc.font('Normal');

var parsingQrcode = false;
var qrCodeUrl = "";

linhas.forEach((linha) => {

    if(linha.trim() == "") return;

    var bufferText = "";

    for(var i = 0; i < linha.length; i++){
        if(linha[i] == "{" && linha[i+1] == "{"){
            if(bufferText){
                printText(bufferText);
                bufferText = "";
            }
            var indexFinal = linha.substring(i+2).indexOf("}}");
            var comando = linha.substring(i+2, i+2+indexFinal);
            i = i+2+indexFinal+1;
            parseComando(comando);
        }else{
            bufferText += linha[i];
        }
    }

    if(bufferText){
        printText(bufferText);
        bufferText = "";
    }

    doc.moveDown(0.5);

});

doc.end();

function printText(bufferText){
    if(parsingQrcode){
        qrCodeUrl = bufferText;
        return;
    }
    doc.text(bufferText, {
        align: optAlign,
    });
}

function parseComando(comando){

    switch(comando){
        case "AlinhaEsquerda":
            optAlign = "left";
        break;
        case "AlinhaCentro":
            optAlign = "center";
        break;
        case "AlinhaDireita":
            optAlign = "right";
        break;
        case "AlturaDuplaOn":
            doc.fontSize(fontSizeGrande);
        break;
        case "AlturaDuplaOff":
        case "FonteNormal":
            doc.fontSize(fontSizeNormal);
        break;
        case "FontePequena":
            doc.fontSize(fontSizePequena);
        break;
        case "NegritoOn":
            doc.font('Bold');
        break;
        case "NegritoOff":
            doc.font('Normal');
        break;
        case "QrCode":
            if(!parsingQrcode){
                parsingQrcode = true;
            }else{
                parsingQrcode = false;
                var qrImage = qrcode.imageSync(qrCodeUrl, { type: 'png' });
                doc.moveDown();
                doc.image(qrImage, (doc.page.width - (docWidth - 50)) / 2, doc.y, {
                    fit: [docWidth - 50, docWidth - 50],
                    align: 'center',
                    valign: 'center'
                });
                doc.moveDown();
            }            
        break;
        case "CondensadoOn":
        case "CondensadoOff":        
        case "CortaPapel":
        case "ExtendidoOn":        
        case "ExtendidoOff":
            //FazNada
        break;        
    }

}